FROM node:14.16.1

RUN mkdir -p /usr/scr/app

WORKDIR /usr/scr/app

COPY package*.json .

RUN npm i

RUN chown -R node:node .

COPY . .

RUN chown -R node:node .

EXPOSE 3000

User node

ENTRYPOINT [ "npm", "start"]
