const axios = require('axios');
const { XoxoToken } = require('../models/xoxoDay_token');
const config = require('config');

module.exports = async function generateXoxoToken() {
    try {
        setTimeout(async () => {
            const client_id = config.get("CLIENT_ID")
            console.log('client_id :>> ', client_id);
            
    
            const client_secret = config.get("CLIENT_SECRET") 
            console.log('client_secret :>> ', client_secret);
           
    
            const grant_type = "refresh_token"
    
            const xoxo_refresh_token = config.get("REFRESH_TOKEN") 
            console.log('xoxo_refresh_token :>> ', xoxo_refresh_token);
            
    
            console.log('xoxo_refresh_token :>> ', xoxo_refresh_token);
    
            const ref_token_data = await XoxoToken.find({});
    
            let refresh_token;
            if (ref_token_data.length != 0) {
                refresh_token = ref_token_data[0].refresh_token
            } else {
                refresh_token = xoxo_refresh_token
            }
    
            const url = config.get("GET_XOXO_TOKEN_URL")  
            console.log('url :>> ', url);
    
            const data = {
                grant_type,
                refresh_token,
                client_id,
                client_secret
            }
            console.log('data :>> ', data);
    
            const resp = await axios.post(url, data);
            console.log('resp :>> ', resp.data);
            const tokenData = await XoxoToken.find({});
    
            if (tokenData.length != 0) {
                const tokenId = tokenData[0]._id;
                await XoxoToken.updateOne({ _id: tokenId }, { access_token: resp.data.access_token, expires_in: resp.data.expires_in, refresh_token: resp.data.refresh_token }, { upsert: true, new: true })
            } else {
                await XoxoToken.create(resp.data)
            }
        }, 5000);

    } catch (error) {
        console.log('CATCH_ERROR :: generateXoxoToken :>> ', error, error.message);
    }

}