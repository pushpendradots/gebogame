const axios = require('axios');
const config = require("config");
const { XoxoToken } = require('../models/xoxoDay_token');
const { Voucher } = require('../models/vouchers');
const { htmlToText } = require('html-to-text');

module.exports = async function getXoxoVoucher() {
    try {
        console.log('abcd :>> ');
        const tokenData = await XoxoToken.find({});
        console.log('tokenData :>> ', tokenData);
        const getVoucherUrl = config.get("GET_VOUCHER_URL") 
        let token
        if (tokenData) {
            token = tokenData[0].access_token
        }

        const bearerToken = `Bearer` + ` ` + `${token}`
        console.log('bearerToken :>> ', bearerToken);

        customConfig = {
            headers: {
                Authorization: bearerToken
            }
        };

        let voucherBody = {
            query: "plumProAPI.mutation.getVouchers",
            tag: "plumProAPI",
            variables: {}
        }

        voucherBody.variables["data"] = {
            limit: 0,
            page: 0,
            includeProducts: "",
            excludeProducts: "",
            sort: {
                field: "",
                order: ""
            },
            filters: [
                {
                    key: "country",
                    value: "india"
                },
                {
                    key: "deliveryType",
                    value: "Realtime"
                },
                {
                    key: "maxPrice",
                    value: "10000"
                },
                {
                    key: "minPrice",
                    value: "10"
                }
            ]
        }

        const resp = await axios.post(getVoucherUrl, voucherBody, customConfig);



        const fetchedVoucher = resp.data.data.getVouchers.data

        let fixedValueVoucher = [];
        let openValueVoucher = [];
        for (let i = 0; i < fetchedVoucher.length; i++) {
            const element = fetchedVoucher[i];
            const obj = {
                productId: element.productId,
                name: element.name,
                orderQuantityLimit: element.orderQuantityLimit,
                categories: element.categories,
                lastUpdateDate: element.lastUpdateDate,
                imageUrl: element.imageUrl,
                currencyCode: element.currencyCode,
                currencyName: element.currencyName,
                countryName: element.countryName,
                countryCode: element.countryCode,
                countries: element.countries,
                valueType: element.valueType,
                maxValue: element.maxValue,
                minValue: element.minValue,
                tatInDays: element.tatInDays,
                usageType: element.usageType,
                deliveryType: element.deliveryType,
                fee: element.fee,
                discount: element.discount,
                isPhoneNumberMandatory: element.isPhoneNumberMandatory
            }

            // convert html string to normal string
            const normalString = htmlToText(element.expiryAndValidity, { wordwrap: 130 })
            obj["expiryAndValidity"] = normalString

            const denominationString = element.valueDenominations
            const arrayNumbers = denominationString.split(",");
            obj["valueDenominations"] = arrayNumbers

            //"Imagine Apple Premium Reseller", "Spar Hypermarket",
            const brandArray = [
                "AJIO",
                "BPCL",
                "Domino's",
                "EatSure",
                "HP",
                "Myntra",
                "Nykaa",
                "Ovenstory",
                "Paytm",
                "Spar",
                "Zomato",
                "Tata",
                "Apple",
                "PVR",
                "Lifestyle",
                "BookMyShow"
            ];

            for (let index = 0; index < brandArray.length; index++) {
                const el = brandArray[index];

                console.log('(element.name).includes(el) :>> ', (element.name).includes(el));
                if ((element.name).includes(el)) {
                    if(element.valueType == "fixed_denomination") {
                        fixedValueVoucher.push(obj)
                    }else{
                        openValueVoucher.push(obj)
                    }
                   
                }
            }


            // allVoucherArray.push(obj)
        }

        const findVoucherDetail = await Voucher.find({});

        console.log('findVoucherDetail :>> ', findVoucherDetail);

        if(findVoucherDetail.length == 0){
            await Voucher.create({fixedValueVoucher: fixedValueVoucher, openValueVoucher: openValueVoucher});
        }else{
            const voucherId = findVoucherDetail[0]._id
            await Voucher.updateOne({_id: voucherId },{$set: {fixedValueVoucher: fixedValueVoucher, openValueVoucher: openValueVoucher }},{upsert:true, new: true})
        }


    } catch (error) {
        console.log('CATCH_ERROR :: generateXoxoToken :>> ', error, error.message);
    }

}