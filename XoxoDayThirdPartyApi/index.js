module.exports = {
    getToken: require('./generate-XoxoDay-Token'),
    getVoucher: require('./get-xoxo-Voucher'),
    placeXoxoVoucherOrder: require('./placeOrder')
}