const axios = require('axios');
const config = require("config");
const { XoxoToken } = require('../models/xoxoDay_token');
const { Voucher } = require('../models/vouchers');
const { htmlToText } = require('html-to-text');

module.exports = async function placeXoxoVoucherOrder(data) {
    try {

        const tokenData = await XoxoToken.find({});
        console.log('tokenData :>> ', tokenData);
        const placeOrderUrl = config.get("PLACE_ORDER") ;
        console.log('placeOrderUrl :>> ', placeOrderUrl);
        let token
        if (tokenData) {
            token = tokenData[0].access_token
        }

        const bearerToken = `Bearer ${token}`;
        console.log('bearerToken :>> ', bearerToken);

        const customConfig = {
            headers: {
                'Content-Type': "application/json",
                authorization: bearerToken
            }
        };

        let placeOrderBody = {
            query: "plumProAPI.mutation.placeOrder",
            tag: "plumProAPI",
            variables: {}
        }

        placeOrderBody.variables["data"] = data;
        console.log('placeOrderBody :>> ', placeOrderBody);

        const resp = await axios.post(placeOrderUrl, placeOrderBody, customConfig)
       
        const result = {
            isError : false,
            resp : resp.data.data.placeOrder.data
        }
        console.log('placeXoxoVoucherOrder :: result :>> ', result);
        return result
    } catch (error) {
        console.log('CATCH_ERROR :: generateXoxoToken :>> ', error.response.data);
        console.log('error.response.data :>> ', error.response.data);
        const result = {
            isError : true,
            resp : error.response.data
        }
        return result
    }

}