const config = require("config");
const fs = require("fs");
const EmailHelper = require("../utils/emailHelper");
async function sendOTPforUser(obj) {
    console.log(obj);
    let sendEmail = config.get("ENABLE_EMAIL");
    if (sendEmail == true) {
        var template = fs.readFileSync(__dirname + "/../utils/template/headerfooter.html").toString();

        template = template.replace(/{{otp}}/g, obj.otp);

        console.log("sendEmail------>1>", template);
        EmailHelper.sendEmail(obj.email, "Welcome to Prediction App", template);
        return true;
    }
}

module.exports = {
    sendOTPforUser : sendOTPforUser
}
