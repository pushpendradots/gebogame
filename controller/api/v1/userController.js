const router = require("express").Router();
const moment = require('moment');
const logger = require('winston');
const { Session } = require("./../../../models/Session")
const { User, validate } = require("./../../../models/User");
const { FriendList } = require('./../../../models/firendsList');
const HelperUtils = require("./../../../utils/helpers");
const { Dailyrewards } = require('./../../../models/dailyRewards');
const { DailySpins } = require('./../../../models/dailySpin');
const { successResponseOBJ } = require('./../../../utils/helpers');
const { UserLevel } = require('./../../../models/userLevel');
const { Histroy } = require('./../../../models/purchaseHistory');
const { VoucherOrder } = require('./../../../models/voucherOrder');
const { Voucher } = require('./../../../models/vouchers');
const config = require("config");
const xoxoDay = require('./../../../XoxoDayThirdPartyApi');
const {
    joinTable,
    adminJoinTable,
    adminTicketOpen,
    setWinnData,
    dailyRewardsCounter,
    getRemainingRewardTime,
    setDailyRewardEventResponse,
    getRemainingSpinTime,
    setDailySpinEventResponse,
    adminLeave,
    userLeave,
    resetTicket,
    setBetData,
    setGameBetData,
    getCourrantLevelVoucher
} = require('./../../../service');
const { FeedBack } = require('./../../../models/feedBack');
const { Table } = require('../../../../models/Table');
const { Tablebet } = require('./../../../models/Tablebet');


/*  Post Signup Api
    @Apipath: api/v1/user_signup
    @Method: Post
    @Params: {
      
    }
    @Tags: This API is used for signup user
*/

// const candidateBankDetail = async (req, res) => {
//     const { candidate_id} = req.params;
  
//     try {
//       const candidate = await Candidate.findByPk(candidate_id);
//       if (!candidate) {
//         return res.apiError("Candidate not found!", 400);
//       }
  
//       let emptypename;
//       if(candidate.induction_type==1){
//         emptypename = candidate.emptype;
//       }
//       else if(candidate.induction_type==2){
//         emptypename = candidate.emptype_2;
//       }
//       else if(candidate.induction_type==3){
//         emptypename = candidate.emptype_3;
//       }
  
//       const candidateBankDetailList = await CandidateBankDetail.findOne({
//         where: { candidate_id: candidate_id, emp_type: emptypename },
//       });
  
//       return res.apiSuccess(
//         "Candidate bank detail list get successfully.",
//         candidateBankDetailList
//       );
//     } catch (error) {
//       console.error("Error getting candidate bank detail list :", error);
//       return res.apiError("Something went wrong!", 500);
//     }
//   };


  const userSignup = async (req, res) => {
    const { candidate_id} = req.params;
    // start api routes
    console.log(' en :  SIGN_UP  start :>> ', msg);
    try {
      res.status(400).send(HelperUtils.errorObj("This email already exits."));
      return;
    } catch (error) {
      res.status(400).send(HelperUtils.errorObj("This email already exits."));
      return;

    }
  };

  module.exports = {
    userSignup,
  };