const {User} = require("../models/User");

module.exports = async function isUserOnline() {
    try {

        const users = await User.find({isActive: true});
        if(users.length == 0) {
            return true
        }else {
            return false
        }
        
    } catch (error) {
        console.log('CATCH_ERROR :: isUserOnline :>> ', error );
    }
    
}