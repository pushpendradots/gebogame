
const { Table } = require('../models/Table');
const service = require('../service');

module.exports = async function botJoinIdealTableScheduler() {
    try {
        
        const table = await Table.find({ isStatic: true });
        
        let tables = []
        for (let element of table) {
            console.log('element :>> ', element);
            const userData = element.userData
            if (userData.length <= 8) {
                tables.push(element._id);
                const roomSize = 0;
                const userLeave = false;
                const isStatic = true
                const socket = null
                
                await service.botJoinTable(element._id, roomSize, userLeave, socket, isStatic)
            }
        }
        
    } catch (error) {
        console.log('CATCH_ERROR :: botJoinIdealTableScheduler :>> ', error);
    }
}