
const { Table } = require('../models/Table');
const { User } = require('../models/User');
const service = require('../service');
const config = require('config');
// update admin balance every 12AM
module.exports = async function updateAdminBalance() {
    try {

        const adminUserId = config.get("STATIC_ADMIN_ID");

        await User.updateOne({ _id: adminUserId }, { walletBalance: 1000000000 })

        return
    } catch (error) {
        console.log('CATCH_ERROR :: botJoinIdealTableScheduler :>> ', error);
    }
}