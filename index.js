const express = require('express')
const app = express()
const FB = require("fb");
const config = require("config");
const cron = require('node-cron');
const jobSchedule = require('./cronJobSchedule');
// const xoxoApi = require('./XoxoDayThirdPartyApi');
let protcol = config.get("serverProtocal");

    if (protcol == "https") {
        const fs = require("fs");
        httpsOptions = module.exports = {
            key: fs.readFileSync("cert/artoon.key"),
            cert: fs.readFileSync("cert/artoon.crt")
        };
        console.log('httpsOptions :>> ', httpsOptions);
        console.log("https Server Started2");
        server = require("https").createServer(httpsOptions, app);
    } else {
        console.log("http Server Started3");
        server = require("http").createServer(app);
    };

cron.schedule('0 0 * * *', async () => {
    console.log('running a task every 12:00 AM');
    await jobSchedule.updateAdminBalance()
});

// get voucher detail every 8:00 AM for sun to fri
cron.schedule('0 8 * * 0-6', async () => {
    console.log('running a task get voucher detail every 8:00 AM for sun to fri');
    await xoxoApi.getVoucher()
});

// get voucher detail every 8:00 PM for sun to fri
cron.schedule('0 20 * * 0-6', async () => {
    console.log('running a task get voucher detail every 8:00 PM for sun to fri');
    await xoxoApi.getVoucher()
});


//  get voucher detail every 8:00 PM for sun to fri
cron.schedule('0 0 */14 * *', async () => {
    console.log('Generate xoxo day token every 14 day');
    await xoxoApi.getToken()
});

app.get("/test1", (req, res) => {
    // console.log("---config---", req.body, config);
    res.status(200).send("0OK");
});


app.set('view engine', 'ejs')
require("./startup/logging")(app);

require("./startup/database")();
require("./startup/routes")(app);
require("./startup/swagger")(app);
require("./startup/socketserver")(server);

const PORT = config.get("port");

server.listen(PORT, () => { console.log("Server started 56", PORT) })

//require('./XoxoDayThirdPartyApi/generate-XoxoDay-Token')();
process
    .on('unhandledRejection', (reason, p) => {
        console.error(
            reason,
            'Unhandled Rejection at Promise >> ',
            new Date(),
            ' >> ',
            p
        );
    })
    .on('uncaughtException', (err) => {
        console.error('Uncaught Exception thrown', new Date(), ' >> ', '\n', err);
    });
