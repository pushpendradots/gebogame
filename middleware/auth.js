const HelperUtils = require("../utils/helpers");
const { Session } = require("../models/Session");


async function auth(req, res, next) {
    const token = req.header("x-auth-token");
    if (!token) return res.status(401).send(HelperUtils.errorObj("Access Denied No Token Found"));
    try {
        let query = {
            "sid": token
        }
        let payload = await Session.findOne(query);
        if (payload) {
            req.user = payload;
            req.user._id = payload.uid
            next();
        }
        else {
            res.status(401).send(HelperUtils.errorObj("Invalid Token"));
        }
    } catch (ex) {
        console.log(ex)
        res.status(401).send(HelperUtils.errorObj("Invalid Token"));
    }
}


module.exports = auth;