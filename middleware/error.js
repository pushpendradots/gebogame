const winston = require("winston");
const HelperUtils = require("../utils/helpers");

module.exports = function (err, req, res, next) {
    winston.error(err.message, err);
    res.status(500).send(HelperUtils.errorObj(err.message, err));
};