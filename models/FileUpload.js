const mongoose = require("mongoose");
const { PathUrlConstant } = require("../utils/const")

const FileUploadSchema = new mongoose.Schema({
    fileId: { type: String, required: true },
    uploadType: { type: Number, required: true },
    filePath: { type: String, required: true },
    originalname: { type: String, required: true },
    mimetype: { type: String },
    durs: { type: Number, default: 0 }, //duration
    size: {type: Number }

}, { timestamps: true, strict: false });

FileUploadSchema.virtual('fullPath').get(function () {
    return PathUrlConstant.APPLICATION_URL + this.filePath;
});

const FileUpload = mongoose.model("FileUpload", FileUploadSchema);


exports.FileUpload = FileUpload;
