const mongoose = require("mongoose");

const LetterSchema = new mongoose.Schema({
    name: { type: String, required: true },
    fileUrl: { type: String, required: true },
    status: { type: Boolean, required: true, default: false },
}, { timestamps: true, strict: false });


const Letter = mongoose.model("Letter", LetterSchema);


module.exports = {
    Letter
}