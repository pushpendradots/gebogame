const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const SessionSchema = new mongoose.Schema(
    {
        sid: { type: String, required: true },
        fullName: { type: String, required: true },
        email: { type: String, required: false, default: "" },
        mobileNo: { type: String, required: false, default: "" },
        profileUrl: { type: String, required: false, default: "" },
        uid: {
            type: Schema.Types.ObjectId,
            ref: "User"
        },
        role: { type: String, required: false, default: "" }
    },
    { timestamps: true, strict: false }
);

const Session = mongoose.model("Session", SessionSchema);
exports.Session = Session;

