const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TableSchema = new mongoose.Schema({
    tableName: { type: String, required: true },
    tableNumber: { type: String, required: true },
    maxPlayerSeat: { type: Number, required: true, default: 10 },
    minPlayerSeat: { type: Number, required: true, default: 0 },
    status: { type: Boolean, required: true, default: false },
    isRoundStart: { type: Boolean, required: false, default: false },
    isAdminStartRound: { type: Boolean, required: false, default: false },
    adminTicketSkipCount: { type: Number, required: true, default: 0 },
    isStatic: {type: Boolean, required: false, default: false },
    isTicketOpenByAdmin: { type: Boolean, required: false, default: false },
    tableStatus: {type: String , enum: ["inWaiting" , "timerStart" , "inBatting", "winnerDeclared"],  default: "inWaiting"},
    userData: [
        {
            userId: {
                type: Schema.Types.ObjectId,
                ref: "User"
            },
            userBet: [],
            isComp: { type: Boolean, required: false, default: false },
            socketId: { type: String, default: "" }
        }
    ],
    adminDetail: {
        adminUserId: { type: String, required: false },
        isAdminJoin: { type: Boolean, required: false },
        socketId: { type: String, required: false }
    },
    currentRoundStartTimer: { type: Date, required: false, default: "" },
    currentRoundCounter: { type: Number, required: false, default: 0 },
    agoraToken: { type: String, require: false, default: "" }
}, { timestamps: true, strict: false });


const Table = mongoose.model("Table", TableSchema);

// Join Table Schema

const JoinTableSchema = new mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    tableId: {
        type: Schema.Types.ObjectId,
        ref: "Table"
    },
    isActive: { type: Boolean, required: false, default: false },
    availablePlayer: { type: Number, required: false, default: 0 }
}, { timestamps: true, strict: false });

const JoinTable = mongoose.model("JoinTable", JoinTableSchema);

// bat table

const BatTableSchema = new mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    tableId: {
        type: Schema.Types.ObjectId,
        ref: "Table"
    },
    bets: [
        {
            batLatter: {
                type: Number,
                required: true,
                default: null,
            },
            batLattereAmount: {
                type: Number,
                required: true,
                default: null,
            }
        }
    ],

}, { timestamps: true, strict: false });

const BatTable = mongoose.model("BatTable", BatTableSchema)


module.exports = {
    Table,
    JoinTable,
    BatTable
}