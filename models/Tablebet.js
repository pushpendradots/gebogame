const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Tablebet Schema
const TablebetSchema = new mongoose.Schema({
    tableName: { type: String, required: true },
    gameId: { type: String, required: true },
    userId: { type: String, required: true }, // Changed type to String
    betsAmount: { type: Number, required: true, default: 0 },
    userData: [
        {
            userId: { type: String, required: true }, // Include user_id here
            userId: {
                type: Schema.Types.ObjectId,
                ref: "User"
            },
            socketId: { type: String, default: "" }
        }
    ]
}, { timestamps: true, strict: false });

const Tablebet = mongoose.model("Tablebet", TablebetSchema);

// JoinTablebet Schema
const JoinTablebetSchema = new mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    tableId: {
        type: Schema.Types.ObjectId,
        ref: "Tablebet"
    }
}, { timestamps: true, strict: false });

const JoinTablebet = mongoose.model("JoinTablebet", JoinTablebetSchema);

module.exports = {
    Tablebet,
    JoinTablebet
};
