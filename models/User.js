const mongoose = require("mongoose");
const Joi = require("joi");
const HelperUtils = require("./../utils/helpers");
const { Session } = require("./../models/Session")


const UserSchema = new mongoose.Schema({
    fullName: { type: String, required: true },
    email: { type: String, required: false, default: "" },
    mobileNo: { type: String, required: false, default: "" },
    profileUrl: { type: String, required: false, default: "" },
    pass: { type: String, required: false },
    password: { type: String, required: false },
    role: { type: String, required: false, default: "" }, // Admin, User
    age: { type: String, required: false, default: "" },
    isComp: { type: Boolean, default: false },
    isMobileVarified: { type: Boolean, default: false },
    otp: { type: String, required: false, default: "" },
    deviceType: { type: String, default: "" }, // Android, IOS
    isActive: { type: Boolean, default: false },
    walletBalance: { type: Number, required: false, default: 300000 },
    rewardPoint: { type: Number, required: false, default: 0 },
    remainingCoin: { type: Number, required: false, default: 0 },
    win: { type: Number, required: false, default: 0 },
    lose: { type: Number, required: false, default: 0 },
    tie: { type: Number, required: false, default: 0 },
    userLevel: { type: Number, required: false, default: 1 }, // User level
    // Facebook 
    fid: { type: String, required: false, default: "" }, // facebookId
    ult: { type: String, required: false, default: "" }, // User login Type
    token: { type: String, required: false, default: "" },
    currantTableId: { type: String, required: false, default: "" },
    socketId: { type: String, required: false, default: "" },
    disconnectionTime: {type: Date}
}, { timestamps: true, strict: false });

UserSchema.methods.generateAuthToken = async function () {
    const token = HelperUtils.generateUUID();
    const sessionObj = {
        uid: this.id,
        fullName: this.fullName,
        role: this.role,
        profileUrl: this.profileUrl,
        mobileNo: this.mobileNo,
        email: this.email,
        sid: token,
        socketId: this.socketId
    };

    session = new Session(sessionObj);

    try {
        await session.save();
    }
    catch (ex) {
        console.log(ex);
    }

    return new Promise((resolve, reject) => {
        return resolve({ "token": token, role: this.role });
    })
};


const User = mongoose.model("User", UserSchema);

function validateUser(User) {
    const schema = {
        fullName: Joi.string().required(),
        email: Joi.string().required().email(),
        password: Joi.string().required()
    };
    return Joi.validate(User, schema);
}



module.exports = {
    User: User,
    validate: validateUser
}