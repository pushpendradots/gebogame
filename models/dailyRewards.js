const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const dailyRewards = new mongoose.Schema({
    userId: { type: String ,require : false , default: "" },
    dailyReward: [],
    lastCollectedDay : { type: Number, require: false, default: 0 },
    nextRewardTime : { type: Date, require: false, default: 0 },
    remainingTime: { type: Date, require: false, default: 0 },
    collectionTime: { type: String, require: false, default: 1 },
    collectionSkipTime: { type: String, require: false, default: 2 },
    collectionType: {type : String , require: false , default : "day"}
}, { timestamps: true, strict: false });


const Dailyrewards = mongoose.model("DailyRewards", dailyRewards);

module.exports = {
    Dailyrewards
}