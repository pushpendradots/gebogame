const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const dailySpin = new mongoose.Schema({
    userId: { type: String ,require : false , default: "" },
    dailySpinReward: [],
    isAdSpin: { type: String, require: false, default: "false" },
    lastCollectedSpinTime : { type: Date, require: false, default: 0 },
    nextSpinTime : { type: Date, require: false, default: 0 },
    remainingTime: { type: Date, require: false, default: 0 }
}, { timestamps: true, strict: false });


const DailySpins = mongoose.model("DailySpins", dailySpin);

module.exports = {
    DailySpins
}