const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const feedBack = new mongoose.Schema({
    userId: { type: String, default: "" },
    mobileNo: { type: String, default: "" },
    email: { type: String, default: "" },
    fid: { type: String, require: true, default: "" },
    feedBack: { type: String }
}, { timestamps: true, strict: false });


const FeedBack = mongoose.model("FeedBack", feedBack);

module.exports = {
    FeedBack
}