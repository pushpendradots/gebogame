const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const friendList = new mongoose.Schema({
    userId: {type: Schema.Types.ObjectId, ref: "User"},
    friendsId : [{userId : { type:  String }}],
    friendsRequest : [
        { userId: {type : String}}
     ],
}, { timestamps: true, strict: false });


const FriendList = mongoose.model("FriendList", friendList);

module.exports = {
    FriendList
}