const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const purchaseHistroy = new mongoose.Schema({
    userId: { type: String ,require : false , default: "" },
    histroy: [
        {
            amount: {type: Number, require:false, default: 0},
            purchaseDate: {type: String},
            purchaseTime: {type: String}
        }
    ],
    
}, { timestamps: true, strict: false });


const Histroy = mongoose.model("Histroy", purchaseHistroy);

module.exports = {
    Histroy
}