const mongoose = require("mongoose");

const UserLevelSchema = new mongoose.Schema({
    userId: { type: String, required: true, default: ""},
    userlevel:{type: Number, required: false, default: 1 },
    userlevelRound:{type: Number, required: false, default: 0 },
    userlevelRoundCount:{type: Number, required: false, default: 0 },
    levelUpRound:{type: Number, required: false, default: 0},
    levelUpPrice:{type: Number, required: false, default: 0},
    userVoucher:[{
        level: {type: Number, required: false, default: 1},
        levelPrice: {type: Number, required: false, default: 0},
        voucher: [
            {
                productId: { type: Number, default: 0 },
                name: { type: String, default: "" },
                orderQuantityLimit: { type: Number, default: 0 },
                categories: { type: String, default: "" },
                lastUpdateDate: { type: String, default: "" },
                imageUrl:{ type: String, default: "" },
                currencyCode:{ type: String, default: "" },
                currencyName:{ type: String, default: "" },
                countryName:{ type: String, default: "" },
                countryCode:{ type: String, default: "" },
                countries: [{
                    code: { type: String, default: "" },
                    name: { type: String, default: "" }
                }],
                valueType: { type: String, default: "" },
                maxValue: { type: Number, default: 0 },
                minValue: { type: Number, default: 0 },
                valueDenominations: [],
                tatInDays: { type: Number, default: 0 },
                usageType: { type: String, default: "" },
                deliveryType: { type: String, default: "" },
                fee: { type: Number, default: 0 },
                discount: { type: Number, default: 0 },
                isPhoneNumberMandatory: { type: Boolean, default: false },
                expiryAndValidity: { type: String, default: "" }  
            }
        ]
    }],
    claimedVoucher:[
        {
            level: {type: Number, default: 0},
            levelPrice: {type: Number, default: 0},
            isClaimed: {type: Boolean , default: false},
            voucherData: {
                orderId: {type: Number, default: 0},
                orderTotal: {type: Number, default: 0},
                orderDiscount: {type: String , default: ""},
                discountPercent: {type: String , default: ""},
                currencyCode: {type: String , default: ""},
                currencyValue: {type: Number, default: 0},
                amountCharged: {type: Number, default: 0},
                orderStatus:{type: String , default: ""},
                deliveryStatus:{type: String , default: ""},
                tag:{type: String , default: ""},
                quantity:  {type: Number, default: 0},
                vouchers: [
                    {
                        productId: {type: Number, default: 0},
                        orderId: {type: Number, default: 0},
                        voucherCode:{type: String , default: ""},
                        pin:{type: String , default: ""},
                        validity: {type: String , default: ""},
                        amount: {type: Number, default: 0},
                        currency:{type: String , default: ""},
                        country: {type: String , default: ""},
                        type:{type: String , default: ""},
                        currencyValue:  {type: Number, default: 0}
                    }
                ],
                voucherDetails: [
                    {
                        orderId:  {type: Number, default: 0},
                        productId:  {type: Number, default: 0},
                        productName: {type: String , default: ""},
                        currencyCode:{type: String , default: ""},
                        productStatus: {type: String , default: ""},
                        denomination:  {type: Number, default: 0}
                    }
                ]
            }
        }
    ]
    
}, { timestamps: true, strict: false });

const UserLevel = mongoose.model("UserLevel", UserLevelSchema);

module.exports = {
    UserLevel
}