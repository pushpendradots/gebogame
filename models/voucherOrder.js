const mongoose = require("mongoose");

const VouchersOrderSchema = new mongoose.Schema({
    userId: {type: String , default: ""},
    orderId: { type: Number, default: 0 },
    orderTotal: { type: Number, default: 0 },
    orderDiscount: { type: String, default: "" },
    discountPercent: { type: String, default: "" },
    currencyCode: { type: String, default: "" },
    currencyValue: { type: Number, default: 0 },
    amountCharged: { type: Number, default: 0 },
    orderStatus: { type: String, default: "" },
    deliveryStatus: { type: String, default: "" },
    tag: { type: String, default: "" },
    quantity: { type: Number, default: 0 },
    vouchers: [
        {
            productId: { type: Number, default: 0 },
            orderId: { type: Number, default: 0 },
            voucherCode: { type: String, default: "" },
            pin: { type: String, default: "" },
            validity: { type: String, default: "" },
            amount: { type: Number, default: 0 },
            currency: { type: String, default: "" },
            country: { type: String, default: "" },
            type: { type: String, default: "" },
            currencyValue: { type: Number, default: 0 },
        }
    ],
    voucherDetails: [
        {
            orderId: { type: Number, default: 0 },
            productId: { type: Number, default: 0 },
            productName: { type: String, default: "" },
            currencyCode: { type: String, default: "" },
            productStatus: { type: String, default: "" },
            denomination: { type: Number, default: 0 },
        }
    ]


}, { timestamps: true, strict: false });

const VoucherOrder = mongoose.model("VoucherOrder", VouchersOrderSchema);

module.exports = {
    VoucherOrder
}
