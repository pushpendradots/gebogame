const mongoose = require("mongoose");

const VouchersSchema = new mongoose.Schema({
    userMaxLevel: { type: Number, require: false, default: 0 },
    openValueVoucher: [{
        productId: { type: Number, default: 0 },
        name: { type: String, default: "" },
        orderQuantityLimit: { type: Number, default: 0 },
        categories: { type: String, default: "" },
        lastUpdateDate: { type: String, default: "" },
        imageUrl:{ type: String, default: "" },
        currencyCode:{ type: String, default: "" },
        currencyName:{ type: String, default: "" },
        countryName:{ type: String, default: "" },
        countryCode:{ type: String, default: "" },
        countries: [{
            code: { type: String, default: "" },
            name: { type: String, default: "" }
        }],
        valueType: { type: String, default: "" },
        maxValue: { type: Number, default: 0 },
        minValue: { type: Number, default: 0 },
        valueDenominations:[],
        tatInDays: { type: Number, default: 0 },
        usageType: { type: String, default: "" },
        deliveryType: { type: String, default: "" },
        fee: { type: Number, default: 0 },
        discount: { type: Number, default: 0 },
        isPhoneNumberMandatory: { type: Boolean, default: false },
        expiryAndValidity: { type: String, default: "" }
    }],
    fixedValueVoucher: [{
        productId: { type: Number, default: 0 },
        name: { type: String, default: "" },
        orderQuantityLimit: { type: Number, default: 0 },
        categories: { type: String, default: "" },
        lastUpdateDate: { type: String, default: "" },
        imageUrl:{ type: String, default: "" },
        currencyCode:{ type: String, default: "" },
        currencyName:{ type: String, default: "" },
        countryName:{ type: String, default: "" },
        countryCode:{ type: String, default: "" },
        countries: [{
            code: { type: String, default: "" },
            name: { type: String, default: "" }
        }],
        valueType: { type: String, default: "" },
        maxValue: { type: Number, default: 0 },
        minValue: { type: Number, default: 0 },
        valueDenominations: [],
        tatInDays: { type: Number, default: 0 },
        usageType: { type: String, default: "" },
        deliveryType: { type: String, default: "" },
        fee: { type: Number, default: 0 },
        discount: { type: Number, default: 0 },
        isPhoneNumberMandatory: { type: Boolean, default: false },
        expiryAndValidity: { type: String, default: "" }
    }]


}, { timestamps: true, strict: false });

const Voucher = mongoose.model("Voucher", VouchersSchema);

module.exports = {
    Voucher
}
