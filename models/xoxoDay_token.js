const mongoose = require("mongoose");

const XoxoTokenSchema = new mongoose.Schema({
    access_token: {type: String, default: ""},
    token_type: {type: String, default: ""},
    expires_in: {type: Number, default: ""},
    refresh_token: {type: String, default: ""},

    
}, { timestamps: true, strict: false });

const XoxoToken = mongoose.model("XoxoToken", XoxoTokenSchema);

module.exports = {
    XoxoToken
}