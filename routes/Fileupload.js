// const router = require("express").Router();
// const auth = require("../middleware/auth");
// const HelperUtils = require("../utils/helpers");
// const upload = require('../config/ModelMulter')
// const { FileUpload } = require('../models/FileUpload')


// /**
//  * Upload Single Image for System
//  * @route POST /file/upload
//  * @consumes multipart/form-data
//  * @param {file} file.formData
//  * @group FileUpload - File Upload operation
//  * @returns {object} 200 - file path object
//  *      
//  * @returns {Error}  Error - Unexpected error
//  * @security User
//  */
// router.post('/upload', [auth, upload.single('file')], async (req, res) => {
//     try {
//         const filePath = req.file.key;
//         let filePathArr = filePath.split("/");

//         const originalPath = filePathArr[1];
//         let fileNameArr = originalPath.split(".");

//         const fileId = fileNameArr[0];

//         let fileUploadObj = new FileUpload({
//             fileId: fileId,
//             uploadType: "16",
//             filePath: req.file.location,
//             originalname: req.file.originalname,
//             mimetype: req.file.mimetype,
//             size: req.file.size
//         });
    
//         await fileUploadObj.save();
    
//         res.send(HelperUtils.successObj("File Uploaded", fileUploadObj.toJSON()));
//         return;
//     } catch (error) {
//         console.log(error);
//         res.status(400).send(HelperUtils.errorObj("something went wrong"));
//         return;
//     }
// });


// module.exports = router;