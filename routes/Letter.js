const router = require("express").Router();
const _ = require("lodash");
const auth = require("../middleware/auth");
const HelperUtils = require("../utils/helpers");
const { Letter } = require("../models/Letter");


/**
 * @typedef CreateLetterModel
 * @property {string} name 
 * @property {string} fileUrl
 */

/**
 * Create Letter
 * @route POST /letter/create
 * @param {CreateLetterModel.model} CreateLetterModel.body.required - Create letter object
 * @group Letter - Letter operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.post("/create", auth, async (req, res) => {
    try {
        let checkExists = await Letter.findOne({ name: req.body.name });
        if (checkExists) {
            res.status(400).send(HelperUtils.errorObj("This letter already exits."));
            return;
        }
        let userObj = {};
        if (req.body.name) {
            userObj.name = req.body.name
        }
        if (req.body.fileUrl && req.body.fileUrl.length) {
            userObj.fileUrl = req.body.fileUrl
        }
        let letter = new Letter(userObj);
        await letter.save();
        res.status(200).send(HelperUtils.successObj("Letter created successfully"));
        return;
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});

/**
 * @typedef EditLetterModel
 * @property {string} id
 * @property {string} name
 * @property {string} fileUrl
 */

/**
 * Edit Letter
 * @route PUT /letter/edit
 * @param {EditLetterModel.model} EditLetterModel.body.required - Edit letter object
 * @group Letter - Letter operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.put("/edit", async (req, res) => {
    try {
        if (req.body.id) {
            let checkLetter = await Letter.findById({ _id: req.body.id });
            if (checkLetter == null) {
                res.status(400).send(HelperUtils.errorObj("This letter id not found"));
                return;
            }
            let checkLetterNameExits = await Letter.findOne({
                _id: { $ne: req.body.id },
                name: req.body.name
            });
            if (checkLetterNameExits != null) {
                res.send(HelperUtils.errorObj("This letter is already exist!"));
                return;
            }
            let obj = {};
            if (req.body.name) {
                obj.name = req.body.name
            }
            if (req.body.fileUrl && req.body.fileUrl.length) {
                obj.fileUrl = req.body.fileUrl
            }
            await Letter.findOneAndUpdate({ _id: checkLetter._id }, { $set: obj });
            res.status(200).send(HelperUtils.successObj("Letter updated successfully"));
            return;
        } else {
            res.status(400).send(HelperUtils.errorObj("Please pass letter id"));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});

/**
 * @typedef DeleteLetterModel
 * @property {string} id
 */

/**
 * Delete Letter
 * @route DELETE /letter/delete
 * @param {DeleteLetterModel.model} DeleteLetterModel.body.required - Delete letter object
 * @group Letter - Letter operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.delete("/delete", async (req, res) => {
    try {
        if (req.body.id) {
            let checkLetter = await Letter.findById({ _id: req.body.id });
            if (checkLetter == null) {
                res.status(400).send(HelperUtils.errorObj("This letter id not found"));
                return;
            }
            await Letter.findOneAndUpdate({ _id: checkLetter._id }, { $set: { status: true } });
            res.status(200).send(HelperUtils.successObj("Letter deleted successfully"));
            return;
        } else {
            res.status(400).send(HelperUtils.errorObj("Please pass letter id"));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});


/**
 * Letter List
 * @route GET /letter/list
 * @group Letter - Letter operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.get("/list", async (req, res) => {
    try {
        let list = await Letter.find({ status: false }).select("name fileUrl");
        res.status(200).send(HelperUtils.successObj("Letter list successfully", list));
        return;
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});


module.exports = router;