const router = require("express").Router();
const _ = require("lodash");
const auth = require("../middleware/auth");
const HelperUtils = require("../utils/helpers");
const { Table } = require("../models/Table");
const botJoinTable = require('../service/botJoinTable');
const { User } = require("../models/User");
const service = require('../service');
const config = require('config');

/**
 * @typedef CreateTableModel
 * @property {string} tableName 
 */


/**
 * Create Table
 * @route POST /table/create
 * @param {CreateTableModel.model} CreateTableModel.body.required - Create table object
 * @group Table - Table operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.post("/create", auth, async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.userId})
        if(user.walletBalance > 10000) {
            let checkExists = await Table.findOne({ tableName: req.body.tableName });
            if (checkExists) {
                res.status(400).send(HelperUtils.errorObj("This Table already exits."));
                return;
            }
            let tableNumber = Math.floor((Math.random() * 1000000) + 1);
            var finalName;
            if(req.body.isStatic == true){
                const randomName = service.randomName
                finalName = randomName[Math.floor(Math.random() * randomName.length)];
            }else{
                finalName = req.body.tableName;
            }
            let createTable = {
                tableName: finalName,
                tableNumber: tableNumber
            }
    
            // for local
            // const adminId = "63ce10cfd747138aa22b9db3"
            
            // for dev
            const adminId = config.get("STATIC_ADMIN_ID")
            console.log('adminId :>> ', adminId);
    
            console.log('createTable befor if:>> ', createTable);
            if (req.body.isStatic) {
                createTable["isStatic"] = req.body.isStatic;
                createTable["isAdminStartRound"] = req.body.isAdminStartRound
                createTable["adminDetail"] = {
                    adminUserId: adminId,
                    isAdminJoin: true,
                    socketId: ""
                }
            }
    
            console.log('createTable after if:>> ', createTable);
            let table = await Table.create(createTable);
            let resObj = {
                _id: table._id,
                tableName: table.tableName,
                tableNumber: table.tableNumber,
                minPlayerSeat: table.minPlayerSeat,
                maxPlayerSeat: table.maxPlayerSeat,
                availableSeat: table.availableSeat
            }
            if (req.body.isStatic == true) {
                const tableId = table._id
                const userLeave = false
                const isStatic = true
                const socket = ""
                await botJoinTable(tableId, userLeave, socket, isStatic)
    
            }
           return res.status(200).send(HelperUtils.successObj("Table Created successfully", resObj));
            
        }else {
            return res.status(200).send(HelperUtils.successObj(" minimum 10K balance is requerd "));
        }
       
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});


/**
 * Table List
 * @route GET /table/list
 * @group Table - Table operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.get("/list", auth, async (req, res) => {
    try {
        let list = await Table.aggregate([
            {
                $group: {
                    _id: "$_id",
                    tableName: { $first: "$tableName" },
                    tableNumber: { $first: "$tableNumber" },
                    maxPlayerSeat: { $first: "$maxPlayerSeat" },
                    minPlayerSeat: { $first: "$minPlayerSeat" }
                }
            }, {
                $addFields: {
                    availableSeat: { $subtract: ["$maxPlayerSeat", "$minPlayerSeat"] }
                }
            }, {
                $sort: {
                    createdAt: -1
                }
            }
        ]);
        res.status(200).send(HelperUtils.successObj("Table List successfully", list));
        return;
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});

/**
 * @typedef SearchTableModel
 * @property {string} tableNumber
 */


/**
 * Get Search Table
 * @route POST /table/search
 * @param {SearchTableModel.model} SearchTableModel.body.required - Search table object
 * @group Table - Table operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.post("/search", auth, async (req, res) => {
    try {
        let tableNumber = req.body.tableNumber.toString();
        let searchTable = await Table.findOne({ tableNumber: tableNumber });

        if (searchTable == null) {
            res.status(400).send(HelperUtils.errorObj("No table found"));
            return;
        }
        res.status(200).send(HelperUtils.successObj("Table get successfully", searchTable));
        return;
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});



module.exports = router;