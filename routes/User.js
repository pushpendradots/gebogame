const router = require("express").Router();
const bcrypt = require("bcryptjs");
const _ = require("lodash");
const auth = require("../middleware/auth");
const HelperUtils = require("../utils/helpers");
const { User, validate } = require("../models/User");
const { FriendList } = require('../models/firendsList');
const EmailController = require("../controller/Email.controller");
const EmailHelper = require("../utils/emailHelper");
const { Dailyrewards } = require("../models/dailyRewards");
const { DailySpins } = require("../models/dailySpin");
const { UserLevel } = require("../models/userLevel");
const { Session } = require("../models/Session");
const { FeedBack } = require("../models/feedBack");
const { Histroy } = require('../models/purchaseHistory');
const { Tablebet } = require('../models/Tablebet');
const { Table } = require('../models/Table');

const config = require("config");
const {
   
    setDailySpinEventResponse,
    getCourrantLevelVoucher,
    joinTable,
    dailyRewardsCounter,
    getRemainingSpinTime,
    setWinnData,
    adminJoinTable,
    getRemainingRewardTime,
    adminTicketOpen,
    resetTicket,
    adminLeave,
    setDailyRewardEventResponse

    
} = require('../service');

/**
 * @typedef UserSignupModel
 * @property {string} name.required
 * @property {number} type.required
 * @property {string} email
 * @property {number} phone
 * @property {string} password.required
 * @property {string} facebookUserId
 * @property {string} profileUrl
 * @property {string} deviceType
 */

/**
 * User Signup
 * @route POST /user/signup
 * @param {UserSignupModel.model} UserSignupModel.body.required - user Signup object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 */
router.post("/signup", async (req, res) => {
    try {
        let type = req.body.type;

        if (type === "1") {
            console.log('type === 1 :>> ', type === "1");
            const feedback = await FeedBack.findOne({ email: req.body.email })
            console.log('feedback :>> ', feedback);
            let checkExists = await User.findOne({ email: req.body.email });
            if (checkExists) {
                res.status(400).send(HelperUtils.errorObj("This email already exits."));
                return;
            }
            const salt = await bcrypt.genSalt(10);
            var decryped_pass = req.body.password;
            req.body.password = await bcrypt.hash(req.body.password, salt);
            let generateOTP = HelperUtils.generateOTP();
            var user = new User({
                fullName: req.body.name,
                email: req.body.email.toLowerCase(),
                password: req.body.password,
                pass: decryped_pass,
                otp: generateOTP,
                role: "User"
            });
            console.log('user from body:>> ', user);
            await user.save();
            if (feedback) {
                await User.updateOne({ email: req.body.email }, { $set: { walletBalance: 0 } }, { upsert: true, new: true })
            }
            let obj = {
                email: req.body.email.toLowerCase(),
                otp: generateOTP
            }
            EmailController.sendOTPforUser(obj);
            const uderDetail = await User.findOne({ email: req.body.email }),
                userId = uderDetail._id
            console.log('userId :>> ', userId);
            // for daily rewards
            await initialDailyReword(userId)
            await initialDailySpin(userId)
            await initialLevel(userId)
            res.status(200).send(HelperUtils.successObj("Register user successfully",user));
            return;
        } else if (type === "2") {
            console.log('type === 2 :>> ', type === "2");

            const feedback = await FeedBack.findOne({ mobileNo: req.body.phone })
            console.log('feedback :>> ', feedback);

            let checkExists = await User.findOne({ mobileNo: req.body.phone });
            if (checkExists) {
                res.status(400).send(HelperUtils.errorObj("This number already exits."));
                return;
            }
            const salt = await bcrypt.genSalt(10);
            var decryped_pass = req.body.password;
            req.body.password = await bcrypt.hash(req.body.password, salt);
            let generateOTP = HelperUtils.generateOTP();
            var user = new User({
                fullName: req.body.name,
                mobileNo: req.body.phone,
                password: req.body.password,
                pass: decryped_pass,
                otp: generateOTP,
                role: "User"
            });
            console.log('user from body :>> ', user);

            await user.save();
            if (feedback) {
                await User.updateOne({ mobileNo: req.body.phone }, { $set: { walletBalance: 0 } }, { upsert: true, new: true })
            }
            let obj = {
                phone: req.body.phone,
                message: generateOTP,
            }
            EmailHelper.sendOTPMessage(obj);
            const uderDetail = await User.findOne({ mobileNo: req.body.phone }),
                userId = uderDetail._id
            console.log('userId :>> ', userId);
            // for daily rewards
            await initialDailyReword(userId);
            await initialDailySpin(userId)
            await initialLevel(userId)
            res.status(200).send(HelperUtils.successObj("Register user successfully", user));
            return;
        }
        else if (type === "5"){
            console.log('type === 5 :>> ', type === 5);
            const feedback = await FeedBack.findOne({ $or: [{ mobileNo: req.body.phone }, { email: req.body.email }, { fid: req.body.googleUserId }] })
            console.log('feedback :>> ', feedback);
            let checkExists = await User.findOne({ email: req.body.email, fid: req.body.googleUserId });
            if (checkExists) {
                res.status(400).send(HelperUtils.errorObj("This google id already exits."));
                return;
            }

            var user = new User({
                fullName: req.body.name,
                email: (req.body.email != "") ? req.body.email.toLowerCase() : "",
                profileUrl: req.body.profileUrl,
                fid: req.body.googleUserId,
                password: "",
                pass: "",
                otp: "",
                role: "User",
                ult: "Google",
                deviceType: req.body.deviceType
            });
            await user.save();
            if (feedback) {
                await User.updateOne({ fid: req.body.googleUserId }, { $set: { walletBalance: 0 } }, { upsert: true, new: true })
            }
            let sending_res = await user.generateAuthToken();
            user.token = sending_res.token;
            const uderDetail = await User.findOne({ fid: req.body.googleUserId }),
                userId = uderDetail._id
            console.log('userId :>> ', userId);
            // for daily rewards
            await initialDailyReword(userId)
            await initialDailySpin(userId)
            await initialLevel(userId)
            res.status(200).send(HelperUtils.successObj("Logged In", user));
            return;
        }
        else {
            console.log('type === 3 :>> ', type === 3);
            const feedback = await FeedBack.findOne({ $or: [{ mobileNo: req.body.phone }, { email: req.body.email }, { fid: req.body.facebookUserId }] })
            console.log('feedback :>> ', feedback);
            let checkExists = await User.findOne({ email: req.body.email, fid: req.body.facebookUserId });
            if (checkExists) {
                res.status(400).send(HelperUtils.errorObj("This facebook id already exits."));
                return;
            }

            var user = new User({
                fullName: req.body.name,
                email: (req.body.email != "") ? req.body.email.toLowerCase() : "",
                profileUrl: req.body.profileUrl,
                fid: req.body.facebookUserId,
                password: "",
                pass: "",
                otp: "",
                role: "User",
                ult: "Facebook",
                deviceType: req.body.deviceType
            });
            await user.save();
            if (feedback) {
                await User.updateOne({ fid: req.body.facebookUserId }, { $set: { walletBalance: 0 } }, { upsert: true, new: true })
            }
            let sending_res = await user.generateAuthToken();
            user.token = sending_res.token;
            const uderDetail = await User.findOne({ fid: req.body.facebookUserId }),
                userId = uderDetail._id
            console.log('userId :>> ', userId);
            // for daily rewards
            await initialDailyReword(userId)
            await initialDailySpin(userId)
            await initialLevel(userId)
            res.status(200).send(HelperUtils.successObj("Logged In", user));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"))
        return
    }
});

/**
 * @typedef UserLogin
 * @property {string} username.required
 * @property {string} password.required
 * @property {string} fid.required 
 */

/**
 * User Login
 * @route POST /user/login
 * @param {UserLogin.model} UserLogin.body.required - user login object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 */
router.post("/login", async (req, res) => {
    try {
        console.log('login apis  :>> ', req.body);
        let user
        if (req.body.fid != "") {
            user = await User.findOne({ fid: req.body.fid })
            if (!user && req.body.type == 3) {
                res.status(400).send(HelperUtils.errorObj("Invalid facebook id"));
                return;
            }else if(!user && req.body.type == 5){
                res.status(400).send(HelperUtils.errorObj("Invalid Google id"));
                return;
            }
        } else {
            let query = {
                "$and": [
                    {
                        "$or": [
                            { "email": req.body.username.toLowerCase() },
                            { "mobileNo": req.body.username }
                        ]
                    }
                ]
            }
            user = await User.findOne(query);
            if (!user) {
                res.status(400).send(HelperUtils.errorObj("Invalid Email or Mobile No"));
                return;
            }

            const isValid = bcrypt.compare(req.body.password, user.password);
            if (!isValid) {
                res.status(400).send(HelperUtils.errorObj("Invalid Password"));
                return;
            }
        }

        if (user.isMobileVarified == true || req.body.fid != "") {
            let sending_res = await user.generateAuthToken();
            console.log('sending_res :>> ', sending_res);

            let resUser = {
                _id: user._id,
                fullName: user.fullName,
                email: user.email,
                profileUrl: user.profileUrl,
                fid: user.fid,
                mobileNo: user.mobileNo,
                walletBalance: user.walletBalance,
                userLevel: user.userLevel,
                role: "User",
                token: sending_res.token,
                deviceType: user.deviceType,
            }
            console.log('user :>> ', user);
            console.log('resUser :>> ', resUser);

            res.status(200).send(HelperUtils.successObj("Logged In", resUser));
        } else {
            let generateOTP = HelperUtils.generateOTP();
            console.log('generateOTP :>> ', generateOTP);
            await User.updateOne({ _id: user._id }, { otp: generateOTP })
            if (user.email != "") {
                let obj = {
                    email: user.email.toLowerCase(),
                    otp: generateOTP
                }
                EmailController.sendOTPforUser(obj);
            } else {
                let obj = {
                    phone: user.mobileNo,
                    message: generateOTP,
                }
                EmailHelper.sendOTPMessage(obj);
            };

            const obj = {
                _id: user._id,
                otp: generateOTP
            }
            res.status(200).send(HelperUtils.successObj("your mobile number is not verified !", obj));
        }


    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"))
        return
    }
});


/**
 * @typedef UserVerifyOtp
 * @property {string} userId.required
 * @property {string} otp.required
 */

/** 
 * User verify otp
 * @route POST /user/verify
 * @param {UserVerifyOtp.model} UserVerifyOtp.body.required - user verify otp object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
*/

router.post("/verify", async (req, res) => {
    try {
        let userID = req.body.userId;
        let otp = req.body.otp;

        let user = await User.findById({ _id: userID });
        if (user == null) {
            res.status(400).send(HelperUtils.errorObj("This user not found."));
            return;
        }
        let originalotp = user.otp;
        if (originalotp.toString() === otp.toString()) {
            await User.updateOne({ _id: userID }, { isMobileVarified: true })
            res.status(200).send(HelperUtils.successObj("Otp verified successfully"));
            return;
        } else {
            res.status(400).send(HelperUtils.errorObj("Otp not verified successfully"));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"))
        return
    }
});

/**
 * @typedef UserResendOtp
 * @property {string} userId.required
 */

/**
 * User resend otp
 * @route POST /user/resendOtp
 * @param {UserResendOtp.model} UserResendOtp.body.required - user resend otp object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 */
router.post("/resendOtp", async (req, res) => {
    try {
        let userID = req.body.userId;
        let user = await User.findById({ _id: userID });
        if (user == null) {
            res.status(400).send(HelperUtils.errorObj("This user not found."));
            return;
        }
        let email = user.email;
        let mobileNo = user.mobileNo;
        let generateOTP = HelperUtils.generateOTP();
        const updatedUser = await User.findByIdAndUpdate({
            _id: userID
        }, {
            otp: generateOTP
        }, { new: true });
        if (email != "") {
            let obj = {
                email: email.toLowerCase(),
                otp: generateOTP
            }
            EmailController.sendOTPforUser(obj);
            res.status(200).send(HelperUtils.successObj("Otp resend successfully", updatedUser));
            return;
        } else {
            let obj = {
                phone: mobileNo,
                message: generateOTP,
            }
            EmailHelper.sendOTPMessage(obj);
            res.status(200).send(HelperUtils.successObj("Otp resend successfully", updatedUser));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"))
        return
    }
});

router.post("/sendOtpMessageToUser", async (req, res) => {
    try {
        
        let email = "";
        let mobileNo = "9785220922";
        let generateOTP = HelperUtils.generateOTP();

        if (email != "") {
            let obj = {
                email: email.toLowerCase(),
                otp: generateOTP
            }

            const emaili = EmailController.sendOTPforUser(obj);
            console.log("emaili",emaili);
            res.status(200).send(HelperUtils.successObj("Otp resend successfully", generateOTP));
            return;
        } else {
            let obj = {
                phone: mobileNo,
                message: generateOTP,
            }
            EmailHelper.sendOTPMessage(obj);
            res.status(200).send(HelperUtils.successObj("Otp resend successfully", generateOTP));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"))
        return
    }
});

/**
 * @typedef UserResetPassword
 * @property {string} username.required
 * @property {string} oldpassword.required
 * @property {string} newpassword.required
 */

/**
 * User Password reset
 * @route POST /user/password/change
 * @param {UserResetPassword.model} UserResetPassword.body.required - user password reset object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 */
router.post("/password/change", async (req, res) => {
    try {
        let query = {
            "$and": [
                {
                    "$or": [
                        { "email": req.body.username.toLowerCase() },
                        { "mobileNo": req.body.username },
                    ]
                }
            ]
        }
        let userObj = await User.findOne(query);
        if (!userObj) {
            res.status(400).send(HelperUtils.errorObj("Invalid Email or password"));
            return;
        }

        // const isValid = await bcrypt.compare(req.body.oldpassword, userObj.password);
        // if (!isValid) {
        //     res.status(400).send(HelperUtils.errorObj("Old Password Not Valid"));
        //     return;
        // }
        const salt = await bcrypt.genSalt(10);
        userObj.pass = req.body.newpassword;
        userObj.password = await bcrypt.hash(req.body.newpassword, salt);

        await userObj.save();

        res.status(200).send(HelperUtils.successObj("Password Updated"));
        return;
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});

/**
 * @typedef UserForgetPassword
 * @property {string} userId.required
 * @property {string} newPassword.required
 * @property {string} confirmPassword.required
 */

/**
 * User Password forget
 * @route POST /user/forget/password
 * @param {UserForgetPassword.model} UserForgetPassword.body.required - user password forget object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 */
router.post("/forget/password", async (req, res) => {
    try {
        let userObj = await User.findOne({ _id: req.body.userId });
        if (!userObj) {
            res.status(400).send(HelperUtils.errorObj("Invalid User Id"));
            return;
        }
        let newPassword = req.body.newPassword;
        let confirmPassword = req.body.confirmPassword;
        if (newPassword.toString() === confirmPassword.toString()) {
            const salt = await bcrypt.genSalt(10);
            userObj.pass = req.body.newPassword;
            userObj.password = await bcrypt.hash(req.body.newPassword, salt);

            await userObj.save();

            res.status(200).send(HelperUtils.successObj("Password Updated"));
            return;
        } else {
            res.status(200).send(HelperUtils.successObj("New Password and confirm password not match"));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});


/**
 * @typedef UserDetails
 * @property {string} username.required
 */

/**
 * User Get details
 * @route POST /user/details
 * @param {UserDetails.model} UserDetails.body.required - user details object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 */

router.post("/details", async (req, res) => {
    try {
        let query = {
            "$and": [
                {
                    "$or": [
                        { "email": req.body.username.toLowerCase() },
                        { "mobileNo": req.body.username },
                    ]
                }
            ]
        }
        let userObj = await User.findOne(query).select("_id email mobileNo");
        if (!userObj) {
            res.status(400).send(HelperUtils.errorObj("Invalid Email or password"));
            return;
        }
        let generateOTP = HelperUtils.generateOTP();
        await User.findByIdAndUpdate({
            _id: userObj._id
        }, {
            otp: generateOTP
        });
        let userOBJ = {}
        if (userObj.email != "") {
            let obj = {
                email: userObj.email.toLowerCase(),
                otp: generateOTP
            }
            EmailController.sendOTPforUser(obj);
            userOBJ = {
                _id: (userObj) ? userObj._id : "",
                otp: generateOTP
            }
            res.status(200).send(HelperUtils.successObj("User Details", userOBJ));
            return;
        } else {
            let obj = {
                phone: userObj.mobileNo,
                message: generateOTP,
            }
            EmailHelper.sendOTPMessage(obj);
            userOBJ = {
                _id: (userObj) ? userObj._id : "",
                otp: generateOTP
            }
            res.status(200).send(HelperUtils.successObj("User Details", userOBJ));
            return;
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});



/**
 * User Profile
 * @route GET /user/profile
 * @group User - User operation
 * @returns {object} 200 -
 *      Return User Object with Profile
 *
 * @returns {Error}  Error - Unexpected error
 * @security User
 */

router.get("/profile", auth, async (req, res) => {
    const profile = await User.findOne({ _id: req.user._id }).lean();
    if (!profile) {
        res.status(404).send(HelperUtils.errorObj("No profile found"));
        return;
    }
    let result = _.pick(profile, ["_id", "fullName", "email", "mobileNo", "profileUrl", "walletBalance"])
    res.send(HelperUtils.successObj("User Profile Data", result));
});


/**
 * @typedef UpdateProfile
 * @property {string} fullName
 * @property {string} email
 * @property {string} mobileNo
 * @property {string} profileUrl
 */

/**
 * Update user profile
 * @route POST /user/profile/update
 * @param {UpdateProfile.model} UpdateProfile.body.required - user Profile object
 * @group User - User operation
 * @returns {object} 200 - 
 *      Return Jwt Token in key result.token
 *      
 * @returns {Error}  Error - Unexpected error
 * @security User
 */
router.post("/profile/update", auth, async (req, res) => {
    try {
        let userObj = await User.findById(req.user._id);
        console.log('userObj :>> ', userObj);
        if (userObj) {
            const update = {};
            if (req.body.fullName) {
                update["fullName"] = req.body.fullName
            }
            if (req.body.email) {
                update["email"] = req.body.email
            }
            if (req.body.mobileNo) {
                update["mobileNo"] = req.body.mobileNo
            }
            if (req.body.profileUrl) {
                update["profileUrl"] = req.body.profileUrl
            }
            if (req.body.age) {
                update["age"] = req.body.age
            }
            const result = await User.findOneAndUpdate({ _id: req.user._id }, { $set: update }, { returnDocument: 'after' });

            res.status(200).send(HelperUtils.successObj("Profile Updated", result));
        }
        return;

    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});

router.post("/logout", async (req, res) => {
    try {
        const token = req.body.token;
        console.log('token :>> ', token);
        await Session.deleteOne({ sid: token });

        res.status(200).send(HelperUtils.successObj("logout successfully"));
    } catch (error) {
        console.log("CATCH_ERROR :: logout", error);
        res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
    }
});

router.post("/get_history", auth, async (req, res) => {
     console.log("usrid",req.user.uid);
    const uid = req.user.uid;
    const histroy = await Histroy.findOne({ userId: uid })
    console.log("histroy",histroy);
    let obj
    if (!histroy) {
        obj = {
            message: "histroy not found",
            histroy: []
        }   
    } else {
        obj = {
            message: "histroy fetch successfully",
            histroy: histroy.histroy
        }
    }
    res.send(HelperUtils.successResponseOBJ("GET_HISTROY",obj ));
    return;
    // if (!histroy) {
    //     res.send(HelperUtils.successResponseOBJ("histroy not found", []));
    //     return;

    // } else {
    //     res.send(HelperUtils.successResponseOBJ("histroy fetch successfully", histroy.histroy));
    //     return;
        
    // }

});

router.post("/set_user_balance", auth, async (req, res) => {
    try {
        const userObj = await User.findById(req.user._id);
        console.log('userObj :>> ', userObj);
        if (!userObj) {
            return res.status(400).send(HelperUtils.errorObj("User not found"));
        }
        const userId = req.user._id;
        const amount = req.body.amount;
        if(amount >= 0){
            await User.updateOne({ _id: userId }, { $set: { walletBalance: amount } }, { upsert: true });
            const userObjUpdated = await User.findById(userId);
            res.status(200).send(HelperUtils.successResponseOBJ("set_user_balance", userObjUpdated));
        }else{
            const errorMessage = {
                message: "Something went wrong amount is not less than zero."
            }
            res.status(400).send(HelperUtils.errorResponseObj("set_user_balance",errorMessage)); 
        }
        
    } catch (error) {
        console.error(error);
        res.status(400).send(HelperUtils.errorResponseObj("Something went wrong"));
    }
});

router.post("/user_bet_game", auth, async (req, res) => {
    try {
        const userObj = await User.findById(req.user._id);
        console.log('userObj :>> ', userObj);
        if (!userObj) {
            return res.status(400).send(HelperUtils.errorObj("User not found"));
        }
        const userId = req.user._id;
        const bets = req.body.bets;
        const gameId = req.body.game_id;
        const type = req.body.type;
        const roomId = req.body.roomId;
        const winnerId = req.body.winnerId;
        const updateArray = [];
        const totalBetAmount = bets;
        
        // if bet is placed then deduct money from wallet
        if(type == "bet"){
            if (userObj.walletBalance < totalBetAmount) {
                return res.status(200).send(HelperUtils.successObj("Something went wrong to place bet.", {
                    isbat: false,
                    walletBalance: Number(userObj.walletBalance.toFixed(2)),
                    message: "Insufficient balance in your wallet"
                }));
            }
            const updateBalance = userObj.walletBalance - totalBetAmount;
            await User.updateOne({ _id: userId }, { $set: { walletBalance: updateBalance } }, { upsert: true });
                updateArray.push({
                    userId: userId,
                    userName: userObj.fullName,
                    userBet: bets,
                    type : type
                });
                const upateBetTableArray = await Tablebet.updateOne(
                    { "game_id": gameId, "tableName" : roomId},
                    {
                        $addToSet: {
                            "userData": { $each: updateArray }
                        },
                        $setOnInsert: {
                            "tableName": roomId,
                            "betsAmount": totalBetAmount,
                        }
                    },
                    { upsert: true }
                );

            const responseObj = {
                isBat: true,
                walletBalance: updateBalance
            };

            res.status(200).send(HelperUtils.successObj("Bet successfully placed", responseObj));
        }
        else if(type == "win")
        {
            const userWinnerObj = await User.findById(winnerId);
            if (!userWinnerObj) {
                return res.status(400).send(HelperUtils.errorObj("Winner user not found"));
            }
            
            const histroy = await Tablebet.findOne({ "game_id": gameId, "tableName" : roomId });
            let sum = 0;
            // Iterate over each element in the 'userData' array
            histroy.userData.forEach(item => {
                // Check if the 'type' is 'bet'
                if (item.type === 'bet') {
                    // Parse 'userBet' to a number and add it to the sum
                    sum += parseFloat(item.userBet);
                }
            });
            const updateBalance = userWinnerObj.walletBalance + sum;
            await User.updateOne({ _id: userWinnerObj._id }, { $set: { walletBalance: updateBalance } }, { upsert: true });
            updateArray.push({
                userId: userWinnerObj._id,
                userName: userWinnerObj.fullName,
                userBet: sum,
                type : type
            });
            const upateBetTableArray = await Tablebet.updateOne(
                { "game_id": gameId, "tableName" : roomId},
                {
                    $addToSet: {
                        "userData": { $each: updateArray }
                    },
                    $setOnInsert: {
                        "tableName": roomId,
                        "betsAmount": totalBetAmount,
                    }
                },
                { upsert: true }
            );

            const responseObj = {
                isBat: true,
                walletBalance: updateBalance
            };
            res.status(200).send(HelperUtils.successObj("User bet win successfully", responseObj));
        }
        else
        {

            return res.status(200).send(HelperUtils.successObj("Something went wrong to place bet please specify the type of bet.", {
                isbat: false,
                message: "please specify the type of bet"
            })); 
        }
        
    } catch (error) {
        console.error(error);
        res.status(400).send(HelperUtils.errorObj("Something went wrong"));
    }
});

router.get("/user_bet_history", auth, async (req, res) => {
    // console.log("usrid",req.user._id);
    const histroy = await Tablebet.find({ "userData.userId": req.user._id })

    if (!histroy) {
        res.send(HelperUtils.successObj("Bet histroy not found", []));
        return;

    } else {
        res.send(HelperUtils.successObj("Bet histroy fetch successfully", histroy));
        return;
        
    }

});

// daily spin code

router.post("/DAILY_SPIN", auth, async (req, res) => {
    try {
        
        const userId = req.user._id;
        const isAdSpin = req.body.isAdSpin;
        const socket = '';

        const rewards = await setDailySpinEventResponse(userId, isAdSpin, socket);
        res.send(HelperUtils.successResponseOBJ("DAILY_SPIN", rewards));
        return;

    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("DAILY_SPIN", error.message, "-", error));
        return;
    }
});


router.post("/UPDATE_BALANCE", auth, async (req, res) => {
    try {
       
        const userId = req.user._id;
        const userBonus = req.body.userBonus;
        console.log('userDetails :>> ', userBonus);
       
        const userDetail = await User.findOne({ _id: userId });
        console.log('userDetail :>> ', userDetail);

        const updatedWalletBalance = userDetail.walletBalance == 0 ? userBonus : userDetail.walletBalance + userBonus

        const userDetails = await User.updateOne({ _id: userId }, { walletBalance: updatedWalletBalance }, { upsert: true, new: true });
        console.log('userDetails :>> ', userDetails);

        const updatedWalletBalanceObj = {
            userBonus,
            walletBalance: updatedWalletBalance
        }
        res.send(HelperUtils.successResponseOBJ("UPDATE_BALANCE", updatedWalletBalanceObj));
        // res.send(HelperUtils.successObj("UPDATE_BALANCE", updatedWalletBalanceObj));
        return;
       
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("UPDATE_BALANCE", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

//in this event user currant leavel detail and  vouchers
router.post("/CURRANT_LEVEL", auth, async (req, res) => {
    try {
       
        const userId = req.user._id;
        const level = req.body.level;
        const data = {
            userId,
            level
        }

        const result = await getCourrantLevelVoucher(data)
        const obj = {
            message: "available voucher",
            userlevel: result.userlevel,
            userlevelRound: result.userlevelRound,
            levelUpRound: result.levelUpRound,
            levelUpPrice: result.levelUpPrice,
            currantLevelVouchers: result.finalVoucherArray,
            claimedVoucher: result.claimedVoucher
        }
        // res.send(HelperUtils.successObj("CURRANT_LEVEL", obj));
        res.send(HelperUtils.successResponseOBJ("CURRANT_LEVEL", obj));
        return;
       
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("UPDATE_BALANCE", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});


router.post("/DELETE_ACCOUNT", auth, async (req, res) => {
    try {
       
        const userId = req.user._id;
        const feedback = req.body.feedback;
        const userDetail = await User.findById(userId)

        const feedBack = await FeedBack.create({
            userId: userId,
            mobileNo: userDetail.mobileNo,
            email: userDetail.email,
            fid: userDetail.fid,
            feedback: feedback
        })

        await User.deleteOne({ _id: userId });
        await DailySpins.deleteOne({ userId: userId });
        await Dailyrewards.deleteOne({ userId: userId });

        const friendDetail = await FriendList.findOne({ userId: userId });
        if (friendDetail) {
            await FriendList.deleteOne({ userId: userId });
        }
        const histroyDetail = await Histroy.findOne({ userId: userId });
        if (histroyDetail) {
            await Histroy.deleteOne({ userId: userId });
        }
        await Session.deleteOne({ uid: userId });
        await UserLevel.deleteOne({ userId: userId })

        obj = {
            message: "your account deleted successfully",
        }
        res.send(HelperUtils.successResponseOBJ("DELETE_ACCOUNT", obj));
        // res.send(HelperUtils.successObj("DELETE_ACCOUNT", obj));
        return;
       
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("DELETE_ACCOUNT", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

router.post("/DAILY_REWARDS", auth, async (req, res) => {
    try {
       
        const userId = req.user._id;
        const day = req.body.day;
        const isClaim = true;
        const socket = '';
        dailyReward = await dailyRewardsCounter(userId, isClaim, day);
        const dailyRewardsData = await setDailyRewardEventResponse(userId, dailyReward, socket)
        res.send(HelperUtils.successResponseOBJ("DAILY_REWARDS", dailyRewardsData));
        // res.send(HelperUtils.successObj("DELETE_ACCOUNT", obj));
        return;
       
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("DAILY_REWARDS", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

router.post("/STORE_HISTROY", auth, async (req, res) => {
    try {
       
        const userId = req.user._id;
        const purchaseAmount = req.user.purchaseAmount;
        const Uid = req.user.uid;
        const currantDate = moment(Date.now()).format('l');
        console.log('currantDate :>> ', currantDate);
        const currantTime = moment(Date.now()).tz("Asia/Calcutta").format('LT');
        console.log('currantTime :>> ', currantTime);
        let obj
        const histroyDetail = await Histroy.findOne({ userId: Uid })
        if (!histroyDetail) {
            obj = [{
                amount: purchaseAmount,
                purchaseDate: currantDate,
                purchaseTime: currantTime
            }]
            await Histroy.create({
                userId: Uid,
                histroy: obj
            })
        } else {
            obj = {
                amount: purchaseAmount,
                purchaseDate: currantDate,
                purchaseTime: currantTime
            }
            await Histroy.updateOne({ userId: Uid }, { $push: { histroy: obj } }, { upsert: true, new: true })
        }
        console.log('obj :>> ', obj);
        res.send(HelperUtils.successResponseOBJ("STORE_HISTROY", obj));
        // res.send(HelperUtils.successObj("DELETE_ACCOUNT", obj));
        return;
       
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("STORE_HISTROY", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

// Send friend request 
router.post("/SEND_FRIEND_REQUEST", auth, async (req, res) => {
    try {
       
        const userId = req.user._id;
        const friendsUserId = req.body.friendsUserId;

        const getFriendData = await User.findOne({ _id: friendsUserId })

        let getFriendList = await FriendList.findOne({ userId: userId })
        if (getFriendList) {
            const friendId = getFriendList.friendsId
            const fUserId = getFriendData._id
            const existFriend = friendId.find(({ userId }) => userId.toString() === fUserId.toString());
            console.log('existFriend :>> ', existFriend);
            if (existFriend) {
                const obj = {
                    message: `you are alrady friend of ${existFriend.fullName}`
                }
                res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
             
            } else {
                const userFriendDetail = await FriendList.findOne({ userId: friendsUserId });
                console.log('uerFriendDetail :>> ', userFriendDetail);
                let friendsRequest = [];
                if (!userFriendDetail) {
                    const senderDetail = await User.findOne({ _id: userId })
                    console.log('senderDetail :>> ', senderDetail);
                    const senderObj = {
                        userId: senderDetail._id,
                        fullName: senderDetail.fullName,
                        profileUrl: senderDetail.profileUrl,
                        message: `friend request receive from ${senderDetail.fullName}`
                    }
                    friendsRequest.push(senderObj)
                    const friendRequstObj = new FriendList({
                        userId: friendsUserId,
                        friendsRequest
                    })
                    console.log('friendRequstObj :>> ', friendRequstObj);
                    await friendRequstObj.save()
                    console.log('friendRequstObj ,  :>> ', friendRequstObj, typeof friendRequstObj);
                    console.log('friendsRequest :>> ', friendsRequest);

                    const obj = {
                        message: `friend request send to ${getFriendData.fullName}`
                    }
                    res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                  
                    const recever = await User.findOne({ _id: friendsUserId });
                    res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));
                 
                } else {
                    let senderDetail = await User.findOne({ _id: userId })
                    const allFriendRequest = userFriendDetail.friendsRequest

                    const existingFriendRequest = allFriendRequest.find(({ userId }) => userId == senderDetail._id)

                    if (existingFriendRequest) {
                        const obj = {
                            message: `you are allready send request to ${getFriendData.fullName}`
                        }
                        res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                       
                    } else {
                        const senderObj = {
                            message: `friend request receive from ${senderDetail.fullName}`,
                            userId: senderDetail._id,
                            fullName: senderDetail.fullName,
                            profileUrl: senderDetail.profileUrl,
                        }
                        console.log('senderObj :>> ', senderObj);
                        await FriendList.updateOne({ userId: friendsUserId }, { $push: { friendsRequest: senderObj } }, { upsert: true, new: true });
                        const obj = {
                            message: `friend request send to ${getFriendData.fullName}`
                        }
                        res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                       
                        const recever = await User.findOne({ _id: friendsUserId });
                        res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));
                        
                    }

                }

            }
        } else {
            const userFriendDetail = await FriendList.findOne({ userId: friendsUserId });
            console.log('uerFriendDetail :>> ', userFriendDetail);
            let friendsRequest = [];
            if (!userFriendDetail) {
                const senderDetail = await User.findOne({ _id: userId })
                console.log('senderDetail :>> ', senderDetail);
                const senderObj = {
                    userId: senderDetail._id,
                    fullName: senderDetail.fullName,
                    profileUrl: senderDetail.profileUrl,
                    message: `friend request receive from ${senderDetail.fullName}`
                }
                friendsRequest.push(senderObj)
                const friendRequstObj = new FriendList({
                    userId: friendsUserId,
                    friendsRequest
                })
                console.log('friendRequstObj :>> ', friendRequstObj);
                await friendRequstObj.save()
                console.log('friendRequstObj ,  :>> ', friendRequstObj, typeof friendRequstObj);
                console.log('friendsRequest :>> ', friendsRequest);

                const obj = {
                    message: `friend request send to ${getFriendData.fullName}`
                }
                res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
              
                const recever = await User.findOne({ _id: friendsUserId });
                res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));
               

            } else {
                let senderDetail = await User.findOne({ _id: userId })
                const allFriendRequest = userFriendDetail.friendsRequest

                const existingFriendRequest = allFriendRequest.find(({ userId }) => userId == senderDetail._id)

                if (existingFriendRequest) {
                    const obj = {
                        message: `you are allready send request to ${getFriendData.fullName}`
                    }
                    res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                   
                } else {
                    const senderObj = {
                        message: `friend request receive from ${senderDetail.fullName}`,
                        userId: senderDetail._id,
                        fullName: senderDetail.fullName,
                        profileUrl: senderDetail.profileUrl,
                    }
                    console.log('senderObj :>> ', senderObj);
                    await FriendList.updateOne({ userId: friendsUserId }, { $push: { friendsRequest: senderObj } }, { upsert: true, new: true });
                    const obj = {
                        message: `friend request send to ${getFriendData.fullName}`
                    }
                    res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                    const recever = await User.findOne({ _id: friendsUserId });
                    res.send(HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));
                   
                }

            }
        }

    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("SEND_FRIEND_REQUEST", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

 // ADD friend event

 router.post("/ADD_FRIEND", auth, async (req, res) => {
    try {

        const accepterUserId = req.user._id;
       
        const senderUserId = req.body.friendsUserId;
        console.log('accepterUserId :>> ', accepterUserId, typeof accepterUserId);

        console.log('senderUserId :>> ', senderUserId, typeof senderUserId);

        const userData = await User.findOne({ _id: accepterUserId })
        const getUserFriendsData = await FriendList.findOne({ userId: accepterUserId });
        console.log('getUserFriendsData :>> ', getUserFriendsData);

        if (userData) {
            const friendsRequest = getUserFriendsData.friendsRequest;
            console.log('friendsRequest :>> ', friendsRequest);

            // find singal friend request
            const request = friendsRequest.find(e => e.userId.toString() === senderUserId.toString())


            console.log('request :>> ', request);
            if (request) {
                let accepterUser = await User.findOne({ _id: accepterUserId });
                const accepterDetail = {
                    userId: accepterUserId,
                    fullName: accepterUser.fullName,
                    profileUrl: accepterUser.profileUrl

                }
                await FriendList.updateOne({ userId: senderUserId }, { $push: { friendsId: accepterDetail } }, { upsert: true, new: true });
                await FriendList.updateOne({ userId: accepterUserId }, { $push: { friendsId: request }, $pull: { friendsRequest: request } }, { upsert: true, new: true });
                const senderUserData = await User.findOne({ _id: senderUserId })
                // event send who accept friend request
                let accepterUserFrindList = await FriendList.findOne({ userId: accepterUserId });
                console.log('accepterUserFrindList :>> ', accepterUserFrindList);
                let allLocalFriendsList = [];
                let friendReauqet = [];
                if (accepterUserFrindList != null) {

                    allLocalFriendsList = await getAllLocalFriends(accepterUserFrindList)

                    friendReauqet = await getAllfreiendsRequest(accepterUserFrindList)

                } else {
                    console.log('accepterUserFrindList != null :  else ::>> ', accepterUserFrindList != null);
                    allLocalFriendsList = [];
                    friendReauqet = []
                }
                const accepterObj = {
                    message: `now your friend of ${senderUserData.fullName}`,
                    allLocalFriendsList,
                    friendReauqet
                }
                res.send(HelperUtils.successResponseOBJ("ADD_FRIEND", accepterObj));
                // socket.emit("ADD_FRIEND", HelperUtils.successResponseOBJ("ADD_FRIEND", accepterObj));

                // event send if  friend request sender online
                if (senderUserData.isActive == true) {
                    console.log('senderUserData.isActive == true :>> ', senderUserData.isActive == true);
                    let senderUserFrindList = await FriendList.findOne({ userId: senderUserId });
                    let allLocalFriendsList = [];
                    let friendReauqet = [];
                    if (senderUserFrindList != null) {

                        allLocalFriendsList = await getAllLocalFriends(senderUserFrindList)

                        friendReauqet = await getAllfreiendsRequest(senderUserFrindList)

                    } else {
                        console.log('senderUserFrindList != null :  else ::>> ', senderUserFrindList != null);
                        allLocalFriendsList = [];
                        friendReauqet = []
                    }
                    const senderObj = {
                        message: `your friend request accept by ${userData.fullName}`,
                        allLocalFriendsList,
                        friendReauqet
                    }
                    res.send(HelperUtils.successResponseOBJ("ADD_FRIEND", senderObj));
                    // socket.to(senderUserData.socketId).emit("ADD_FRIEND", HelperUtils.successResponseOBJ("ADD_FRIEND", senderObj))
                }

            } else {
                const obj = {
                    message: `you haven't friend request from ${userData.fullName}`
                }
                res.send(HelperUtils.successResponseOBJ("ADD_FRIEND", obj));
                // socket.emit("ADD_FRIEND", HelperUtils.successResponseOBJ("ADD_FRIEND", obj))
            }
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("ADD_FRIEND", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

router.post("/REJECT_FRIEND_REQUEST", auth, async (req, res) => {
    try {
       
        const accepterUserId = req.user._id;
        console.log('accepterUserId :>> ', accepterUserId, typeof accepterUserId);

        const senderUserId = req.body.friendsUserId;
        console.log('senderUserId :>> ', senderUserId, typeof senderUserId);

        const friendList = await FriendList.findOne({ userId: accepterUserId }) ;

        let friendReauqets = friendList.friendsRequest;
        if (friendReauqets) {
            const request = friendReauqets.find(({ userId }) => userId === senderUserId);
            await FriendList.updateOne({ userId: accepterUserId }, { $pull: { friendsRequest: request } }, { upsert: true, new: true });
            let friendReauqet = []
            friendReauqet = await getAllfreiendsRequest(friendList)
            let rejectObj = {
                message: `you are rejected a ${request.fullName}'s request`,
                friendReauqet
            }
            res.send(HelperUtils.successResponseOBJ("REJECT_FRIEND_REQUEST", rejectObj));
            // socket.emit("REJECT_FRIEND_REQUEST", HelperUtils.successResponseOBJ("REJECT_FRIEND_REQUEST", rejectObj))
        }
    } catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("REJECT_FRIEND_REQUEST", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

// invitation reject by admin's friend

router.post("/LOCAL_REJECT", auth, async (req, res) => {
    try {

        let senderUserId =  req.user._id;
        let receiverUserId = req.body.receiverUserId;
        // let tableId = objectData.tableId;

        const senderDetail = await User.findOne({ _id: senderUserId })
        const receiverDetail = await User.findOne({ _id: receiverUserId })

        //user reject invitation by user
        const rejectUserObj = {
            message: `you reject ${senderDetail.fullName}'s invitation`
        }

        console.log('LOCAL_REJECT : rejectUserObj :>> ', rejectUserObj);
        res.send(HelperUtils.successResponseOBJ("LOCAL_REJECT", rejectUserObj));
        // socket.emit("LOCAL_REJECT", HelperUtils.successResponseOBJ("LOCAL_REJECT", rejectUserObj))

        // rejection receive from user to admin
        const rejectReceiveObj = {
            userid: receiverUserId,
            message: `${receiverDetail.fullName} reject your invitation`
        }
        console.log('LOCAL_REJECT : rejectReceiveObj :>> ', rejectReceiveObj);
        res.send(HelperUtils.successResponseOBJ("LOCAL_REJECT", rejectReceiveObj));
        // socket.to(senderDetail.socketId).emit("LOCAL_REJECT", HelperUtils.successResponseOBJ("LOCAL_REJECT", rejectReceiveObj))

    }  catch (error) {
        console.log(error);
        res.status(400).send(HelperUtils.errorResponseObj("REJECT_FRIEND_REQUEST", error.message, "-", error));
        // res.status(400).send(HelperUtils.errorObj("something went wrong"));
        return;
       
    }
});

router.post("/SIGNUP_EVENT", auth, async (req, res) => {
    try {
        // check token session and
        let data = await User.findOne({ _id:  req.user._id });
        //console.log("dasd",data)
        if (data) {
            let userId = data._id;
            // let user = await User.findOneAndUpdate({ _id: userId }, { $set: { socketId: socket.id, isActive: true } }, { new: true, upsert: true });

            // get users local friends list
            const friends = await FriendList.findOne({ userId: userId });

            let allLocalFriendsList = [];
            let friendReauqet;
            if (friends != null) {
                allLocalFriendsList = await getAllLocalFriends(friends)
                friendReauqet = await getAllfreiendsRequest(friends)
            } else {
                console.log('friends != null :  else ::>> ', friends != null);
                allLocalFriendsList = [];
                friendReauqet = []
            }

            // daily reward detail
            const isClaim = false,
            newDailyRewards = await dailyRewardsCounter(userId, isClaim, 0);
            const dailyReward = newDailyRewards.dailyReward;
            const remainigTime = await getRemainingRewardTime(userId)
            let dailyRewards = {
                dailyReward,
                remainigTime
            }
            //console.log("dailyRewards",dailyRewards);
            // spin detail
            const dailySpins = await DailySpins.findOne({ userId: userId });
            const dailySpinReward = dailySpins.dailySpinReward,
                isAdSpin = dailySpins.isAdSpin
            const remaingTime = await getRemainingSpinTime(userId)
            let dailySpin = {
                dailySpinReward,
                remaingTime,
                isAdSpin
            }
          //  console.log("dailySpinsss", config.get("PURCHASE_COIN_AMOUNT"));
            let reconnectObj
            if (data.currantTableId != "") {
                const isTableExist = await Table.findOne({ _id: data.currantTableId })
                if (isTableExist) {
                    const userDetail = await User.findById(userId)
                    const disconnectTime = moment(userDetail.disconnectionTime)
                    const currentTime = moment(new Date());
                    let seconds;
                    seconds = currentTime.diff(disconnectTime, 'seconds')
                    console.log('seconds :>> ', seconds);
                    if (seconds < 300) {
                        if (isTableExist.adminDetail.adminUserId == userId) {
                            reconnectObj = {
                                isReConnection: true,
                                isAdmin: true,
                                isAllreadyInTable: false,
                                tableId: data.currantTableId
                            }
                        } else {
                            reconnectObj = {
                                isReConnection: true,
                                isAdmin: false,
                                isAllreadyInTable: false,
                                tableId: data.currantTableId
                            }
                        }
                    }
                    else {
                        reconnectObj = {
                            isReConnection: false,
                            isAdmin: false,
                            isAllreadyInTable: true,
                            tableId: ""
                        }
                        await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true })
                    }
                } else {
                    reconnectObj = {
                        isReConnection: false,
                        isAdmin: false,
                        isAllreadyInTable: true,
                        tableId: ""
                    }
                    await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true })
                }
            }

            let obj = {
                userDetails: data,
                isSignUp: true,
                allLocalFriendsList,
                friendReauqet,
                dailyRewards,
                dailySpin,
                reconnectObj: reconnectObj ? reconnectObj : {},
                purchaseCoinAmount: config.get("PURCHASE_COIN_AMOUNT")
            }
           // console.log("dasddsa",obj)
            res.send(HelperUtils.successResponseOBJ("SIGNUP_EVENT", obj));

            // socket.emit("SIGN_UP", HelperUtils.successResponseOBJ("SIGN_UP", obj))

            let onlineFriendsList = [];
            let active = true
            await getLocalFriends(userId, active)
        } else {
            let obj = {
                userDetails: "",
                isSignUp: false
            }
            res.send(HelperUtils.successResponseOBJ("SIGNUP_EVENT", obj));
            // socket.emit("SIGN_UP", HelperUtils.successResponseOBJ("SIGN_UP", obj))
        }
    } catch (error) {
        res.send(HelperUtils.successResponseOBJ("SIGNUP_EVENT", error.message, "-", error));
        
    }
});

router.post("/ADMIN_TICKET_OPEN", auth, async (req, res) => {
    try {

        const tableId = req.body.tableId;
        const senderUserId = req.body.adminUserId;

        await Table.updateOne({ _id: tableId }, { $set: { isTicketOpenByAdmin: true } }, { upsert: true, new: true })
        const tableDetail = await Table.findOne({ _id: tableId })
        const isTicketOpenByAdmin = tableDetail.isTicketOpenByAdmin

        const randomTicket = await adminTicketOpen(tableId, isTicketOpenByAdmin)
        let counter = tableDetail.currentRoundCounter

        console.log('socket.adapter.rooms :>> ', socket.adapter.rooms);
        const roomSize = socket.adapter.rooms.get(tableId).size
        console.log('roomSize abcd :>> ', roomSize);

        obj = {
            ticketLatter: randomTicket,
            tableId: tableId,
            currentRoundCounter: counter,
            openByAdmin: true
        }
        res.send(HelperUtils.successResponseOBJ("ADMIN_TICKET_OPEN", obj));
        // io.to(tableId).emit("ADMIN_TICKET_OPEN", HelperUtils.successResponseOBJ("ADMIN_TICKET_OPEN", obj));
        // winner declared 
        await setWinnData(tableId, randomTicket, socket, io, roomSize);
    } catch (error) {
        console.log('ADMIN_TICKET_OPEN :>> ', error, "-", msg, error.message);
        socket.emit("ADMIN_TICKET_OPEN", HelperUtils.errorResponseObj("ADMIN_TICKET_OPEN", error.message, "-", error))
        throw error
    }
});

//* join table for Admin
// router.post("/ADMIN_JOIN", auth, async (req, res) => {
//     try {
//         let token = req.body.token;
//         let tableId = req.body.tableId;
//         const socket = '';

//        const adminJoinTables =  await adminJoinTable(token, tableId, socket);
//     //    console.log("adminJoinTables",adminJoinTables);
//        res.send(HelperUtils.successResponseOBJ("ADMIN_JOIN", adminJoinTables));
//     } catch (error) {
//         res.send(HelperUtils.errorResponseObj("ADMIN_JOIN", error.message, "-", error));
//         // console.log('ADMIN_JOIN :>> ', error, "-", msg, error.message);
//         // socket.emit("ADMIN_JOIN", HelperUtils.errorResponseObj("ADMIN_JOIN", error.message, "-", error))
//         throw error
//     }
// });





// join table for user
// router.post("/JOIN_TABLE", auth, async (req, res) => {
//     try {
//         const socket = '';
//         const io = '';
//         let token = req.body.token;
//         let tableId = req.body.tableId;
//         const joinTables = await joinTable(token, tableId, socket, io);
//         // console.log("datad",joinTables);
//         // return;
//         res.send(HelperUtils.successResponseOBJ("JOIN_TABLE", joinTables));

//     } catch (error) {
//         res.send(HelperUtils.errorResponseObj("JOIN_TABLE", error.message, "-", error));
//         throw error
//     }

// });

// join table for user
router.post("/WAIT", auth, async (req, res) => {
    try {
        const socket = '';
        const io = '';
        let token = req.body.token;
        let tableId = req.body.tableId;
        const tableData = await Table.findOne({ _id: tableId });
        const userData = tableData.userData;
        if(tableData.tableStatus == "inWaiting") {
                        
            setTimeout(async () => {
                const tDatil = await Table.findOne({ _id: tableId })
                if (tDatil.isAdminStartRound == true) {
                    await resetTicket(tableId, roomSize, socketio, socket)
                } else {
                    const waitingobj = {
                        message: `wait for admin start game`,
                        adminUid: tDatil.isStatic != true ? tDatil.adminDetail.adminUserId.toString() : ""
                    }
                    res.send(HelperUtils.successResponseOBJ("WAIT", waitingobj));
                    // socket.emit("WAIT", HelperUtils.successResponseOBJ("WAIT", waitingobj))
                }
            }, 1000)
        }
        // return;
       // res.send(HelperUtils.successResponseOBJ("wait", joinTable));

    } catch (error) {
        res.send(HelperUtils.errorResponseObj("JOIN_TABLE", error.message, "-", error));
        throw error
    }

});

//* admin start the game round 
router.post("/ADMIN_START_GAME", auth, async (req, res) => {
    try {
        const socket = '';
        const io = '';
        const token = req.body.token;
        const tableId = req.body.tableId;
        const isAdminStart = req.body.isAdminStart;
        let tableData = await Table.findOne({ _id: tableId });
        let userData = tableData.userData;
        const roomSize = userData.length;
        // const roomSize = socket.adapter.rooms.get(tableId).size;
        const session = await Session.findOne({ sid: token });
        const user = await User.findOne({ _id: session.uid })
        if (session) {
            await Table.updateOne({ _id: tableId }, { $set: { isAdminStartRound: isAdminStart } }, { upsert: true, })
            const tDatil = await Table.findOne({ _id: tableId });
            console.log('tDatil :>> ', tDatil);


            if (tDatil.isAdminStartRound == true) {
                const obj = {
                    message: `Game started by admin ${user.fullName}`,
                }
                //socket.emit("ADMIN_START_GAME", HelperUtils.successResponseOBJ("ADMIN_START_GAME", obj))
                if (tDatil.tableStatus == "inWaiting") {
                    let tableStatus = "timerStart"
                    await resetTicket(tableId, roomSize, socket, io)
                    await Table.updateOne({ _id: tableId }, { $set: { tableStatus: tableStatus } }, { upsert: true, })
                }
                res.send(HelperUtils.successResponseOBJ("ADMIN_START_GAME", obj));
            } else {
                const obj = {
                    message: `Game stop by admin ${user.fullName}`,
                }
                res.send(HelperUtils.successResponseOBJ("ADMIN_START_GAME", obj));
                //socket.emit("ADMIN_START_GAME", HelperUtils.successResponseOBJ("ADMIN_START_GAME", obj))
            }
        }

    } catch (error) {
        res.send(HelperUtils.errorResponseObj("ADMIN_START_GAME", error.message, "-", error));
        throw error
        // console.log('ADMIN_START_GAME :>> ', error, " - ", msg);
        // socket.emit("ADMIN_START_GAME", HelperUtils.errorResponseObj("ADMIN_START_GAME", error.message, "-", error))
        // throw error
    }
});

// for admin leave table
router.post("/ADMIN_LEAVE_TABLE", auth, async (req, res) => {
    try {
        const socket = '';
        const io = '';
        const userId = req.body.userId;
        const tableId = req.body.tableId;
        const message = "you left table successfully."
        const userMessage = "you are leave table due to admin left."
        obj = {
            message: "you left table successfully and all user leave table due to admin left."
        }
        //const userMessageForEveryone = "you left table successfully and all user leave table due to admin left."
        await adminLeave(userId, tableId, message, userMessage, socket, io)
        res.send(HelperUtils.successResponseOBJ("ADMIN_LEAVE_TABLE", obj));
    } catch (error) {
        res.send(HelperUtils.errorResponseObj("ADMIN_LEAVE_TABLE", error.message, "-", error));
        throw error
    }
});

// for admin leave table
router.post("/ADMIN_MUTE_ALL", auth, async (req, res) => {
    try {
        const isAllMute = req.body.isAllMute;
        const tableId = req.body.tableId;
        let resobj;
        if (isAllMute) {
            resobj = {
                isAdminMute: true,
                message: "Admin mute all user"
            }
        } else {
            resobj = {
                isAdminMute: false,
                message: "Admin unmute all user"
            }
        }

        const tDatail = await Table.findOne({ _id: tableId })
        const userdata = tDatail.userData
        userdata.forEach((element) => {
            const socketId = element.socketId
           // socket.to(socketId).emit("ADMIN_MUTE_ALL", HelperUtils.successResponseOBJ("ADMIN_MUTE_ALL", resobj))
        });
        let adminObj;
        if (isAllMute) {
            adminObj = {
                isAdminMute: true,
                message: "You muted all user in table"
            }
        } else {
            adminObj = {
                isAdminMute: false,
                message: "You unmuted all user in table"
            }
        }
        res.send(HelperUtils.successResponseOBJ("ADMIN_MUTE_ALL", adminObj));
        // socket.emit("ADMIN_MUTE_ALL", HelperUtils.successResponseOBJ("ADMIN_MUTE_ALL", adminObj))

    } catch (error) {
        res.send(HelperUtils.errorResponseObj("ADMIN_MUTE_ALL", error.message, "-", error));
        throw error
    }
})

 // for user leave table
router.post("/LEAVE_TABLE", auth, async (req, res) => {
    try {
        const socket = '';
        const userId = req.body.userId;
        const tableId = req.body.tableId;
        await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true });
        const isAdminLeft = false;
        obj = {
            message: "you are leave table successfully."
        }
        // const message = "you are leave table successfully."

        await userLeave(userId, tableId, isAdminLeft, message, socket);
        res.send(HelperUtils.successResponseOBJ("LEAVE_TABLE", obj));
    } catch (error) {
        res.send(HelperUtils.errorResponseObj("LEAVE_TABLE", error.message, "-", error));
        throw error
    }
});

// end

async function initialDailyReword(userId) {
    try {
        // [ "claim", "claimed" , "skiped" , "next" ,]
        const dailyRewards = await Dailyrewards.create({
            userId,
            dailyReward: [
                { day: 1, rewardPoint: 10000, action: "claim", collectedTime: null },
                { day: 2, rewardPoint: 15000, action: "next", collectedTime: null },
                { day: 3, rewardPoint: 20000, action: "", collectedTime: null },
                { day: 4, rewardPoint: 25000, action: "", collectedTime: null },
                { day: 5, rewardPoint: 30000, action: "", collectedTime: null },
                { day: 6, rewardPoint: 35000, action: "", collectedTime: null },
                { day: 7, rewardPoint: 50000, action: "", collectedTime: null },
            ],
            remainingTime: 0
        });
        console.log('dailyRewards :>> ', dailyRewards);


    } catch (error) {
        console.log('CATCH_ERROR :  initialDailyReword :>>', error, error.message);
        throw error
    }
}

async function initialDailySpin(userId) {
    try {
        const dailySpin = await DailySpins.create({
            userId,
            dailySpinReward: [1000, 10000, 15000, 2000, 20000, 25000, 4000, 30000, 5000, 35000, 45000, 50000],
            isAdSpin: "false",
            lastCollectedSpinTime: 0,
            nextSpinTime: 0,
            remainingTime: 0
        });
        console.log('dailySpin :>> ', dailySpin);

    } catch (error) {
        console.log('CATCH_ERROR :  initialDailySpin :>> ', error, error.message);
        throw error
    }
}

async function initialLevel(userId) {
    try {

        const config = require("config");
        const levelDetail = require('../service/userStaticLevel');
        console.log('levelDetail :>> ', levelDetail);
        const VOUCHER_DIVIDE = config.get("VOUCHER_DIVIDE");
        console.log('VOUCHER_DIVIDE :>> ', VOUCHER_DIVIDE);

        const findPriceVoucher = levelDetail.find((element) => {
            if (element.level == 1) {
                return element
            }
        });

        const levelUpRequierRound = findPriceVoucher.voucher / VOUCHER_DIVIDE
        const userLevel = await UserLevel.create({
            userId: userId,
            levelUpRound: levelUpRequierRound,
            levelUpPrice: findPriceVoucher.voucher
        })

    } catch (error) {
        console.log('CATCH_ERROR :: levelManagement  :>> ', error);
        throw error
    }
}
async function getAllLocalFriends(friends) {
    try {

        console.log('friends != null :  if ::>> ', friends != null);
        let friendsDeatil = friends.friendsId;
        console.log('friendsDeatil :>> ', JSON.stringify(friendsDeatil, null, 2));
        let allLocalFriendsList = []
        for (let index = 0; index < friendsDeatil.length; index++) {
            const element = friendsDeatil[index];
            const getUser = await User.findOne({ _id: element.userId });
            const allFriendsListObj = {
                userId: getUser._id.toString(),
                userName: getUser.fullName,
                profileUrl: getUser.profileUrl,
                isActive: getUser.isActive
            };
            allLocalFriendsList.push(allFriendsListObj)
        }
        console.log('getAllLocalFriends : allLocalFriendsList :>> ', allLocalFriendsList);
        return allLocalFriendsList
    } catch (error) {
        console.log('CATCH_ERROR : getAllLocalFriends :>> ', error);
        throw error
    }
}

async function getAllfreiendsRequest(friends) {
    try {
        console.log('friends != null :  if ::>> ', friends != null);
        let friendReauqet = friends.friendsRequest;
        console.log('friendReauqet :>> ', JSON.stringify(friendReauqet, null, 2));
        let allFriendRequest = []
        friendReauqet.forEach(async (element) => {
            const allFriendsListObj = {
                userId: element.userId,
                userName: element.fullName,
                profileUrl: element.profileUrl,
                message: element.message
            };
            allFriendRequest.push(allFriendsListObj)
        });
        console.log('getAllfreiendsRequest : allFriendRequest :>> ', allFriendRequest);
        return allFriendRequest


    } catch (error) {
        console.log('CATCH_ERROR : getAllfreiendsRequest :>> ', error);
        throw error
    }
};

module.exports = router;