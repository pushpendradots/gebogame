const router = require("express").Router();
const { User, validate } = require("../models/User");
const HelperUtils = require("../utils/helpers");

const Agora = require("agora-access-token");

router.post("/rtctoken", (req, res) => {
    try {
        const appID = "eaaade76fedd45058902f7fe0f8e7ddb";
    const appCertificate = "b178ed7d475d43adbd775c8fa1bd0be2";

    // const uid = Math.floor(Math.random() * 100000);
    const account = Math.floor(Math.random() * 100000)
    const expirationTimeInSeconds = 86400;
    const currentTimestamp = Math.floor(Date.now() / 1000);
    const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds;

    const role = Agora.RtcRole.PUBLISHER ;

    const channel = req.body.channel;
    console.log('channel :>> ', channel , typeof channel);

    const token = Agora.RtcTokenBuilder.buildTokenWithAccount(appID, appCertificate, channel, account, role, privilegeExpiredTs);
    const resObj = {
        account,
        token
    }
    res.status(200).send(HelperUtils.successObj("rtc token genarated successfully", resObj));
    // res.status(200).send( resObj);
    } catch (error) {
        console.log('error ,  :>> ', error , error.message);
    }
    
});

module.exports = router;