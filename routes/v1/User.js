const router = require("express").Router();
const moment = require('moment');
const _ = require("lodash");
const logger = require('winston');
const { Session } = require("./../../models/Session");
const { User, validate } = require("./../../models/User");
const { FriendList } = require('./../../models/firendsList');
const HelperUtils = require("./../../utils/helpers");
const { Dailyrewards } = require('./../../models/dailyRewards');
const { DailySpins } = require('./../../models/dailySpin');
const { UserLevel } = require('./../../models/userLevel');
const { Histroy } = require('./../../models/purchaseHistory');
const { VoucherOrder } = require('./../../models/voucherOrder');
const { Voucher } = require('./../../models/vouchers');
const config = require("config");
// const xoxoDay = require('./../../XoxoDayThirdPartyApi');
const {
    joinTable,
    adminJoinTable,
    adminTicketOpen,
    setWinnData,
    dailyRewardsCounter,
    getRemainingRewardTime,
    setDailyRewardEventResponse,
    getRemainingSpinTime,
    setDailySpinEventResponse,
    adminLeave,
    userLeave,
    resetTicket,
    setBetData,
    setGameBetData,
    getCourrantLevelVoucher
} = require('./../../service');
const { FeedBack } = require('./../../models/feedBack');
const { Table } = require('./../../models/Table');



/*  Post Signup Api
    @Apipath: api/v1/user_signup
    @Method: Post
    @Params: {
      
    }
    @Tags: This API is used for signup user
*/
router.post("/user_signup", async (req, res) => {
    try {
        let objectData = req.body;
        //console.log('sending_res :>> ', objectData.email);
        //check token session and
        let data = await Session.findOne({ sid: objectData.token });
        if (data) {
            let userId = data.uid
            let user = await User.findOneAndUpdate({ _id: userId }, { $set: { isActive: true } }, { new: true, upsert: true });

            // get users local friends list
            const friends = await FriendList.findOne({ userId: userId });

            let allLocalFriendsList = [];
            let friendReauqet;
            if (friends != null) {
                allLocalFriendsList = await getAllLocalFriends(friends)
                friendReauqet = await getAllfreiendsRequest(friends)
            } else {
                console.log('friends != null :  else ::>> ', friends != null);
                allLocalFriendsList = [];
                friendReauqet = []
            }

            // daily reward detail
            const isClaim = false,
            newDailyRewards = await dailyRewardsCounter(userId, isClaim, 0);
            const dailyReward = newDailyRewards.dailyReward;
            const remainigTime = await getRemainingRewardTime(userId)
            let dailyRewards = {
                dailyReward,
                remainigTime
            }

            // spin detail
            const dailySpins = await DailySpins.findOne({ userId: userId });
            const dailySpinReward = dailySpins.dailySpinReward,
                isAdSpin = dailySpins.isAdSpin
            const remaingTime = await getRemainingSpinTime(userId)
            let dailySpin = {
                dailySpinReward,
                remaingTime,
                isAdSpin
            }

            let reconnectObj
            if (user.currantTableId != "") {
                const isTableExist = await Table.findOne({ _id: user.currantTableId })
                if (isTableExist) {
                    const userDetail = await User.findById(userId)
                    const disconnectTime = moment(userDetail.disconnectionTime)
                    const currentTime = moment(new Date());
                    let seconds;
                    seconds = currentTime.diff(disconnectTime, 'seconds')
                    console.log('seconds :>> ', seconds);
                    if (seconds < 300) {
                        if (isTableExist.adminDetail.adminUserId == userId) {
                            reconnectObj = {
                                isReConnection: true,
                                isAdmin: true,
                                isAllreadyInTable: false,
                                tableId: user.currantTableId
                            }
                        } else {
                            reconnectObj = {
                                isReConnection: true,
                                isAdmin: false,
                                isAllreadyInTable: false,
                                tableId: user.currantTableId
                            }
                        }
                    }
                    else {
                        reconnectObj = {
                            isReConnection: false,
                            isAdmin: false,
                            isAllreadyInTable: true,
                            tableId: ""
                        }
                        await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true })
                    }
                } else {
                    reconnectObj = {
                        isReConnection: false,
                        isAdmin: false,
                        isAllreadyInTable: true,
                        tableId: ""
                    }
                    await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true })
                }
            }

            let obj = {
                userDetails: user,
                isSignUp: true,
                allLocalFriendsList,
                friendReauqet,
                dailyRewards,
                dailySpin,
                reconnectObj: reconnectObj ? reconnectObj : {},
                purchaseCoinAmount: config.get("PURCHASE_COIN_AMOUNT")
            }
            res.status(200).send(HelperUtils.successObj(obj));
           
            let onlineFriendsList = [];
            let active = true
            await getLocalFriends(userId, active)
        } else {
            let obj = {
                userDetails: "",
                isSignUp: false
            }
            res.status(200).send(HelperUtils.successObj(obj));
        }
    } catch (error) {
        res.status(400).send(HelperUtils.errorObj("something went wrongsss"))
        return
    }
   
});

async function getAllLocalFriends(friends) {
    try {

        console.log('friends != null :  if ::>> ', friends != null);
        let friendsDeatil = friends.friendsId;
        console.log('friendsDeatil :>> ', JSON.stringify(friendsDeatil, null, 2));
        let allLocalFriendsList = []
        for (let index = 0; index < friendsDeatil.length; index++) {
            const element = friendsDeatil[index];
            const getUser = await User.findOne({ _id: element.userId });
            const allFriendsListObj = {
                userId: getUser._id.toString(),
                userName: getUser.fullName,
                profileUrl: getUser.profileUrl,
                isActive: getUser.isActive
            };
            allLocalFriendsList.push(allFriendsListObj)
        }
        console.log('getAllLocalFriends : allLocalFriendsList :>> ', allLocalFriendsList);
        return allLocalFriendsList
    } catch (error) {
        console.log('CATCH_ERROR : getAllLocalFriends :>> ', error);
        throw error
    }
}

async function getAllfreiendsRequest(friends) {
    try {
        console.log('friends != null :  if ::>> ', friends != null);
        let friendReauqet = friends.friendsRequest;
        console.log('friendReauqet :>> ', JSON.stringify(friendReauqet, null, 2));
        let allFriendRequest = []
        friendReauqet.forEach(async (element) => {
            const allFriendsListObj = {
                userId: element.userId,
                userName: element.fullName,
                profileUrl: element.profileUrl,
                message: element.message
            };
            allFriendRequest.push(allFriendsListObj)
        });
        console.log('getAllfreiendsRequest : allFriendRequest :>> ', allFriendRequest);
        return allFriendRequest


    } catch (error) {
        console.log('CATCH_ERROR : getAllfreiendsRequest :>> ', error);
        throw error
    }
};

module.exports = router;