const { Session } = require("../models/Session");
const { Table } = require("../models/Table");
const { User } = require("../models/User");
const HelperUtils = require("../utils/helpers");
const getTableStatus = require("./getTableStatus");
const botJoinTable = require('./botJoinTable');
const { FriendList } = require("../models/firendsList");

// change log
module.exports = async function adminJoinTable(token, tableId, socket) {
    try {
        let session = await Session.findOne({ sid: token });
        let user = await User.findOne({ _id: session.uid })
        if (session) {
            await User.updateOne({ _id: session.uid }, { currantTableId: tableId }, { upsert: true, new: true })
            socket.join(tableId)
            const roomSize = socket.adapter.rooms.get(tableId).size;
            console.log('roomSize :>> ', roomSize);
            const tDatil = await Table.findOne({ _id: tableId });

            await Table.updateOne({ _id: tableId }, { tableStatus: "inWaiting" });

            console.log('tDatil :>> ', tDatil);
            const userData = tDatil.userData
            console.log('userData :>> ', userData);
            let tPlayers = []
            if(userData && userData.length != 0) {
                for (let i = 0; i < userData.length; i++) {
                    const element = userData[i];
                    const user = await User.findOne({_id: element.userId});
                    let isFriend
                    if(element.isComp == true) {
                        isFriend = false
                    }else {
                        const findFriend = await FriendList.findOne({userId: element.userId});

                        if(findFriend) {
                            const friendList = findFriend.friendsId
                            
                            for (let j = 0; j < friendList.length; j++) {
                                const el = friendList[j];
                                if(el.userId == tDatil.adminDetail.adminUserId){
                                    isFriend = true
                                }else{
                                    isFriend = false
                                }
                            }
                        }else {
                            isFriend = false
                        }
                   
                    }
                   
                   let userObj = {
                        userDetails: user,
                        isFriend
                    }
                    tPlayers.push(userObj)
                }
            }
            let adminJoin = {
                userJoinTable: tPlayers ,
                userDetails: user,
                isAdminJoin: true,
                tableName: tDatil.tableName,
                tableNumber: tDatil.tableNumber,
                currantRound: tDatil.currentRoundCounter,
                isAdminStartRound: tDatil.isAdminStartRound,
                tableId,
            }
            console.log('adminJoinTable :::: adminJoin :>> ', adminJoin);
            await Table.updateOne({ _id: tableId }, { $set: { adminDetail: { adminUserId: session.uid, isAdminJoin: adminJoin.isAdminJoin, socketId: user.socketId } } }, { upsert: true, new: true });
            socket.emit("ADMIN_JOIN", HelperUtils.successResponseOBJ("ADMIN_JOIN", adminJoin));
            const isLeaveTable = false
            const userId = session.uid
            await getTableStatus(userId, tableId, isLeaveTable, socket);
            const userLeave = false
            const isStatic = false
            await botJoinTable(tableId,userLeave, socket, isStatic)

        }
    } catch (error) {
        console.log('CATCH_ERROR ::: adminJoinTable :>> ', error);
        throw error
    }

}