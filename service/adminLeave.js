const { Table } = require("../models/Table");
const {User} = require("../models/User");
const adminLeavetable = require('./adminLeavetable');
const userLeave = require('./userLeave');
const HelperUtils = require('../utils/helpers');


module.exports = async function adminLeave(userId, tableId, message, userMessage, socket, io) {
    try {
        const tDetail = await Table.findOne({ _id: tableId });
        const userData = tDetail.userData
        const adminDetail = tDetail.adminDetail

        console.log('userData lenth :>> ', userData.length);
        if (userData.length == 0) {
            await adminLeavetable(userId, tableId, message, adminDetail , socket, io)
        } else {
            var bar = new Promise((resolve, reject) => {
                userData.forEach(async (item) => {
                    if(item.isComp == false){
                        const userId = item.userId
                        const isAdminLeft = true
                        await userLeave(userId, tableId, isAdminLeft, userMessage , socket, io);
                       
                        resolve()
                    }else{
                        resolve()
                    }
                });
            }).then(async () => {
                await adminLeavetable(userId, tableId, message, adminDetail , socket, io)
            })
        }
    } catch (error) {
        console.log('CATCH_ERROR : adminLeave :>> ', error, error.message);
        throw error
    }
};
