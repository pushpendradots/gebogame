const { User } = require("../models/User");
const getTableStatus = require('./getTableStatus');
const HelperUtils = require("../utils/helpers");
const dailyRewardsCounter = require('./dailyRewardsCounter');
const setDailyRewardEventResponse = require('./setDailyRewardEventResponse');
const { Table } = require("../models/Table");
const setDailySpinEventResponse = require('./setDailySpinEventResponse');


module.exports = async function adminLeavetable(userId, tableId, message, adminDetail, socket, io) {
    try {
        const leaveAdminObj = {
            message
        }
        if (socket.id != adminDetail.socketId) {
            //socket.to(adminDetail.socketId).emit("ADMIN_LEAVE_TABLE", HelperUtils.successResponseOBJ("ADMIN_LEAVE_TABLE", leaveAdminObj))
            setTimeout(async () => {
                const isClaim = false,
                    dailyReward = await dailyRewardsCounter(adminDetail.adminUserId, isClaim, 0);
                await setDailyRewardEventResponse(adminDetail.adminUserId, dailyReward, socket);
                const isAdSpin = "false"
                await setDailySpinEventResponse(adminDetail.adminUserId, isAdSpin, socket)
            }, 1000)
        } else {
            //socket.emit("ADMIN_LEAVE_TABLE", HelperUtils.successResponseOBJ("ADMIN_LEAVE_TABLE", leaveAdminObj))
            setTimeout(async () => {
                const isClaim = false,
                    dailyReward = await dailyRewardsCounter(userId, isClaim, 0);
                await setDailyRewardEventResponse(userId, dailyReward, socket);
                const isAdSpin = "false"
                await setDailySpinEventResponse(userId, isAdSpin, socket)
            }, 1000)
        }

        const isLeaveTable = true
        await getTableStatus(userId, tableId, isLeaveTable, socket);
        console.log('isLeaveTable :>> ', "isLeaveTable");
        //* remove bot from db
        const tableDetail = await Table.findOne({ _id: tableId });
        const userData = tableDetail.userData
        for (let i = 0; i < userData.length; i++) {
            const element = userData[i];
            if (element.isComp == true) {
                await Table.updateOne({ _id: tableId }, { $pull: { userData: { userId: element.userId } } }, { safe: true, multi: true })
                await User.deleteOne({ _id: element.userId })
            }
        }

        //io.in(tableId).socketsLeave(tableId);
        await Table.deleteOne({ _id: tableId })
       
    } catch (error) {
        console.log('CATCH_ERROR : adminLeavetable :>> ', error, error.message);
        throw error
    }
}

