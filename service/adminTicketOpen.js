const { Table } = require("../models/Table");
const { User } = require("../models/User");


module.exports = async function adminTicketOpen(tableId, isTicketOpenByAdmin) {
    try {
        const tableData = await Table.findOne({ _id: tableId });
        console.log('adminTicketOpen : tableData :>> ', tableData);

        const ticketLatter = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        const ticketLatterIndex = Math.floor(Math.random() * ticketLatter.length);
        if (isTicketOpenByAdmin != true) {
            const randomTicket = ticketLatter[ticketLatterIndex];

            if(tableData.isStatic != true) {
                await Table.updateOne({ _id: tableId }, { $set: { adminTicketSkipCount: tableData.adminTicketSkipCount + 1 } })
            }

            return randomTicket
        } else {
            await Table.updateOne({ _id: tableId }, { $set: { adminTicketSkipCount: 0 } })
            const randomTicket = ticketLatter[ticketLatterIndex];
            console.log('adminTicketOpen : randomTicket :>> ', randomTicket);
            return randomTicket
        }
    } catch (error) {
        console.log('adminTicketOpen :  error :>> ', tableId, adminUserId, isTicketOpen, " - ", error);
        throw error
    }
}

