

module.exports = async function botBating(userId, tableId, socket) {
    try {

        const maxValue = 500,
                minValue = 10;
        const betAmount = Math.floor(Math.random() * (maxValue - minValue + 1) + minValue)
       
        let userBet = [
            [{ betLatter: 1, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 1, betLattereAmount: 0, percentage: 50 },
                { betLatter: 2, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 1, betLattereAmount: 0, percentage: 50 },
                { betLatter: 7, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 1, betLattereAmount: 0, percentage: 25 },
                { betLatter: 2, betLattereAmount: 0, percentage: 25 },
                { betLatter: 7, betLattereAmount: 0, percentage: 25 },
                { betLatter: 8, betLattereAmount: 0, percentage: 25 },
            ],
            [{ betLatter: 2, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 2, betLattereAmount: 0, percentage: 50 },
                { betLatter: 3, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 2, betLattereAmount: 0, percentage: 50 },
                { betLatter: 8, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 2, betLattereAmount: 0, percentage: 25 },
                { betLatter: 3, betLattereAmount: 0, percentage: 25 },
                { betLatter: 8, betLattereAmount: 0, percentage: 25 },
                { betLatter: 9, betLattereAmount: 0, percentage: 25 },
            ],
            [{ betLatter: 3, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 3, betLattereAmount: 0, percentage: 50 },
                { betLatter: 4, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 3, betLattereAmount: 0, percentage: 50 },
                { betLatter: 9, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 3, betLattereAmount: 0, percentage: 25 },
                { betLatter: 4, betLattereAmount: 0, percentage: 25 },
                { betLatter: 9, betLattereAmount: 0, percentage: 25 },
                { betLatter: 10, betLattereAmount: 0, percentage: 25 },
            ],

            [{ betLatter: 4, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 4, betLattereAmount: 0, percentage: 50 },
                { betLatter: 5, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 4, betLattereAmount: 0, percentage: 50 },
                { betLatter: 10, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 4, betLattereAmount: 0, percentage: 25 },
                { betLatter: 5, betLattereAmount: 0, percentage: 25 },
                { betLatter: 10, betLattereAmount: 0, percentage: 25 },
                { betLatter: 11, betLattereAmount: 0, percentage: 25 },
            ],
            [{ betLatter: 5, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 5, betLattereAmount: 0, percentage: 50 },
                { betLatter: 6, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 5, betLattereAmount: 0, percentage: 50 },
                { betLatter: 11, betLattereAmount: 0, percentage: 50 },
            ],
            [
                { betLatter: 5, betLattereAmount: 0, percentage: 25 },
                { betLatter: 6, betLattereAmount: 0, percentage: 25 },
                { betLatter: 11, betLattereAmount: 0, percentage: 25 },
                { betLatter: 12, betLattereAmount: 0, percentage: 25 },
            ],
            [{ betLatter: 6, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 6, betLattereAmount: 0, percentage: 50 },
                { betLatter: 13, betLattereAmount: 0, percentage: 50 },
            ],
            [{ betLatter: 7, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 7, betLattereAmount: 0, percentage: 50 },
                { betLatter: 8, betLattereAmount: 0, percentage: 50 },
            ],
            [{ betLatter: 8, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 8, betLattereAmount: 0, percentage: 50 },
                { betLatter: 9, betLattereAmount: 0, percentage: 50 },
            ],
            [{ betLatter: 9, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 9, betLattereAmount: 0, percentage: 50 },
                { betLatter: 10, betLattereAmount: 0, percentage: 50 },
            ],

            [{ betLatter: 10, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 10, betLattereAmount: 0, percentage: 50 },
                { betLatter: 11, betLattereAmount: 0, percentage: 50 },
            ],
            [{ betLatter: 11, betLattereAmount: 0, percentage: 100 }],
            [
                { betLatter: 11, betLattereAmount: 0, percentage: 50 },
                { betLatter: 12, betLattereAmount: 0, percentage: 50 },
            ],
            [{ betLatter: 12, betLattereAmount: 0, percentage: 100 }],
        ]

        const randomUserBet = Math.floor(Math.random() * userBet.length);
        const betArr = userBet[randomUserBet]
        console.log('betArr :>> ', userBet[randomUserBet]);

        let bet = [];
        console.log('betArr.length :>> ', betArr.length);
        if (betArr.length == 1) {
            const botBet = betAmount / betArr.length
            console.log('botBet :>> ', botBet);
            for (let i = 0; i < betArr.length; i++) {
                const element = betArr[i];
                const obj = {
                    betLatter: element.betLatter,
                    betLattereAmount: botBet,
                    percentage: element.percentage
                }
                bet.push(obj)
            }
        } else if (betArr.length == 2) {
            const botBet = betAmount / betArr.length
            console.log('botBet :>> ', botBet);
            for (let i = 0; i < betArr.length; i++) {
                const element = betArr[i];
                const obj = {
                    betLatter: element.betLatter,
                    betLattereAmount: botBet,
                    percentage: element.percentage
                }
                bet.push(obj)
            }
        } else {
            const botBet = betAmount / betArr.length
            console.log('botBet :>> ', botBet);
            for (let i = 0; i < betArr.length; i++) {
                const element = betArr[i];
                const obj = {
                    betLatter: element.betLatter,
                    betLattereAmount: botBet,
                    percentage: element.percentage
                }
                bet.push(obj)
            }
        }
        console.log('bet :>> ', bet);
        const resp = {
            tableId: tableId,
            userId: userId,
            bets: bet
        }
        console.log('resp :>> ', resp);

        return resp

    } catch (error) {
        console.log(' CATCH_ERROR ::: botBating :>> ', error);
        throw error
    }
}

