const { Table } = require("../models/Table");
const { User } = require("../models/User");
const moment = require('moment');
const HelperUtils = require('../utils/helpers');


module.exports = async function botJoinTable(tableId, userLeave, socket, isStatic) {
    try {
        const createBot = require('./createBot');
        let botJoinTableTime;
        if (isStatic == true) {
            botJoinTableTime = 1000
        } else {
            botJoinTableTime = 30000
        }
        setTimeout(async () => {
            let tableData = await Table.findOne({ _id: tableId });
            if (tableData) {
                let playerCount;
                if (tableData.isStatic == true) {
                    playerCount = 4;
                } else {
                    playerCount = 4
                }
                // check userdata length in table 
                const userData = tableData.userData
                if (userData.length <= playerCount) {
                    const bot = await createBot()
                    console.log('bot :>> ', bot);
                    const user = await User.findOne({ _id: bot._id })

                    if (tableData) {
                        let userBet = [
                            { betLatter: 1, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 2, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 3, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 4, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 5, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 6, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 7, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 8, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 9, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 10, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 11, betLattereAmount: 0, percentage: 0 },
                            { betLatter: 12, betLattereAmount: 0, percentage: 0 },
                        ]
                        let obj = {}
                        if (isStatic != true) {
                            obj = { userId: user._id, userBet, isComp: user.isComp, socketId: socket.id }
                        } else {
                            obj = { userId: user._id, userBet, isComp: user.isComp, socketId: "" }
                        }
                        await Table.updateOne({ _id: tableId }, { $push: { userData: obj } });
                    }
                    obj = {

                        userDetails: user,
                        isFriend: true
                    }

                    if (isStatic != true) {
                        // if (userLeave == true) {
                        //     socket.to(tableData.adminDetail.socketId).emit("USER_JOIN_TABLE", HelperUtils.successResponseOBJ("USER_JOIN_TABLE", obj))
                        // } else {

                        //     socket.emit("USER_JOIN_TABLE", HelperUtils.successResponseOBJ("USER_JOIN_TABLE", obj));
                        // }
                    }

                    const table = await Table.findById(tableId);

                    const userData = table.userData
                    await Table.updateOne({ _id: tableId }, { minPlayerSeat: userData.length }, { upsert: true, new: true });
                    return await botJoinTable(tableId, userLeave, socket, isStatic);
                }
            }
        }, botJoinTableTime)



    } catch (error) {
        console.log('CATCH_ERROR ::: botJoinTable :>> ', error);
        throw error
    }
}