
const { Table } = require('../models/Table');
const { User } = require('../models/User');
const userLeave = require('./userLeave');

module.exports = async function botLeaveTable(userId, tableId, socket, io) {
    try {

        const isAdminLeft = false
        const message = "you are leave table successfully."
        const table = await Table.findById(tableId);

        const userData = table.userData

        await Table.updateOne({ _id: tableId }, { minPlayerSeat: userData.length })

        await userLeave(userId, tableId, isAdminLeft, message, socket, io)
        await User.deleteOne({ _id: userId })

    } catch (error) {
        console.log('CATCH_ERROR ::  botLeaveTable :>> ', error);
        throw error
    }
}