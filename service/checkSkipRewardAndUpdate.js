
const { Dailyrewards } = require("../models/dailyRewards");
const moment = require('moment');

// for daily reward check reward is skip or not
module.exports =  async function checkSkipRewardAndUpdate(dailyRewards, skipTime, userId) {
    try {

        if (dailyRewards) {
            let lastCollectedDay = dailyRewards.lastCollectedDay;
            const  dailyReward = dailyRewards.dailyReward
            console.log('dailyReward 1 :>> ', dailyReward);
            console.log('skipTime :>> ', skipTime);
            console.log('new Date() :>> ', new Date());
            console.log('skipTime < moment(new Date()) :>> ', skipTime < moment(new Date()));
            let reward = [];


            if (skipTime < moment(new Date())) {

                dailyReward.forEach((element) => {
                    let obj;
                    if (lastCollectedDay == 6) {
                        if (element.day < 3) {
                            lastCollectedDay = -1;
                        }
                        else {
                            lastCollectedDay = dailyRewards.lastCollectedDay;
                        }
                    }
                    if (element.day == (lastCollectedDay + 1)) {
                        obj = {
                            day: element.day,
                            rewardPoint: element.rewardPoint,
                            action: "skiped",
                            collectedTime: null
                        };
                        reward.push(obj);

                    }
                    else if (element.day == (lastCollectedDay + 2)) {
                        obj = {
                            day: lastCollectedDay + 2,
                            rewardPoint: element.rewardPoint,
                            action: "claim",
                            collectedTime: null
                        }
                        reward.push(obj);
                    }
                    else if (element.day == (lastCollectedDay + 3)) {
                        obj = {
                            day: lastCollectedDay + 3,
                            rewardPoint: element.rewardPoint,
                            action: "next",
                            collectedTime: null
                        }
                        reward.push(obj);
                    }
                    else {
                        reward.push(element);
                    }
                });
                await Dailyrewards.updateOne({ userId: userId }, { dailyReward: reward }, { upsert: true, new: true });
            }
        }

    } catch (error) {
        console.log('CATCH_ERROR ::: checkSkipRewardAndUpdate :>> ', error, error.message, " - ", userId);
        throw error
    }
}