const { User } = require("../models/User");

const randomName = require('./randomName');


module.exports = async function createBot() {
    try {

        let avatar = "Avatar_" + Math.floor(Math.random() * 8)
        var finalName = randomName[Math.floor(Math.random() * randomName.length)];
        
        const bot = await User.create({
            fullName: finalName,
            walletBalance: 500,
            profileUrl: avatar,
            isComp: true
        })
        console.log('bot :>> ', bot._id.toString);
        return bot
    } catch (error) {
        console.log('CATCH_ERROR :: createBot ::  :>> ', error);
        throw error
    }
}




 

