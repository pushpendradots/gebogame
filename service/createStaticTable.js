const { Table } = require("../models/Table");
const botJoinTable = require('../service/botJoinTable');
const randomName = require('./randomName');
const config = require('config');
module.exports = async function createStaticTable() {
    try {

        const staticAdminId = config.get("STATIC_ADMIN_ID")
        console.log('staticAdminId :>> ', staticAdminId);
        let tableNumber = Math.floor((Math.random() * 1000000) + 1);
        const table = await Table.findOne({tableNumber: tableNumber})
        let tableNum;
        if(table) {
           tableNum = tableNumber + 1
        }else {
            tableNum = tableNumber
        }
        var finalName = randomName[Math.floor(Math.random() * randomName.length)];
        const staticTable = await Table.create({
            tableName: finalName,
            tableNumber: tableNum,
            isStatic: true,
            isAdminStartRound: true,
            adminDetail: {
                adminUserId: staticAdminId,
                isAdminJoin: true,
                socketId: ""
            }
        });

        const tableId = staticTable._id
        const userLeave = false
        const isStatic = true
        const socket = ""
        await botJoinTable(tableId,userLeave, socket, isStatic)

        return;
        
    } catch (error) {
        console.log('CATCH_ERROR :: createStaticTable :>> ', error);
    }
}