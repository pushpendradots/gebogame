const { Dailyrewards } = require("../models/dailyRewards");
const moment = require('moment');
const { User } = require("../models/User");
const findDailyRewardAvailable = require('./findDailyRewardAvailable');
const checkSkipRewardAndUpdate = require('./checkSkipRewardAndUpdate');
const resetDailyRewards = require('./resetDailyRewards');


module.exports = async function dailyRewardsCounter(userId, isClaim, day) {
    try {
        const dailyRewards = await Dailyrewards.findOne({ userId: userId });
        const collectionTime = dailyRewards.collectionTime;
        const collectionSkipTime = dailyRewards.collectionSkipTime;
        const collectionType = dailyRewards.collectionType;
        if (isClaim) {
            const rewards = dailyRewards.dailyReward,
                currantDateAndTime = moment(new Date()),  // rewards collect time
                nextRewardTime = moment(new Date()).add(collectionTime, collectionType)   // after 24 hour get next daily rewards 
                // nextRewardTime = moment(new Date()).add("1", "minute")
            let reward = []
            rewards.forEach(async (element) => {
                let obj;
                if (element.day === day + 1) {
                    obj = {
                        day: element.day,
                        rewardPoint: element.rewardPoint,
                        action: "next",
                        collectedTime: null
                    }
                    reward.push(obj);
                }
                else if (element.day == day) {
                    obj = {
                        day: element.day,
                        rewardPoint: element.rewardPoint,
                        action: "claimed",
                        collectedTime: currantDateAndTime
                    }
                    reward.push(obj);
                    const userDatile = await User.findOne({ _id: userId });
                    const rewardPoint = userDatile.rewardPoint == 0 ? element.rewardPoint : userDatile.rewardPoint + element.rewardPoint
                    await User.updateOne({ _id: userId }, { rewardPoint: rewardPoint }, { upsert: true, new: true })
                }
                else {
                    reward.push(element);
                }
            });
            await Dailyrewards.updateOne({ userId: userId }, { dailyReward: reward, lastCollectedDay: day, lastCollectedTime: currantDateAndTime, nextRewardTime }, { upsert: true, new: true })

            const rewardArray = dailyRewards.dailyReward
            const claimReward =  rewardArray.find((el) => el.day.toString() === day.toString());
    
            const user = await User.findById(userId);
           
            const rewardAmount = user.walletBalance != 0 ? user.walletBalance + claimReward.rewardPoint : claimReward.rewardPoint 
            await User.updateOne({_id: userId},{walletBalance: rewardAmount },{ upsert: true, new: true });
        }
        const claimDailyReward = await Dailyrewards.findOne({ userId: userId });
        const lastCollectedTime = claimDailyReward.nextRewardTime;
        await findDailyRewardAvailable(lastCollectedTime, claimDailyReward, userId)
        const findDailyReward = await Dailyrewards.findOne({ userId: userId });
        const skipTime = moment(findDailyReward.lastCollectedTime).add(collectionSkipTime, collectionType);
        await checkSkipRewardAndUpdate(findDailyReward, skipTime, userId)
        const checkSkiprewards = await Dailyrewards.findOne({ userId: userId });
        await resetDailyRewards(checkSkiprewards, userId)
        const resetDailyReward = await Dailyrewards.findOne({ userId: userId });
        console.log("resetDailyReward",resetDailyReward);
        return resetDailyReward

    } catch (error) {
        console.log('CATCH_ERROR :: dailyRewardsCounter :>> ', error);
        throw error
    }
}