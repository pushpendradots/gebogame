const { Dailyrewards } = require("../models/dailyRewards");
const moment = require('moment');

module.exports = async function findDailyRewardAvailable(CollectedTime, dailyRewards, userId) {
    try {
        if (dailyRewards.lastCollectedDay != 0) {
            if (CollectedTime < moment(new Date())) {
                const rewards = dailyRewards.dailyReward
                let reward = []
                let lastCollectedDay = dailyRewards.lastCollectedDay
                if (lastCollectedDay == 7) {
                    lastCollectedDay = 0
                    await Dailyrewards.updateOne({ userId: userId }, { lastCollectedDay: 0 }, { upsert: true, new: true })
                }
                rewards.forEach(async (element) => {
                    let obj;
                    if (element.day == lastCollectedDay + 1) {
                        obj = {
                            day: element.day,
                            rewardPoint: element.rewardPoint,
                            action: "claim",
                            collectedTime: null
                        }
                        reward.push(obj);
                    }
                    else {
                        reward.push(element);
                    }
                });
                await Dailyrewards.updateOne({ userId: userId }, { dailyReward: reward }, { upsert: true, new: true })
            }
        }
    } catch (error) {
        console.log('CATCH_ERROE ::: findDailyRewardAvailable :>> ', error);
        throw error
    }
}
