
const e = require('cors');
const { bool } = require('joi');
const { UserLevel } = require('../models/userLevel');
const { Voucher } = require('../models/vouchers');
const levelDetail = require('./userStaticLevel');

module.exports = async function getCourrantLevelVoucher(data) {
    try {
        const userId = data.userId;
        const level = data.level
        console.log('level :>> ', level);
        
        const userLevel = levelDetail.find(el => el.level == level)
        console.log('userLevel :>> ', userLevel);
        const levelPrice = userLevel.voucher

        const getVouchers = await Voucher.find({});

        const fixVoucherData = getVouchers[0].fixedValueVoucher
        const openVoucher = getVouchers[0].openValueVoucher

        const userLevelVoucher = await UserLevel.findOne({ userId: userId })
        let currentLevelVouchers = userLevelVoucher.userVoucher.find(element => element.level == level)
        const finalVoucherArray = []
        if (currentLevelVouchers == undefined) {
            console.log('if : currentLevelVouchers :>> ', currentLevelVouchers);

            let availableVoucher = []
            let zomato = "Zomato",
                dominos = "Domino's",
                mantra = "Myntra"

            for (let i = 0; i < fixVoucherData.length; i++) {
                const element = fixVoucherData[i];

                const priceArray = element.valueDenominations

                const findPrice = priceArray.find((el) => el == levelPrice.toString())

                if (findPrice) {
                    if (zomato.includes(element.name)) {
                        finalVoucherArray.push(element)
                        zomato = ""
                    } else if (dominos.includes(element.name)) {
                        finalVoucherArray.push(element)
                        dominos = ""
                    } else if (mantra.includes(element.name)) {
                        finalVoucherArray.push(element)
                        mantra = ""
                    } else {
                        availableVoucher.push(element)
                    }
                }
            };


            for (let i = 0; i < openVoucher.length; i++) {
                const element = openVoucher[i];
                if (element.minValue <= levelPrice && element.maxValue >= levelPrice) {
                    if (zomato.includes(element.name)) {
                        finalVoucherArray.push(element)
                        zomato = ""
                    } else if (dominos.includes(element.name)) {
                        finalVoucherArray.push(element)
                        dominos = ""
                    } else if (mantra.includes(element.name)) {
                        finalVoucherArray.push(element)
                        mantra = ""
                    } else {
                        availableVoucher.push(element)
                    }
                }
            }

            for (let i = 0; i < availableVoucher.length; i++) {
                const element = availableVoucher[i];
                // add 3 random voucher
                if (finalVoucherArray.length <= 6) {
                    // get random index value
                    const randomIndex = Math.floor(Math.random() * availableVoucher.length);
                    const item = availableVoucher[randomIndex];
                    finalVoucherArray.push(item);
                    availableVoucher.pop(item)
                }
            }

            const update = {
                level: level,
                levelPrice: levelPrice,
                voucher: finalVoucherArray
            }

            await UserLevel.updateOne({ userId: userId }, { $push: { userVoucher: update } }, { upsert: true, new: true })
        } else {
            console.log(' else : currentLevelVouchers :>> ', currentLevelVouchers);

            for (let i = 0; i < currentLevelVouchers.voucher.length; i++) {
                const element = currentLevelVouchers.voucher[i];
                if (element.valueType == "fixed_denomination") {
                    for (let j = 0; i < fixVoucherData.length; j++) {
                        const fixElement = fixVoucherData[j];
                        if (element.productId == fixElement.productId) {
                            const priceArray = fixElement.valueDenominations
                            const findPrice = priceArray.find((el) => el == levelPrice.toString())
                            if (findPrice) {
                                finalVoucherArray.push(element)
                                break;
                            }
                        }
                    }
                } else if (element.valueType == "open_value") {
                    for (let j = 0; i < openVoucher.length; j++) {
                        const openElement = openVoucher[j];
                        if (element.productId == openElement.productId) {
                            if (element.minValue <= levelPrice && element.maxValue >= levelPrice) {
                                finalVoucherArray.push(element)
                                break;
                            }
                        }
                    }
                }
            }
        }

        const resultObj = {
            userlevel: userLevelVoucher.userlevel,
            userlevelRound: userLevelVoucher.userlevelRound,
            levelUpRound: userLevelVoucher.levelUpRound,
            levelUpPrice: userLevelVoucher.levelUpPrice,
            finalVoucherArray,
            claimedVoucher: userLevelVoucher.claimedVoucher
        }
       
        return resultObj

    } catch (error) {
        console.log('CATCH_ERROR :: getCourrantLevelVoucher :>> ', error, " - ", data);
        throw error
    }
};