const { Dailyrewards } = require("../models/dailyRewards");
const moment = require('moment');
 
 
 // for daily reward get remaining time for next reward collect
 module.exports = async function getRemainingRewardTime(userId) {
    try {
        const rewardDetail = await Dailyrewards.findOne({ userId: userId }),
            nextRewardTime = moment(rewardDetail.nextRewardTime)

        const aTime = moment(new Date());
        console.log('aTime :>> ', aTime);

        const bTime = nextRewardTime
        console.log('bTime :>> ', bTime, moment(bTime));

        let seconds;
        seconds = bTime.diff(aTime, 'seconds')
        console.log('seconds :>> ', seconds);
        if (seconds <= 0) {
            seconds = 0
        }
        return seconds

    } catch (error) {
        console.log('getNextRewardTime : error :>> ', error);
        throw error
    }
}