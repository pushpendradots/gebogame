const { DailySpins } = require("../models/dailySpin");
const moment = require('moment');

// for daily spin get remaining time for next spin
module.exports = async function getRemainingSpinTime(userId) {
    try {
        const spinDetail = await DailySpins.findOne({ userId: userId });

        const aTime = moment(new Date()); // currant time 
        console.log('aTime :>> ', aTime);

        const bTime = moment(spinDetail.nextSpinTime) // next reward thime
        console.log('bTime :>> ', bTime, moment(bTime))

        let seconds;
        seconds = bTime.diff(aTime, 'seconds')
        console.log('seconds :>> ', seconds);

        if (seconds <= 0) {
            seconds = 0
        }

        return seconds
    } catch (error) {
        console.log('CATCH_ERROR ::  getRemainingSpinTime :>> ', error);
        throw error
    }
}