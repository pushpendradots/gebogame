const { FriendList } = require("../models/firendsList");
const { Table } = require("../models/Table");
const { User } = require("../models/User");
const sendTableStatus = require("./sendTableStatus");


module.exports = async function getTableStatus(userId, tableId, isLeaveTable , socket) {
    try {
        const friendsList = await FriendList.findOne({ userId })
        console.log('friendsList :>> ', friendsList);

        const tDatail = await Table.findOne({ _id: tableId })
        const userData = tDatail.userData

        if (friendsList != null) {
            const friendId = friendsList.friendsId

            for (let i = 0; i < friendId.length; i++) {
                const element = friendId[i];
                const getFriends = await User.findOne({ _id: element.userId });
                console.log('getFriends :>> ', getFriends);
                if (getFriends.isActive == true) {
                    const user = await User.findOne({ _id: userId })
                    console.log('user :>> ', user, userId);

                    let tstatus = "InGame"
                    if (isLeaveTable) {
                        tstatus = "Invite"
                        await sendTableStatus(getFriends._id.toString(), tstatus, user.socketId, socket)
                    } else {
                        userData.forEach(async (element) => {
                            if (getFriends._id.toString() == element.userId.toString()) {
                                tstatus = "InTable"
                                await sendTableStatus(element.userId, tstatus, user.socketId, socket)
                            }
                        });
                        if (tDatail.adminDetail.adminUserId == getFriends._id) {
                            tstatus = "InTable"
                            await sendTableStatus(tDatail.adminDetail.adminUserId, tstatus, user.socketId, socket)
                        }
                    }
                    await sendTableStatus(userId, tstatus, getFriends.socketId, socket)
                };
            }
        }

        return
    } catch (error) {
        console.log('CATCH_ERROR : getLocalFriends  :>> ', error);
    }
}

