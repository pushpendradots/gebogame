const joinTable = require('./joinTable');
const adminJoinTable = require('./adminJoinTable');
const adminTicketOpen = require('./adminTicketOpen');
const setWinnData = require('./setWinnData');
const dailyRewardsCounter = require('./dailyRewardsCounter');
const getRemainingRewardTime = require('./getRemainingRewardTime');
const setDailyRewardEventResponse = require('./setDailyRewardEventResponse');
const getRemainingSpinTime = require('./getRemainingSpinTime');
const setDailySpinEventResponse = require('./setDailySpinEventResponse');
const adminLeave = require('./adminLeave');
const userLeave = require('./userLeave');
const resetTicket = require('./resetTicket');
const setBetData = require('./setBetData');
const createBot = require('./createBot');
const levelDetail = require('./userStaticLevel');
const setLevelUpEventResponse = require('./setLevelUpEventResponse');
const botBating = require('./botBeting');
const botJoinTable = require('./botJoinTable');
const createStaticTable = require('./createStaticTable');
const getCourrantLevelVoucher = require('./getCurrantLevelVoucher');
const randomName = require('./randomName');

module.exports = {
    joinTable,
    adminJoinTable,
    adminTicketOpen,
    setWinnData,
    dailyRewardsCounter,
    getRemainingRewardTime,
    setDailyRewardEventResponse,
    getRemainingSpinTime,
    setDailySpinEventResponse,
    adminLeave,
    userLeave,
    resetTicket,
    setBetData,
    createBot,
    levelDetail,
    randomName,
    setLevelUpEventResponse,
    botBating,
    botJoinTable,
    createStaticTable,
    getCourrantLevelVoucher
}