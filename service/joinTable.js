const { FriendList } = require("../models/firendsList");
const { Session } = require("../models/Session");
const { Table } = require("../models/Table");
const { User } = require("../models/User");
const moment = require('moment');
const HelperUtils = require("../utils/helpers");
const getTableStatus = require("./getTableStatus");
const resetTicket = require('./resetTicket');
const botLeaveTable = require('./botLeaveTable');
const createStaticTable = require('./createStaticTable');

module.exports = async function joinTable(token, tableId, socket, socketio) {
    try {
        let session = await Session.findOne({ sid: token });

        const user = await User.findOne({ _id: session.uid })
        await User.updateOne({_id: session.uid} ,{ currantTableId : tableId},{upsert:true, new: true})
        if (session) {
            // user join room 
            socket.join(tableId)
            const roomSize = socket.adapter.rooms.get(tableId).size

            const roundCycle = 30
            let currentTime = new Date();
            console.log('currentTime :>> ', currentTime);
            let waitingTime;

            let tableData = await Table.findOne({ _id: tableId });
            let userData = tableData.userData

            const findUserInTable = userData.find(({ userId }) => userId.toString() === session.uid.toString())
            console.log('findUserInTable :>> ', findUserInTable, !!findUserInTable);

            // when table is full user can not join table  
            if (findUserInTable || userData.length == 10) {

                if (findUserInTable) {
                    obj = {
                        userDetails: user,
                        tableIsFull: false,
                        join_table: false,
                        message: "you are already play in this table"
                    }
                } else {
                    obj = {
                        userDetails: user,
                        tableIsFull: true,
                        join_table: false,
                    }
                }
                socket.emit("JOIN_TABLE", HelperUtils.successResponseOBJ("JOIN_TABLE", obj))
                socket.leave(tableId)

            } else {
                let userId = session.uid;
                let tableExit = await Table.findOne({ _id: tableId });
                await User.findOneAndUpdate({ _id: userId }, { $set: { socketId: socket.id } }, { new: true, upsert: true });
                if (tableExit) {
                    let userBet = [
                        { betLatter: 1, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 2, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 3, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 4, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 5, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 6, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 7, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 8, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 9, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 10, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 11, betLattereAmount: 0, percentage: 0 },
                        { betLatter: 12, betLattereAmount: 0, percentage: 0 },
                    ]
                    var userJoinTable = await Table.updateOne({ _id: tableId }, { $push: { userData: { userId: userId, userBet, isComp: user.isComp, socketId: socket.id } } });

                    const table = await Table.findOne({ _id: tableId });
                    const adminDetail = table.adminDetail

                    const adminUser = await User.findOne({ _id: adminDetail.adminUserId });
                    

                   if (((roomSize > 2 && table.isStatic == false) || (roomSize >= 1 && table.isStatic == true)) && table.tableStatus != "inWaiting") {
                        if (table.isAdminStartRound == true) {
                            // await Table.updateOne({_id: tableId}, {currentRoundStartTimer : null})
                            const tableDetail = await Table.findOne({ _id: tableId });
                            const crt = tableDetail.currentRoundStartTimer == null ? 0 : tableDetail.currentRoundStartTimer;
                            let remainingTime = moment(currentTime - crt).format("ss");
                            waitingTime = roundCycle - remainingTime
                        }
                    } else {
                        waitingTime = 0
                    }
                    const currantRound = table.currentRoundCounter

                    obj = {
                        tableId: tableId,
                        tableName: table.tableName,
                        tableNumber: table.tableNumber,
                        userDetails: user,
                        tableIsFull: false,
                        join_table: true,
                        waitingTime: waitingTime,
                        currantRound: currantRound != 0 ? currantRound : 0,
                    }
                    socket.emit("JOIN_TABLE", HelperUtils.successResponseOBJ("JOIN_TABLE", obj))
                    const findAdminFriendsList = await FriendList.findOne({ userId: adminDetail.adminUserId });
                    let isFriend = false;
                    if (findAdminFriendsList != null) {
                       
                        const frindList = findAdminFriendsList.friendsId
                        const findAminFriend = frindList.find(({ userId }) => userId.toString() == session.uid.toString());
                       
                        if (findAminFriend) {
                            isFriend = true
                        }
                    }

                    if(table.isStatic != true) {

                        adminObj = {
                            userDetails: user,
                            isFriend
                        }
                        socket.to(adminUser.socketId).emit("USER_JOIN_TABLE", HelperUtils.successResponseOBJ("USER_JOIN_TABLE", adminObj))
                    }

                    // remove bot when real user join table
                    console.log('table.userData.length > 3 :>> ', table.userData.length > 3);
                    if(table.userData.length > 3) {
                        const userData = table.userData
                        const findBot = userData.find((ele) => ele.isComp == true)
                        
                       await botLeaveTable(findBot.userId, tableId, socket, socketio);
                    }

                    const tableData = await Table.findOne({ _id: tableId });
                    const userData = tableData.userData;
                    await Table.updateOne({_id: tableId},{minPlayerSeat: userData.length},{upsert: true, new: true})

                    if(table.isStatic == true){
                        const userCount = table.userData
                        if(userCount.length == 10) {
                            await createStaticTable()
                        }
                    }

                    if(tableData.tableStatus == "inWaiting") {
                        
                        setTimeout(async () => {
                            const tDatil = await Table.findOne({ _id: tableId })
                            if (tDatil.isAdminStartRound == true) {
                                await resetTicket(tableId, roomSize, socketio, socket)
                            } else {
                                const waitingobj = {
                                    message: `wait for admin satrt game`,
                                    adminUid: tDatil.isStatic != true ? tDatil.adminDetail.adminUserId.toString() : ""
                                }
                                socket.emit("WAIT", HelperUtils.successResponseOBJ("WAIT", waitingobj))
                            }
                        }, 1000)
                    }
                    
                     
                    const isLeaveTable = false
                    await getTableStatus(userId, tableId, isLeaveTable, socket);
                }
            }
        }
    } catch (error) {
        console.log('CATCH_ERROR ::  ganerateBot :>> ', error);
        throw error
    }
}

