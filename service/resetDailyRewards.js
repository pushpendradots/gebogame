const { Dailyrewards } = require("../models/dailyRewards");

// for daily reward reset daily reward while collect all 7 day reward
module.exports = async function resetDailyRewards(dailyRewards, userId) {
    try {
        const rewards = dailyRewards.dailyReward
        if (dailyRewards.lastCollectedDay == 7) {
            const nextRewardTime = dailyRewards.nextRewardTime
            console.log('dailyRewards.lastCollectedDay == 7 :>> ', dailyRewards.lastCollectedDay == 7);
            let reward = []
            rewards.forEach(async (element) => {
                console.log('element :>> ', element);
                if (element.day == 1) {
                    const resetObj = {
                        day: element.day,
                        rewardPoint: element.rewardPoint,
                        action: "next",
                        collectedTime: null
                    }
                    reward.push(resetObj);
                } else {
                    const resetObj = {
                        day: element.day,
                        rewardPoint: element.rewardPoint,
                        action: "",
                        collectedTime: null
                    }
                    reward.push(resetObj)
                }
            });
            console.log('reward :: reseted :>> ', reward);
            await Dailyrewards.updateOne({ userId: userId }, { dailyReward: reward, nextRewardTime }, { upsert: true, new: true })
        }

    } catch (error) {
        console.log('error :>> ', error, error.message);
        throw error
    }
}