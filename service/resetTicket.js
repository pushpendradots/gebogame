const { Table } = require("../models/Table");
const HelperUtils = require("../utils/helpers");
const roundTimer = require('./roundTimer');


module.exports = async function resetTicket(tableId, roomSize, socketio, socket) {
    try {
        console.log('tableId :>> ', tableId, typeof tableId);
        const tableData = await Table.findOne({ _id: tableId });
        let currantRoun = tableData.currentRoundCounter + 1
        await Table.updateOne({ _id: tableId }, { $set: { "currentRoundCounter": currantRoun } }, { new: true, upsert: true });
        const tDatil = await Table.findOne({ _id: tableId });
       
        if((tDatil.currentRoundCounter - 1) % 6 == 0  && (tDatil.currentRoundCounter - 1) != 0) {
            setTimeout(async () => {
                if (tDatil.userData.length > 1) {
                    if (tDatil.currentRoundCounter === 121) {
        
                        await Table.updateOne({ _id: tableId }, { $set: { "currentRoundCounter": 1 } }, { new: true, upsert: true });
                        const resetObj = {
                            resetTicket: true,
                        }
                        socketio.to(tableId).emit("RESET_TICKET", HelperUtils.successResponseOBJ("RESET_TICKET", resetObj))
                        setTimeout(async () => {
                            await roundTimer(tableId, roomSize, socketio, socket)
                        }, 5000);
                    } else {
                        await roundTimer(tableId, roomSize, socketio, socket)
                    }
                }
            },6000)
        }else{
            if (tDatil.userData.length > 1) {
                if (tDatil.currentRoundCounter === 121) {
    
                    await Table.updateOne({ _id: tableId }, { $set: { "currentRoundCounter": 1 } }, { new: true, upsert: true });
                    const resetObj = {
                        resetTicket: true,
                    }
                   socketio.to(tableId).emit("RESET_TICKET", HelperUtils.successResponseOBJ("RESET_TICKET", resetObj))
                    setTimeout(async () => {
                        await roundTimer(tableId, roomSize, socketio, socket)
                    }, 5000);
                } else {
                    await roundTimer(tableId, roomSize, socketio, socket)
                }
            }
        }
       

    } catch (error) {
        console.log('resetTicket : error :>> ', tableId, " - ", error);
        throw error
    }
}