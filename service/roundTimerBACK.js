const { Table } = require("../models/Table");
const { User } = require("../models/User");
const HelperUtils = require("../utils/helpers");
const adminTicketOpen = require('./adminTicketOpen');
const setWinnData = require('./setWinnData');
const updateLevelDetail = require('./updateLevelDetail');
const botBating = require('./botBeting');
const setBetData = require("./setBetData");


module.exports = async function roundTimer(tableId, roomSize, socketio, socket) {
    try {
        const table = await Table.findOne({ _id: tableId });
        const playerCount = table.userData
        let tableDetails = await Table.updateOne({ _id: tableId }, { $set: { minPlayerSeat: playerCount.length } }, { upsert: true });

        if (table.isAdminStartRound) {
            let startTime = new Date();
            let updateRoundObj = {
                currentRoundStartTimer: startTime,
                isRoundStart: true
            }
            await Table.updateOne({ _id: tableId }, { $set: updateRoundObj })
        }
        const tDetail = await Table.findOne({ _id: tableId });
        // const userData = tDetail.userData
        const adminDetail = tDetail.adminDetail
        console.log('adminDetail :>> ', adminDetail);
        const getAdminData = await User.findOne({ _id: adminDetail.adminUserId })
        console.log('getAdminData :>> ', getAdminData);
        const betlimit = (getAdminData.walletBalance * 1) / 100;
        if (tDetail) {
            let obj = {
                turn_time: 5,
                adminUid: tDetail.adminDetail.adminUserId.toString(),
                tableBetLimit: betlimit
            }
           //update table status to timer start
            await Table.updateOne({ _id: tableId }, { tableStatus: "timerStart" }, { upsert: true, new: true })
            await communRoomevent(tableId, "TURN_TIMER", obj, socket)
            // socketio.to(tableId).emit("TURN_TIMER", HelperUtils.successResponseOBJ("TURN_TIMER", obj));

            await Table.updateOne({ _id: tableId }, { $set: { isTicketOpenByAdmin: false } }, { upsert: true, new: true })

        }
        const tableDetail = await Table.findOne({ _id: tableId });
        let counter = tableDetail.currentRoundCounter

        // user turn start 
        setTimeout(async () => {
            obj = {
                round_time: 15,
                adminUid: tDetail.adminDetail.adminUserId.toString()
            }
            await communRoomevent(tableId, "USER_TURN_START", obj, socket)
            await Table.updateOne({ _id: tableId }, { tableStatus: "inBatting" }, { upsert: true, new: true })
            const userData = tableDetail.userData
            for (let i = 0; i < userData.length; i++) {
                const element = userData[i];

                console.log('roundTimer :: botbating :: element.userId :>> ', element.userId);
                await botBattingDelay(element.userId, tableId, socket)
            }


            for (let i = 0; i < userData.length; i++) {
                const element = userData[i];
                await updateLevelDetail(element.userId, socket)
            }
            // socketio.to(tableId).emit("USER_TURN_START", HelperUtils.successResponseOBJ("USER_TURN_START", obj));
            // user bet time over
            setTimeout(async () => {
                obj = {
                    adminTicitOpen_time: 5,
                    currentRoundCounter: counter,
                    adminUid: tDetail.adminDetail.adminUserId.toString()
                };
                await communRoomevent(tableId, "USER_BET_TURN_OVER", obj, socket)
                await Table.updateOne({ _id: tableId }, { tableStatus: "winnerDeclared" }, { upsert: true, new: true })
                // socketio.to(tableId).emit("USER_BET_TURN_OVER", HelperUtils.successResponseOBJ("USER_BET_TURN_OVER", obj));
                // ticket open event
                setTimeout(async () => {

                    const tDatil = await Table.findOne({ _id: tableId });
                    if(tDatil) {

                        const isTicketOpenByAdmin = tDatil.isTicketOpenByAdmin
                        
                        if (!isTicketOpenByAdmin) {
                            const randomTicket = await adminTicketOpen(tableId, isTicketOpenByAdmin) // do 4
                            obj = {
                                ticketLatter: randomTicket,
                                currentRoundCounter: counter,
                                openByAdmin: false
                            }
                            await communRoomevent(tableId, "ADMIN_TICKET_OPEN", obj, socket)
                            // socketio.to(tableId).emit("ADMIN_TICKET_OPEN", HelperUtils.successResponseOBJ("ADMIN_TICKET_OPEN", obj));
                            // winner declared 
                            setWinnData(tableId, randomTicket, socket, socketio, roomSize); // do 1
                        } else {
                            console.log("Ticket Opened By Admin");
                        }
                    }
                }, 5000)

            }, 15000);

        }, 5000);
    } catch (error) {
        console.log('CATCH_ERROR: roundTimer :>> ', error);
        throw error
    }

};

async function communRoomevent(tableId, eventName, obj, socket) {
    try {
        const tDetail = await Table.findOne({ _id: tableId })
        if (tDetail) {
            const adminDeatil = tDetail.adminDetail
            if (adminDeatil.socketId != socket.id) {

                // socket.in(tableId).emit(eventName, HelperUtils.successResponseOBJ(eventName, obj));
                io.to(tableId).emit(eventName, HelperUtils.successResponseOBJ(eventName, obj))
            } else {

                const userData = tDetail.userData
                // const playerArray = []
                for (let i = 0; i < userData.length; i++) {
                    const element = userData[i];
                    if (element.isComp == false) {
                        socket.to(element.socketId).emit(eventName, HelperUtils.successResponseOBJ(eventName, obj));
                        // playerArray.push(element)
                    }
                }

                socket.emit(eventName, HelperUtils.successResponseOBJ(eventName, obj));

            }
        }

    } catch (error) {
        console.log('communRoomevent ::: error :>> ', error);
        throw error
    }
}

async function botBattingDelay(userId, tableId, socket) {
    try {
        const rendomBetTime = [3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000]

        const rendomDelay = Math.floor(Math.random() * rendomBetTime.length);
        console.log('rendomDelay :>> ', rendomDelay);
        const tDetail = await Table.findOne({ _id: tableId })
        const adminDetail = tDetail.adminDetail
        const adminUser = await User.findById(adminDetail.adminUserId)
        setTimeout(async () => {
            const userdetail = await User.findOne({ _id: userId })
            if (userdetail) {
                if (userdetail.isComp == true) {
                    const data = await botBating(userId, tableId, socket)

                    const botData = await setBetData(data, socket , true);
                  
                    await Table.updateOne({ _id: tableId }, { $set: { "userData": botData } }, { new: true, upsert: true });

                    const botBetData = botData.find(el => el.userId.toString() === userId.toString());
                  

                    let betDetailForAdmin = {
                        userId: userId.toString(),
                        betData: botBetData.userBet,
                        adminWalletBalance: adminUser.walletBalance
                    }
                 
                    if (socket.id != tDetail.adminDetail.socketId) {

                        socket.to(tDetail.adminDetail.socketId).emit("ADMIN_BET", HelperUtils.successResponseOBJ("ADMIN_BET", betDetailForAdmin))
                    } else {
                        socket.emit("ADMIN_BET", HelperUtils.successResponseOBJ("ADMIN_BET", betDetailForAdmin))
                    }
                }
            }
        }, rendomBetTime[rendomDelay])
    } catch (error) {
        console.log('CATCH_ERROR :: botBattingDelay :>> ', error);
        throw error
    }
}