const HelperUtils = require("../utils/helpers");


module.exports = async function sendTableStatus(userId, tStatus, socketId , socket) {
    try {
        const fList = {
            userId: userId,
            tableStatus: tStatus
        };
        // return fList;
        if (socket.id == socketId) {
            socket.emit("TABLE_STATUS_LOCAL_FRIEND", HelperUtils.successResponseOBJ("TABLE_STATUS_LOCAL_FRIEND", fList))
        } else {

            socket.to(socketId).emit("TABLE_STATUS_LOCAL_FRIEND", HelperUtils.successResponseOBJ("TABLE_STATUS_LOCAL_FRIEND", fList))
        }

    } catch (error) {
        console.log('CATCH_ERROR : getLocalFriends  :>> ', error);
    }
}

