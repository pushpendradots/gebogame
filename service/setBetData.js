const { Table } = require("../models/Table");
const {User} = require("../models/User");
// const botBatting = require('./botBatting');
module.exports = async function setBetData(data, socket , isComp) {
    try {
        console.log('data :>> ', data);
        const tableId = data.tableId;
        const userId = data.userId;
        const bets = data.bets;

        const tableData = await Table.findOne({ _id: tableId })
        const adminDetail = tableData.adminDetail

        let totalBetAmount = 0;
        bets.forEach(element => {
            totalBetAmount += element.betLattereAmount
        });

        console.log('totalBetAmount a:>> ', totalBetAmount);
        const getAdminData = await User.findOne({_id: adminDetail.adminUserId})
        const updatedBalance = getAdminData.walletBalance ? getAdminData.walletBalance + totalBetAmount : getAdminData.walletBalance
        await User.updateOne({_id: adminDetail.adminUserId }, {walletBalance: updatedBalance}, {upsert: true, new: true})
        const userData = tableData.userData
        const updateArray = [];
        userData.forEach(element => {
            let initialUserBet = element.userBet;
            let userBet = bets;

            var key = "betLatter";
            if (userId.toString() === element.userId.toString()) {

                versions = initialUserBet.map(el => {
                    var found = userBet.find(s => s[key] === el[key]);
                    if (found) {
                        if (el && el.betLattereAmount !== 0) {
                            found.betLattereAmount = found.betLattereAmount + el.betLattereAmount;
                            found.percentage = found.percentage + el.percentage;
                        }
                        el = Object.assign(el, found);
                    }
                    return el;
                });

                updateArray.push({
                    userId,
                    isComp,
                    userBet: versions,
                    socketId: socket.id
                })
            } else {
                updateArray.push({
                    userId: element.userId,
                    isComp: element.isComp,
                    userBet: initialUserBet,
                    socketId: socket.id
                })
            }
            return updateArray;
        });
        return updateArray;
    } catch (error) {
        console.log('setBetData :>> ', data, "-", error);
        throw error
    }
}
