const {User} = require('../models/User');
const getRemainingRewardTime = require('./getRemainingRewardTime');
const HelperUtils = require("../utils/helpers");


// for daily reward set commun daily reward response
module.exports = async function setDailyRewardEventResponse(userId, rewardPoint , socket) {
    try {

        const dailyReward = rewardPoint.dailyReward;
        console.log('dailyReward :>> ', dailyReward);
        const remainigTime = await getRemainingRewardTime(userId)
        console.log('remainigTime :>> ', remainigTime);
        const userDetail = await User.findOne({ _id: userId })
        const resObj = {
            dailyReward,
            remainigTime,
            walletBalance: userDetail.walletBalance
        };

        console.log('resObj :>> ', resObj);
        return resObj;
        // if (socket.id != userDetail.socketId) {

        //     socket.to(userDetail.socketId).emit("DAILY_REWARDS", HelperUtils.successResponseOBJ("DAILY_REWARDS", resObj));
        // } else {

        //     socket.emit("DAILY_REWARDS", HelperUtils.successResponseOBJ("DAILY_REWARDS", resObj))
        // }

    } catch (error) {
        console.log('CATCH_ERROR : setDailyRewardEventResponse :>> ', error, error.message, " - ", userId);
        throw error
    }
}