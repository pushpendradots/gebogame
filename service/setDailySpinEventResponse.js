const { DailySpins } = require("../models/dailySpin");
const moment = require('moment');
const { User } = require("../models/User");
const getRemainingSpinTime = require('./getRemainingSpinTime');
const HelperUtils = require("../utils/helpers");


module.exports = async function setDailySpinEventResponse(userId, isAdSpin , socket) {
    try {

        const spinDetail = await DailySpins.findOne({ userId })
        const dailySpinReward = spinDetail.dailySpinReward
       
        const currantDateAndTime = moment(new Date());
        const nextSpin = moment(new Date()).add('1', 'd');

        let tempSpinArray = [1000, 2000, 4000, 5000]
        const rendomIndex = Math.floor(Math.random() * tempSpinArray.length);
        
        const randomTicket = tempSpinArray[rendomIndex];
    
        const earnIndex = dailySpinReward.indexOf(randomTicket, 0);
       
        const userDetail = await User.findOne({ _id: userId });
        const socketId = userDetail.socketId

        // update user reward point
        const rewardPoint = userDetail.rewardPoint == 0 ? randomTicket : userDetail.rewardPoint + randomTicket;
     
        await User.updateOne({ _id: userId }, { rewardPoint: rewardPoint }, { upsert: true, new: true })

        if (isAdSpin == "") {
            await DailySpins.updateOne({ userId: userId }, { lastCollectedSpinTime: currantDateAndTime, nextSpinTime: nextSpin }, { upsert: true, new: true });
            const remainigSpinTime = await getRemainingSpinTime(userId)
           
            isAdSpin = true
            await DailySpins.updateOne({ userId }, { isAdSpin: isAdSpin }, { upsert: true, new: true })

            const user = await User.findById(userId);
           console.log('user :>> ', user)
            const balance = user.walletBalance != 0 ? user.walletBalance + randomTicket : randomTicket

           await User.updateOne({_id: userId},{walletBalance: balance },{ upsert: true, new: true})

            let resObj = {
                index: earnIndex,
                dailySpinReward,
                walletBalance: balance,
                isAdSpin: isAdSpin,
                remaingTime: remainigSpinTime
            }
            return resObj;
            console.log(' DAILY_SPIN ::: resObj :>> ', resObj);
            res.send(HelperUtils.successObj("DAILY_SPIN", resObj));
            // socket.emit("DAILY_SPIN", HelperUtils.successResponseOBJ("DAILY_SPIN", resObj))

        } else if (isAdSpin == "true") {
            const remainigSpinTime = await getRemainingSpinTime(userId)
            isAdSpin = false
            await DailySpins.updateOne({ userId }, { isAdSpin: isAdSpin }, { upsert: true, new: true })

            const user = await User.findById(userId);
           
            const balance = user.walletBalance != 0 ? user.walletBalance + randomTicket : randomTicket

           await User.updateOne({_id: userId},{walletBalance: balance },{ upsert: true, new: true})

            let resObj = {
                index: earnIndex,
                dailySpinReward,
                walletBalance: balance,
                isAdSpin: isAdSpin,
                remaingTime: remainigSpinTime
            }
            return resObj;
            console.log(' DAILY_SPIN ::: resObj :>> ', resObj);
            socket.emit("DAILY_SPIN", HelperUtils.successResponseOBJ("DAILY_SPIN", resObj))
        } else {
            const remainigSpinTime = await getRemainingSpinTime(userId)
            // remaningTime
            const spinDetail = await DailySpins.findOne({ userId })
            isAdSpin = spinDetail.isAdSpin

            const user = await User.findById(userId);

            let resObj = {
                index: 100,
                dailySpinReward,
                walletBalance: user.walletBalance,
                isAdSpin: isAdSpin,
                remaingTime: remainigSpinTime
            }
            console.log(' DAILY_SPIN ::: resObj :>> ', resObj);
            return resObj;
            // if (socket.id != socketId) {
            //     socket.to(socketId).emit("DAILY_SPIN", HelperUtils.successResponseOBJ("DAILY_SPIN", resObj));
            // } else {

            //     socket.emit("DAILY_SPIN", HelperUtils.successResponseOBJ("DAILY_SPIN", resObj))
            // }
        }
    } catch (error) {
        console.log('CATCH_ERROR :: setDailySpinEventResponse :>> ', error);
        throw error
    }
}
