const { UserLevel } = require('../models/userLevel');
const HelperUtils = require("../utils/helpers");
const levelDetail = require('./userStaticLevel');
module.exports = async function setLevelUpEventResponse(userId) {
    try {
        const levelDetails = await UserLevel.findOne({ userId: userId });
        const currantLevel = levelDetails.userlevel
        levelDetail
        const voucherNewArray = [];
        if (currantLevel >= 101) {
            for (let i = 0; i < 10; i++) {
                const Valve = parseInt(currantLevel / 10);
                let obj = {
                    level: i + (Valve * 10),
                    voucher: 10000
                }
                voucherNewArray.push(obj)
            }

        } else {
            console.log(parseInt(currantLevel / 10));
            const Valve = parseInt(currantLevel / 10);
            console.log('levelDetail :>> ', levelDetail);
            for (let i = 0; i < 10; i++) {
                voucherNewArray.push(levelDetail[i + (Valve * 10)])
            }

        }
        console.log('voucherNewArray :>> ', voucherNewArray);

        const levelObj = {
            userlevel: levelDetails.userlevel,
            userlevelRound: levelDetails.userlevelRound,
            levelUpRound: levelDetails.levelUpRound,
            levelUpPrice: levelDetails.levelUpPrice,
            userVoucher: levelDetails.userVoucher,
            voucherNewArray
        };

        return levelObj
    } catch (error) {
        console.log('CATCH_ERROR :: setLevelUpEventResponse :>> ', error);
        throw error
    }
}


