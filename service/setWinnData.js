const { Table } = require("../models/Table");
const { User } = require("../models/User");
const { UserLevel } = require("../models/userLevel");
const HelperUtils = require("../utils/helpers");
const adminLeave = require('./adminLeave');
const { reject } = require("lodash");


module.exports = async function setWinnData(tableId, WinnTicket, socket, socketio, roomSize) {
    try {
        const resetTicket = require('./resetTicket');
        const tableDetail = await Table.findOne({ _id: tableId });
        if (tableDetail) {

            await Table.updateOne({ _id: tableId }, { tableStatus: "winnerDeclared" }, { upsert: true, new: true });
            const userData = tableDetail.userData
            console.log('userData :>> ', userData);

            const adminDetail = tableDetail.adminDetail
            const adminUid = adminDetail.adminUserId

            let lossAmountArray = []

            let totalBetAmountArray = []

            let userWinnAmountArray = []
            for (let i = 0; i < userData.length; i++) {
                const element = userData[i];

                const userBet = element.userBet

                // sum of total bet of user
                const userTotalBet = userBet.map((item) => item.betLattereAmount).reduce((pre, next) => pre + next);

                // push total bet of user in totalBetAmountArray
                totalBetAmountArray.push(userTotalBet);

                for (let j = 0; j < userBet.length; j++) {
                    const el = userBet[j];
                    if (WinnTicket == el.betLatter) {
                        userWinnAmountArray.push(el.betLattereAmount * 10)
                    }
                }

            }
            const totalBetAmount = totalBetAmountArray.reduce((partialSum, a) => partialSum + a, 0);

            const totalWinnAmount = userWinnAmountArray.reduce((partialSum, a) => partialSum + a, 0);


            const admin = await User.findOne({ _id: adminUid })


            for (let i = 0; i < userData.length; i++) {
                const element = userData[i];


                const userDetail = await User.findOne({ _id: element.userId });
                const socketId = userDetail.socketId

                let loss = 0
                if (totalBetAmountArray[i] < userWinnAmountArray[i]) {
                    loss = 0
                } else {

                    loss = totalBetAmountArray[i] - userWinnAmountArray[i];
                }
                lossAmountArray.push(loss)


                const getAdmin = await User.findOne({ _id: adminUid })
                const adminWalletBalance = getAdmin.walletBalance;

                // deduct admin balance eq! winner
                const dedectAmount = adminWalletBalance != 0 ? adminWalletBalance - Number(userWinnAmountArray[i].toFixed(2)) : adminWalletBalance;

                await User.updateOne({ _id: adminUid }, { walletBalance: dedectAmount }, { upsert: true, new: true })

                const wb = userDetail.walletBalance != 0 ? userDetail.walletBalance + Number(userWinnAmountArray[i].toFixed(2)) : Number(userWinnAmountArray[i].toFixed(2))

                await User.updateOne({ _id: element.userId }, { walletBalance: wb }, { upsert: true, new: true })
                const user = await User.findById(element.userId)
                const winnerObj = {
                    userId: element.userId,
                    walletBalance: Number(user.walletBalance.toFixed(2)),
                    winAmount: Number(userWinnAmountArray[i].toFixed(2)),
                    lossAmount: Number(loss.toFixed(2)),
                    Tbet: totalBetAmountArray[i],
                    socketId
                }
                console.log('winnerObj :>> ', winnerObj);
                console.log('socket.id :>> ', socket.id);
                if (element.isComp == false) {
                    if (socket.id == socketId) {
                        socket.emit("WINNER_DECLARED", HelperUtils.successResponseOBJ("WINNER_DECLARED", winnerObj));
                    } else {
                        socket.to(socketId).emit("WINNER_DECLARED", HelperUtils.successResponseOBJ("WINNER_DECLARED", winnerObj));
                    }
                }
            }

            if (tableDetail.isStatic == false) {
                let totalLossAmount;
                if (totalWinnAmount > totalBetAmount) {
                    totalLossAmount = 0;
                } else {
                    totalLossAmount = totalBetAmount - totalWinnAmount
                }


                const getAdminDetail = await User.findOne({ _id: adminUid });

                let roundBetObj = {
                    userId: adminUid,
                    walletBalance: Number(getAdminDetail.walletBalance.toFixed(2)), // admin nu balance
                    winAmount: totalLossAmount,
                    lossAmount: totalWinnAmount,
                    Tbet: totalBetAmount,
                    socketId: adminDetail.socketId
                }
                if (socket.id != adminDetail.socketId) {
                    socket.to(adminDetail.socketId).emit("WINNER_DECLARED", HelperUtils.successResponseOBJ("WINNER_DECLARED", roundBetObj));
                } else {

                    socket.emit("WINNER_DECLARED", HelperUtils.successResponseOBJ("WINNER_DECLARED", roundBetObj));
                }
            }


            let tempBetData = []
            for (let i = 0; i < userData.length; i++) {
                const element = userData[i];
                const userbet = element.userBet
                const userId = element.userId
                const userDetail = await User.findOne({ _id: userId })
                let userBetReset = [
                    { betLatter: 1, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 2, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 3, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 4, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 5, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 6, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 7, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 8, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 9, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 10, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 11, betLattereAmount: 0, percentage: 0 },
                    { betLatter: 12, betLattereAmount: 0, percentage: 0 },
                ]
                let usdatedObj = {
                    userId: userId,
                    userBet: userBetReset,
                    isComp: element.isComp,
                    socketId: userDetail.socketId
                }
                console.log('usdatedObj :>> ', usdatedObj);
                tempBetData.push(usdatedObj);
            }

            console.log('tempBetData :>> ', tempBetData);

            await Table.updateOne({ _id: tableId }, { $set: { userData: tempBetData } });

            setTimeout(async () => {
                const tDatil = await Table.findOne({ _id: tableId });

                const isAdminOnline = await User.findById(tDatil.adminDetail.adminUserId);
                const adminUserId = tDatil.adminDetail.adminUserId
                if (isAdminOnline.isActive == true || tDatil.isStatic == true) {
                    let getRoom
                    const rooms = socket.sockets;
                    console.log('rooms :>> ', rooms);

                    if (rooms) {
                        getRoom = socket.sockets.adapter.rooms.get(tableId)
                    } else {
                        getRoom = socket.adapter.rooms.get(tableId)
                    }
                    console.log('getRoom :>> ', getRoom);
                    if (getRoom) {
                        const room = getRoom.size
                        console.log('setWinnData :: room :>> ', room);

                        if (room >= 1) {
                            if (tDatil.adminTicketSkipCount >= 3) {
                                const message = "you left table due to not response."
                                const userMessage = "you are leave table due to admin inactivity."

                                await adminLeave(adminUserId, tableId, message, userMessage, socket, socketio) // do 3

                            } else {
                                if (tDatil.isAdminStartRound == true) {
                                    console.log('setWinnData :: roomSize :>> ', roomSize);
                                    await resetTicket(tableId, roomSize, socketio, socket)
                                } else {
                                    await Table.updateOne({ _id: tableId }, { tableStatus: "inWaiting" }, { upsert: true, new: true })
                                    const waitingobj = {
                                        message: `Game pouse by admin please wait for game start by admin`,
                                        adminUid: adminUserId.toString()
                                    }
                                    socket.to(tableId).emit("WAIT", HelperUtils.successResponseOBJ("WAIT", waitingobj))
                                }
                            }
                        } else {
                            await Table.updateOne({ _id: tableId }, { tableStatus: "inWaiting" }, { upsert: true, new: true })
                            await deleteStaticTable(tableId)
                        }
                    } else {
                        await Table.updateOne({ _id: tableId }, { tableStatus: "inWaiting" }, { upsert: true, new: true });
                        await deleteStaticTable(tableId)
                    }
                } else {
                    const message = "you left table due to not response."
                    const userMessage = "you are leave table due to admin inactivity."

                    await adminLeave(adminUserId, tableId, message, userMessage, socket, socketio)
                }


            }, 5000)
        }


    } catch (error) {
        console.log('CATCH_ERROR :: setWinnData ::>> ', "-", error, error.message, error.stack);
        throw error
    }

}


async function deleteStaticTable(tableId) {
    try {
        const tableDetail = await Table.findById(tableId);
        if (tableDetail.isStatic == true) {
            const getAllTable = await Table.find({ isStatic: true })
            if (getAllTable.length > 10) {
                const tDetail = await Table.findOne({ _id: tableId });
                const userCount = tDetail.userData
                let realUserCount = 0;
                for (let element of userCount) {
                    if (element.isComp == false) {
                        realUserCount = realUserCount + 1
                    }
                }

                if (realUserCount == 0) {

                    new Promise(async (resolve, reject) => {
                        for (let index = 0; index < userCount.length; index++) {
                            if (userCount[index].isComp == true) {
                                const deletBot = await User.deleteOne({ _id: userCount[index].userId })
                                await Table.updateOne({ _id: tableId }, { $pull: { userData: { userId: userCount[index].userId } } }, { safe: true, multi: true })

                                console.log('deletBot :>> ', deletBot, userCount[index].userId);

                            }
                        }
                        resolve()
                    }).then(async (data) => {
                        const getTable = await Table.findById(tableId);

                        const deleteTable = await Table.findOneAndDelete({ _id: tableId })

                    }).catch(error => {
                        console.log('CATCH_ERROR :: deleteStaticTable :>> ', error, error.message);
                        reject()
                    })

                }
            } else {
                console.log('Else for getAllTable.length > 10');
            }
        }
    } catch (error) {
        console.log('CATCH_ERROR :: deleteStaticTable  :>> ', error);
    }
}

