const { UserLevel } = require("../models/userLevel");
const config = require("config");
const levelDetail = require('./userStaticLevel');
const HelperUtils = require("../utils/helpers");
const { User } = require("../models/User");
const setLevelUpEventResponse = require('./setLevelUpEventResponse');
const getCourrantLevelVoucher = require('./getCurrantLevelVoucher');

module.exports = async function updateLevelDetail(userId, socket) {
    try {
        const VOUCHER_DIVIDE = config.get("VOUCHER_DIVIDE");
   
        const userLevelDetail = await UserLevel.findOne({ userId: userId });
        if (userLevelDetail) {
            if (userLevelDetail.userlevelRound != userLevelDetail.levelUpRound) {
                const ticketRoundCount = userLevelDetail.userlevelRoundCount == 0 ? 1 : userLevelDetail.userlevelRoundCount + 1
                console.log('ticketRoundCount :>> ', ticketRoundCount);
                await UserLevel.updateOne({ userId: userId }, { userlevelRoundCount: ticketRoundCount }, { upsert: true, new: true });
                if (ticketRoundCount == 6) {
                    const round = userLevelDetail.userlevelRound == 0 ? 1 : userLevelDetail.userlevelRound + 1
                    console.log('round :>> ', round);
                    await UserLevel.updateOne({ userId: userId }, { userlevelRound: round, userlevelRoundCount: 1 }, { upsert: true, new: true });
                }
            } else {

                // update get voucher

                if (userLevelDetail.userlevel >= 402) {
                    const findPriceVoucher = levelDetail.find((element) => {
                        if (element.level == 402) {
                            return element
                        }
                    });
                    console.log('findPriceVoucher :>> ', findPriceVoucher);
                    const update = {
                        level: userLevelDetail.userlevel,
                        isClaimed: false,
                        levelPrice: userLevelDetail.levelUpPrice
                    }
                    await UserLevel.updateOne({userId: userId},{$push: {claimedVoucher: update}}, {upsert: true, new: true})
                    // await UserLevel.updateOne({ userId: userId }, { $push: { userVoucher: findPriceVoucher.voucher } }, { upsert: true, new: true })

                    const userLevelUp = userLevelDetail.userlevel + 1
                    await UserLevel.updateOne({ userId: userId }, { userlevel: userLevelUp, userlevelRound: 0 }, { upsert: true, new: true });
                    await User.updateOne({ _id: userId }, { userLevel: userLevelUp }, { upsert: true, new: true })

                    const round = userLevelDetail.userlevelRoundCount == 0 ? 1 : userLevelDetail.userlevelRoundCount + 1
                    console.log('round :>> ', round);
                    await UserLevel.updateOne({ userId: userId }, { userlevelRoundCount: round, }, { upsert: true, new: true });

                    const levelUpRequierRound = findPriceVoucher.voucher / VOUCHER_DIVIDE
                    await UserLevel.updateOne({ userId: userId }, { levelUpPrice: findPriceVoucher.voucher, levelUpRound: levelUpRequierRound }, { upsert: true, new: true })
                    const data = {
                        userId,
                        level: updatedLevel.userlevel
                    }
                   const resp =  await getCourrantLevelVoucher(data)
                   socket.emit("LEVEL_UP", HelperUtils.successResponseOBJ("LEVEL_UP",resp))
                } else {
                    const findPriceVoucher = levelDetail.find((element) => {
                        if (element.level == userLevelDetail.userlevel) {
                            return element
                        }
                    });
                    console.log('findPriceVoucher :>> ', findPriceVoucher);

                    const update = {
                        level: userLevelDetail.userlevel,
                        isClaimed: false,
                        levelPrice: userLevelDetail.levelUpPrice
                    }
                    await UserLevel.updateOne({userId: userId},{$push: {claimedVoucher: update}}, {upsert: true, new: true})
                    const userLevelUp = userLevelDetail.userlevel + 1
                    await UserLevel.updateOne({ userId: userId }, { userlevel: userLevelUp, userlevelRound: 0 }, { upsert: true, new: true });
                    await User.updateOne({ _id: userId }, { userLevel: userLevelUp }, { upsert: true, new: true })

                    // update round count
                    const round = userLevelDetail.userlevelRoundCount == 0 ? 1 : userLevelDetail.userlevelRoundCount + 1
                    console.log('round :>> ', round);
                    await UserLevel.updateOne({ userId: userId }, { userlevelRoundCount: round, }, { upsert: true, new: true })

                    const updatedLevel = await UserLevel.findOne({ userId: userId })
                    console.log('updatedLevel :>> ', updatedLevel);

                    const newLevelPrice = levelDetail.find((element) => element.level == updatedLevel.userlevel);
                    console.log('newLevelPrice :>> ', newLevelPrice);
                    
                    const levelUpRequierRound = newLevelPrice.voucher / VOUCHER_DIVIDE
                    console.log('levelUpRequierRound :>> ', levelUpRequierRound);
                    await UserLevel.updateOne({ userId: userId }, { levelUpPrice: newLevelPrice.voucher, levelUpRound: levelUpRequierRound }, { upsert: true, new: true })

                    
                    const data = {
                        userId,
                        level: updatedLevel.userlevel
                    }
                   const resp =  await getCourrantLevelVoucher(data)
                   
                   socket.emit("LEVEL_UP", HelperUtils.successResponseOBJ("LEVEL_UP",resp))
                }


            }
        }


    } catch (error) {
        console.log('CATCH_ERROR :: updateLevelDetail  :>> ', error);
        throw error
    }
}