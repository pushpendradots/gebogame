const { User } = require("../models/User");
const getTableStatus = require('./getTableStatus');
const dailyRewardsCounter = require('./dailyRewardsCounter');
const setDailyRewardEventResponse = require('./setDailyRewardEventResponse');
const { Table } = require("../models/Table");
const setDailySpinEventResponse = require('./setDailySpinEventResponse');
const HelperUtils = require("../utils/helpers");
const adminLeave = require("./adminLeave");
const botJoinTable = require('./botJoinTable');
const mongoose = require('mongoose');


module.exports = async function userLeave(userId, tableId, isAdminLeft, message, socket, io) {
    try {
        //User   
        const userDetail = await User.findOne({ _id: userId });

        const tableDetail = await Table.findOne({ _id: tableId });
        console.log('tableDetail :>> ', tableDetail);
        const userData = tableDetail.userData
        console.log('userData :>> ', userData);
        const userBetData = userData.find(element => element.userId.toString() == userId.toString())
        console.log('userBetData :>> ', userBetData);
        if (userDetail.isComp == true) {
            const adminDetail = tableDetail.adminDetail
            if (adminDetail) {
                const userLeaveObj = {
                    userId: userId
                }
                socket.to(adminDetail.socketId).emit("USER_LEAVE_TABLE", HelperUtils.successResponseOBJ("USER_LEAVE_TABLE", userLeaveObj))
            };
            if (userBetData) {
                await Table.updateOne({ _id: tableId }, { $pull: { userData: { userId: userId } } }, { safe: true, multi: true })
            };
        } else {

            let obj = {
                isAdminLeft,
                message
            }
            if (socket.id != userDetail.socketId) {

                socket.to(userDetail.socketId).emit("LEAVE_TABLE", HelperUtils.successResponseOBJ("LEAVE_TABLE", obj));
            } else {
                socket.emit("LEAVE_TABLE", HelperUtils.successResponseOBJ("LEAVE_TABLE", obj));
            }

            // send event to admin to informe that user leave table
            const adminDetail = tableDetail.adminDetail
            if (adminDetail) {
                const userLeaveObj = {
                    userId: userId
                }
                socket.to(adminDetail.socketId).emit("USER_LEAVE_TABLE", HelperUtils.successResponseOBJ("USER_LEAVE_TABLE", userLeaveObj))
            };
            const isLeaveTable = true
            await getTableStatus(userId, tableId, isLeaveTable, socket);


            setTimeout(async () => {

                console.log('isAdminLeft == true :>> ', isAdminLeft == true);
                const isClaim = false,
                    dailyReward = await dailyRewardsCounter(userId, isClaim, 0);
               
                await setDailyRewardEventResponse(userId, dailyReward, socket)
                const isAdSpin = "false"
                await setDailySpinEventResponse(userId, isAdSpin, socket)
            }, 1000)

            if (userBetData) {
                await Table.updateOne({ _id: tableId }, { $pull: { userData: { userId: userId } } }, { safe: true, multi: true })
            };

            const table = await Table.findById(tableId);
            const userData = table.userData

            await Table.updateOne({ _id: tableId }, { minPlayerSeat: userData.length }, { upsert: true, new: true })

            console.log('userLeave :::: isAdminLeft == false :>> ', isAdminLeft);
            if (isAdminLeft == false) {
                socket.leave(tableId)
            }

            if (isAdminLeft == false) {
                const tDetail = await Table.findOne({ _id: tableId });
                console.log('tDetail line 141 :>> ', tDetail, tableId);
                if (tDetail) {
                    const userData = tDetail.userData;
                    let botArray = []
                    for (let i = 0; i < userData.length; i++) {
                        const element = userData[i];
                        if (element.isComp == true) {
                            botArray.push(element)
                        }
                    }

                    let isStatic = false
                    if (botArray.length < 3) {
                        if (tDetail.isStatic == true) {
                            isStatic = true
                        }
                        const userLeave = true;
                        await botJoinTable(tableId, userLeave, socket, isStatic)
                    }
                }
            }
        }


    } catch (error) {
        console.log('CATCH_ERROR userLeave  :>> ', error);
        throw error
    }
};