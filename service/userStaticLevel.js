
const levelDetail = [
    {
      level: 1,
      voucher: 10
    },
    {
      level: 2,
      voucher: 20
    },
    {
      level: 3,
      voucher: 25
    },
    {
      level: 4,
      voucher: 50
    },
    {
      level: 5,
      voucher: 75
    },
    {
      level: 6,
      voucher: 100
    },
    {
      level: 7,
      voucher: 125
    },
    {
      level: 8,
      voucher: 150
    },
    {
      level: 9,
      voucher: 175
    },
    {
      level: 10,
      voucher: 200
    },
    {
      level: 11,
      voucher: 225
    },
    {
      level: 12,
      voucher: 250
    },
    {
      level: 13,
      voucher: 275
    },
    {
      level: 14,
      voucher: 300
    },
    {
      level: 15,
      voucher: 325
    },
    {
      level: 16,
      voucher: 350
    },
    {
      level: 17,
      voucher: 375
    },
    {
      level: 18,
      voucher: 400
    },
    {
      level: 19,
      voucher: 425
    },
    {
      level: 20,
      voucher: 450
    },
    {
      level: 21,
      voucher: 475
    },
    {
      level: 22,
      voucher: 500
    },
    {
      level: 23,
      voucher: 525
    },
    {
      level: 24,
      voucher: 550
    },
    {
      level: 25,
      voucher: 575
    },
    {
      level: 26,
      voucher: 600
    },
    {
      level: 27,
      voucher: 625
    },
    {
      level: 28,
      voucher: 650
    },
    {
      level: 29,
      voucher: 675
    },
    {
      level: 30,
      voucher: 700
    },
    {
      level: 31,
      voucher: 725
    },
    {
      level: 32,
      voucher: 750
    },
    {
      level: 33,
      voucher: 775
    },
    {
      level: 34,
      voucher: 800
    },
    {
      level: 35,
      voucher: 825
    },
    {
      level: 36,
      voucher: 850
    },
    {
      level: 37,
      voucher: 875
    },
    {
      level: 38,
      voucher: 900
    },
    {
      level: 39,
      voucher: 925
    },
    {
      level: 40,
      voucher: 950
    },
    {
      level: 41,
      voucher: 975
    },
    {
      level: 42,
      voucher: 1000
    },
    {
      level: 43,
      voucher: 1025
    },
    {
      level: 44,
      voucher: 1050
    },
    {
      level: 45,
      voucher: 1075
    },
    {
      level: 46,
      voucher: 1100
    },
    {
      level: 47,
      voucher: 1125
    },
    {
      level: 48,
      voucher: 1150
    },
    {
      level: 49,
      voucher: 1175
    },
    {
      level: 50,
      voucher: 1200
    },
    {
      level: 51,
      voucher: 1225
    },
    {
      level: 52,
      voucher: 1250
    },
    {
      level: 53,
      voucher: 1275
    },
    {
      level: 54,
      voucher: 1300
    },
    {
      level: 55,
      voucher: 1325
    },
    {
      level: 56,
      voucher: 1350
    },
    {
      level: 57,
      voucher: 1375
    },
    {
      level: 58,
      voucher: 1400
    },
    {
      level: 59,
      voucher: 1425
    },
    {
      level: 60,
      voucher: 1450
    },
    {
      level: 61,
      voucher: 1475
    },
    {
      level: 62,
      voucher: 1500
    },
    {
      level: 63,
      voucher: 1525
    },
    {
      level: 64,
      voucher: 1550
    },
    {
      level: 65,
      voucher: 1575
    },
    {
      level: 66,
      voucher: 1600
    },
    {
      level: 67,
      voucher: 1625
    },
    {
      level: 68,
      voucher: 1650
    },
    {
      level: 69,
      voucher: 1675
    },
    {
      level: 70,
      voucher: 1700
    },
    {
      level: 71,
      voucher: 1725
    },
    {
      level: 72,
      voucher: 1750
    },
    {
      level: 73,
      voucher: 1775
    },
    {
      level: 74,
      voucher: 1800
    },
    {
      level: 75,
      voucher: 1825
    },
    {
      level: 76,
      voucher: 1850
    },
    {
      level: 77,
      voucher: 1875
    },
    {
      level: 78,
      voucher: 1900
    },
    {
      level: 79,
      voucher: 1925
    },
    {
      level: 80,
      voucher: 1950
    },
    {
      level: 81,
      voucher: 1975
    },
    {
      level: 82,
      voucher: 2000
    },
    {
      level: 83,
      voucher: 2025
    },
    {
      level: 84,
      voucher: 2050
    },
    {
      level: 85,
      voucher: 2075
    },
    {
      level: 86,
      voucher: 2100
    },
    {
      level: 87,
      voucher: 2125
    },
    {
      level: 88,
      voucher: 2150
    },
    {
      level: 89,
      voucher: 2175
    },
    {
      level: 90,
      voucher: 2200
    },
    {
      level: 91,
      voucher: 2225
    },
    {
      level: 92,
      voucher: 2250
    },
    {
      level: 93,
      voucher: 2275
    },
    {
      level: 94,
      voucher: 2300
    },
    {
      level: 95,
      voucher: 2325
    },
    {
      level: 96,
      voucher: 2350
    },
    {
      level: 97,
      voucher: 2375
    },
    {
      level: 98,
      voucher: 2400
    },
    {
      level: 99,
      voucher: 2425
    },
    {
      level: 100,
      voucher: 2450
    },
    {
      level: 101,
      voucher: 2475
    },
    {
      level: 102,
      voucher: 2500
    },
    {
      level: 103,
      voucher: 2525
    },
    {
      level: 104,
      voucher: 2550
    },
    {
      level: 105,
      voucher: 2575
    },
    {
      level: 106,
      voucher: 2600
    },
    {
      level: 107,
      voucher: 2625
    },
    {
      level: 108,
      voucher: 2650
    },
    {
      level: 109,
      voucher: 2675
    },
    {
      level: 110,
      voucher: 2700
    },
    {
      level: 111,
      voucher: 2725
    },
    {
      level: 112,
      voucher: 2750
    },
    {
      level: 113,
      voucher: 2775
    },
    {
      level: 114,
      voucher: 2800
    },
    {
      level: 115,
      voucher: 2825
    },
    {
      level: 116,
      voucher: 2850
    },
    {
      level: 117,
      voucher: 2875
    },
    {
      level: 118,
      voucher: 2900
    },
    {
      level: 119,
      voucher: 2925
    },
    {
      level: 120,
      voucher: 2950
    },
    {
      level: 121,
      voucher: 2975
    },
    {
      level: 122,
      voucher: 3000
    },
    {
      level: 123,
      voucher: 3025
    },
    {
      level: 124,
      voucher: 3050
    },
    {
      level: 125,
      voucher: 3075
    },
    {
      level: 126,
      voucher: 3100
    },
    {
      level: 127,
      voucher: 3125
    },
    {
      level: 128,
      voucher: 3150
    },
    {
      level: 129,
      voucher: 3175
    },
    {
      level: 130,
      voucher: 3200
    },
    {
      level: 131,
      voucher: 3225
    },
    {
      level: 132,
      voucher: 3250
    },
    {
      level: 133,
      voucher: 3275
    },
    {
      level: 134,
      voucher: 3300
    },
    {
      level: 135,
      voucher: 3325
    },
    {
      level: 136,
      voucher: 3350
    },
    {
      level: 137,
      voucher: 3375
    },
    {
      level: 138,
      voucher: 3400
    },
    {
      level: 139,
      voucher: 3425
    },
    {
      level: 140,
      voucher: 3450
    },
    {
      level: 141,
      voucher: 3475
    },
    {
      level: 142,
      voucher: 3500
    },
    {
      level: 143,
      voucher: 3525
    },
    {
      level: 144,
      voucher: 3550
    },
    {
      level: 145,
      voucher: 3575
    },
    {
      level: 146,
      voucher: 3600
    },
    {
      level: 147,
      voucher: 3625
    },
    {
      level: 148,
      voucher: 3650
    },
    {
      level: 149,
      voucher: 3675
    },
    {
      level: 150,
      voucher: 3700
    },
    {
      level: 151,
      voucher: 3725
    },
    {
      level: 152,
      voucher: 3750
    },
    {
      level: 153,
      voucher: 3775
    },
    {
      level: 154,
      voucher: 3800
    },
    {
      level: 155,
      voucher: 3825
    },
    {
      level: 156,
      voucher: 3850
    },
    {
      level: 157,
      voucher: 3875
    },
    {
      level: 158,
      voucher: 3900
    },
    {
      level: 159,
      voucher: 3925
    },
    {
      level: 160,
      voucher: 3950
    },
    {
      level: 161,
      voucher: 3975
    },
    {
      level: 162,
      voucher: 4000
    },
    {
      level: 163,
      voucher: 4025
    },
    {
      level: 164,
      voucher: 4050
    },
    {
      level: 165,
      voucher: 4075
    },
    {
      level: 166,
      voucher: 4100
    },
    {
      level: 167,
      voucher: 4125
    },
    {
      level: 168,
      voucher: 4150
    },
    {
      level: 169,
      voucher: 4175
    },
    {
      level: 170,
      voucher: 4200
    },
    {
      level: 171,
      voucher: 4225
    },
    {
      level: 172,
      voucher: 4250
    },
    {
      level: 173,
      voucher: 4275
    },
    {
      level: 174,
      voucher: 4300
    },
    {
      level: 175,
      voucher: 4325
    },
    {
      level: 176,
      voucher: 4350
    },
    {
      level: 177,
      voucher: 4375
    },
    {
      level: 178,
      voucher: 4400
    },
    {
      level: 179,
      voucher: 4425
    },
    {
      level: 180,
      voucher: 4450
    },
    {
      level: 181,
      voucher: 4475
    },
    {
      level: 182,
      voucher: 4500
    },
    {
      level: 183,
      voucher: 4525
    },
    {
      level: 184,
      voucher: 4550
    },
    {
      level: 185,
      voucher: 4575
    },
    {
      level: 186,
      voucher: 4600
    },
    {
      level: 187,
      voucher: 4625
    },
    {
      level: 188,
      voucher: 4650
    },
    {
      level: 189,
      voucher: 4675
    },
    {
      level: 190,
      voucher: 4700
    },
    {
      level: 191,
      voucher: 4725
    },
    {
      level: 192,
      voucher: 4750
    },
    {
      level: 193,
      voucher: 4775
    },
    {
      level: 194,
      voucher: 4800
    },
    {
      level: 195,
      voucher: 4825
    },
    {
      level: 196,
      voucher: 4850
    },
    {
      level: 197,
      voucher: 4875
    },
    {
      level: 198,
      voucher: 4900
    },
    {
      level: 199,
      voucher: 4925
    },
    {
      level: 200,
      voucher: 4950
    },
    {
      level: 201,
      voucher: 4975
    },
    {
      level: 202,
      voucher: 5000
    },
    {
      level: 203,
      voucher: 5025
    },
    {
      level: 204,
      voucher: 5050
    },
    {
      level: 205,
      voucher: 5075
    },
    {
      level: 206,
      voucher: 5100
    },
    {
      level: 207,
      voucher: 5125
    },
    {
      level: 208,
      voucher: 5150
    },
    {
      level: 209,
      voucher: 5175
    },
    {
      level: 210,
      voucher: 5200
    },
    {
      level: 211,
      voucher: 5225
    },
    {
      level: 212,
      voucher: 5250
    },
    {
      level: 213,
      voucher: 5275
    },
    {
      level: 214,
      voucher: 5300
    },
    {
      level: 215,
      voucher: 5325
    },
    {
      level: 216,
      voucher: 5350
    },
    {
      level: 217,
      voucher: 5375
    },
    {
      level: 218,
      voucher: 5400
    },
    {
      level: 219,
      voucher: 5425
    },
    {
      level: 220,
      voucher: 5450
    },
    {
      level: 221,
      voucher: 5475
    },
    {
      level: 222,
      voucher: 5500
    },
    {
      level: 223,
      voucher: 5525
    },
    {
      level: 224,
      voucher: 5550
    },
    {
      level: 225,
      voucher: 5575
    },
    {
      level: 226,
      voucher: 5600
    },
    {
      level: 227,
      voucher: 5625
    },
    {
      level: 228,
      voucher: 5650
    },
    {
      level: 229,
      voucher: 5675
    },
    {
      level: 230,
      voucher: 5700
    },
    {
      level: 231,
      voucher: 5725
    },
    {
      level: 232,
      voucher: 5750
    },
    {
      level: 233,
      voucher: 5775
    },
    {
      level: 234,
      voucher: 5800
    },
    {
      level: 235,
      voucher: 5825
    },
    {
      level: 236,
      voucher: 5850
    },
    {
      level: 237,
      voucher: 5875
    },
    {
      level: 238,
      voucher: 5900
    },
    {
      level: 239,
      voucher: 5925
    },
    {
      level: 240,
      voucher: 5950
    },
    {
      level: 241,
      voucher: 5975
    },
    {
      level: 242,
      voucher: 6000
    },
    {
      level: 243,
      voucher: 6025
    },
    {
      level: 244,
      voucher: 6050
    },
    {
      level: 245,
      voucher: 6075
    },
    {
      level: 246,
      voucher: 6100
    },
    {
      level: 247,
      voucher: 6125
    },
    {
      level: 248,
      voucher: 6150
    },
    {
      level: 249,
      voucher: 6175
    },
    {
      level: 250,
      voucher: 6200
    },
    {
      level: 251,
      voucher: 6225
    },
    {
      level: 252,
      voucher: 6250
    },
    {
      level: 253,
      voucher: 6275
    },
    {
      level: 254,
      voucher: 6300
    },
    {
      level: 255,
      voucher: 6325
    },
    {
      level: 256,
      voucher: 6350
    },
    {
      level: 257,
      voucher: 6375
    },
    {
      level: 258,
      voucher: 6400
    },
    {
      level: 259,
      voucher: 6425
    },
    {
      level: 260,
      voucher: 6450
    },
    {
      level: 261,
      voucher: 6475
    },
    {
      level: 262,
      voucher: 6500
    },
    {
      level: 263,
      voucher: 6525
    },
    {
      level: 264,
      voucher: 6550
    },
    {
      level: 265,
      voucher: 6575
    },
    {
      level: 266,
      voucher: 6600
    },
    {
      level: 267,
      voucher: 6625
    },
    {
      level: 268,
      voucher: 6650
    },
    {
      level: 269,
      voucher: 6675
    },
    {
      level: 270,
      voucher: 6700
    },
    {
      level: 271,
      voucher: 6725
    },
    {
      level: 272,
      voucher: 6750
    },
    {
      level: 273,
      voucher: 6775
    },
    {
      level: 274,
      voucher: 6800
    },
    {
      level: 275,
      voucher: 6825
    },
    {
      level: 276,
      voucher: 6850
    },
    {
      level: 277,
      voucher: 6875
    },
    {
      level: 278,
      voucher: 6900
    },
    {
      level: 279,
      voucher: 6925
    },
    {
      level: 280,
      voucher: 6950
    },
    {
      level: 281,
      voucher: 6975
    },
    {
      level: 282,
      voucher: 7000
    },
    {
      level: 283,
      voucher: 7025
    },
    {
      level: 284,
      voucher: 7050
    },
    {
      level: 285,
      voucher: 7075
    },
    {
      level: 286,
      voucher: 7100
    },
    {
      level: 287,
      voucher: 7125
    },
    {
      level: 288,
      voucher: 7150
    },
    {
      level: 289,
      voucher: 7175
    },
    {
      level: 290,
      voucher: 7200
    },
    {
      level: 291,
      voucher: 7225
    },
    {
      level: 292,
      voucher: 7250
    },
    {
      level: 293,
      voucher: 7275
    },
    {
      level: 294,
      voucher: 7300
    },
    {
      level: 295,
      voucher: 7325
    },
    {
      level: 296,
      voucher: 7350
    },
    {
      level: 297,
      voucher: 7375
    },
    {
      level: 298,
      voucher: 7400
    },
    {
      level: 299,
      voucher: 7425
    },
    {
      level: 300,
      voucher: 7450
    },
    {
      level: 301,
      voucher: 7475
    },
    {
      level: 302,
      voucher: 7500
    },
    {
      level: 303,
      voucher: 7525
    },
    {
      level: 304,
      voucher: 7550
    },
    {
      level: 305,
      voucher: 7575
    },
    {
      level: 306,
      voucher: 7600
    },
    {
      level: 307,
      voucher: 7625
    },
    {
      level: 308,
      voucher: 7650
    },
    {
      level: 309,
      voucher: 7675
    },
    {
      level: 310,
      voucher: 7700
    },
    {
      level: 311,
      voucher: 7725
    },
    {
      level: 312,
      voucher: 7750
    },
    {
      level: 313,
      voucher: 7775
    },
    {
      level: 314,
      voucher: 7800
    },
    {
      level: 315,
      voucher: 7825
    },
    {
      level: 316,
      voucher: 7850
    },
    {
      level: 317,
      voucher: 7875
    },
    {
      level: 318,
      voucher: 7900
    },
    {
      level: 319,
      voucher: 7925
    },
    {
      level: 320,
      voucher: 7950
    },
    {
      level: 321,
      voucher: 7975
    },
    {
      level: 322,
      voucher: 8000
    },
    {
      level: 323,
      voucher: 8025
    },
    {
      level: 324,
      voucher: 8050
    },
    {
      level: 325,
      voucher: 8075
    },
    {
      level: 326,
      voucher: 8100
    },
    {
      level: 327,
      voucher: 8125
    },
    {
      level: 328,
      voucher: 8150
    },
    {
      level: 329,
      voucher: 8175
    },
    {
      level: 330,
      voucher: 8200
    },
    {
      level: 331,
      voucher: 8225
    },
    {
      level: 332,
      voucher: 8250
    },
    {
      level: 333,
      voucher: 8275
    },
    {
      level: 334,
      voucher: 8300
    },
    {
      level: 335,
      voucher: 8325
    },
    {
      level: 336,
      voucher: 8350
    },
    {
      level: 337,
      voucher: 8375
    },
    {
      level: 338,
      voucher: 8400
    },
    {
      level: 339,
      voucher: 8425
    },
    {
      level: 340,
      voucher: 8450
    },
    {
      level: 341,
      voucher: 8475
    },
    {
      level: 342,
      voucher: 8500
    },
    {
      level: 343,
      voucher: 8525
    },
    {
      level: 344,
      voucher: 8550
    },
    {
      level: 345,
      voucher: 8575
    },
    {
      level: 346,
      voucher: 8600
    },
    {
      level: 347,
      voucher: 8625
    },
    {
      level: 348,
      voucher: 8650
    },
    {
      level: 349,
      voucher: 8675
    },
    {
      level: 350,
      voucher: 8700
    },
    {
      level: 351,
      voucher: 8725
    },
    {
      level: 352,
      voucher: 8750
    },
    {
      level: 353,
      voucher: 8775
    },
    {
      level: 354,
      voucher: 8800
    },
    {
      level: 355,
      voucher: 8825
    },
    {
      level: 356,
      voucher: 8850
    },
    {
      level: 357,
      voucher: 8875
    },
    {
      level: 358,
      voucher: 8900
    },
    {
      level: 359,
      voucher: 8925
    },
    {
      level: 360,
      voucher: 8950
    },
    {
      level: 361,
      voucher: 8975
    },
    {
      level: 362,
      voucher: 9000
    },
    {
      level: 363,
      voucher: 9025
    },
    {
      level: 364,
      voucher: 9050
    },
    {
      level: 365,
      voucher: 9075
    },
    {
      level: 366,
      voucher: 9100
    },
    {
      level: 367,
      voucher: 9125
    },
    {
      level: 368,
      voucher: 9150
    },
    {
      level: 369,
      voucher: 9175
    },
    {
      level: 370,
      voucher: 9200
    },
    {
      level: 371,
      voucher: 9225
    },
    {
      level: 372,
      voucher: 9250
    },
    {
      level: 373,
      voucher: 9275
    },
    {
      level: 374,
      voucher: 9300
    },
    {
      level: 375,
      voucher: 9325
    },
    {
      level: 376,
      voucher: 9350
    },
    {
      level: 377,
      voucher: 9375
    },
    {
      level: 378,
      voucher: 9400
    },
    {
      level: 379,
      voucher: 9425
    },
    {
      level: 380,
      voucher: 9450
    },
    {
      level: 381,
      voucher: 9475
    },
    {
      level: 382,
      voucher: 9500
    },
    {
      level: 383,
      voucher: 9525
    },
    {
      level: 384,
      voucher: 9550
    },
    {
      level: 385,
      voucher: 9575
    },
    {
      level: 386,
      voucher: 9600
    },
    {
      level: 387,
      voucher: 9625
    },
    {
      level: 388,
      voucher: 9650
    },
    {
      level: 389,
      voucher: 9675
    },
    {
      level: 390,
      voucher: 9700
    },
    {
      level: 391,
      voucher: 9725
    },
    {
      level: 392,
      voucher: 9750
    },
    {
      level: 393,
      voucher: 9775
    },
    {
      level: 394,
      voucher: 9800
    },
    {
      level: 395,
      voucher: 9825
    },
    {
      level: 396,
      voucher: 9850
    },
    {
      level: 397,
      voucher: 9875
    },
    {
      level: 398,
      voucher: 9900
    },
    {
      level: 399,
      voucher: 9925
    },
    {
      level: 400,
      voucher: 9950
    },
    {
      level: 401,
      voucher: 9975
    },
    {
      level: 402,
      voucher: 10000
    }
  ]

module.exports = levelDetail