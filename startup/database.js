const mongoose = require("mongoose");
const { MongoClient } = require('mongodb');
const winston = require("winston");
const config = require("config");


module.exports = function connectToDatabase() {
    const db = config.get("db");
    mongoose.set('debug', true);
    mongoose.set('strictQuery', false);
    mongoose
        .connect(
            db,
            { useNewUrlParser: true, useUnifiedTopology: true }
        )
        .then(() => {
            console.log(`Connected to ${db} successfully...`);
            winston.info(`Connected to ${db} successfully...`)
        })
}
