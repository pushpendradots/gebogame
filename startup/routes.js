const error = require("../middleware/error");
// const fileUploadRoutes = require("./../routes/Fileupload");
const UserRoute = require("./../routes/User");
const TableRoute = require("./../routes/Table");
const LetterRoute = require("./../routes/Letter");
const rtcToken = require('./../routes/agoraToken');
const userApiRoute = require('./../routes/v1/User');
// const rozorPay = require('./../routes/paymentGetway');
const cors = require('cors')
const express = require("express");

module.exports = function (app) {
    app.use(express.json({ limit: '50mb' }));
    app.use(express.urlencoded({ extended: true }));

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    app.options('*', cors())
    // app.use("/api/file", fileUploadRoutes);
    // added routes files
    app.use("/api/v1/user", userApiRoute);
    app.use("/api/user", UserRoute);
    app.use("/api/table", TableRoute);
    app.use("/api/letter", LetterRoute);
    app.use("/api/agoratoken",rtcToken)
    // app.use("/api/payment",rozorPay)

    app.use(error);
}