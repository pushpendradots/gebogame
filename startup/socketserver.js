const socketio = require('socket.io')
const moment = require('moment');
const logger = require('winston')
const { Session } = require("./../models/Session")
const { User, validate } = require("../models/User");
const { FriendList } = require('../models/firendsList');
const HelperUtils = require("../utils/helpers");
const { Dailyrewards } = require('../models/dailyRewards');
const { DailySpins } = require('../models/dailySpin');
const { successResponseOBJ } = require('../utils/helpers');
const { UserLevel } = require('../models/userLevel');
const { Histroy } = require('../models/purchaseHistory');
const { VoucherOrder } = require('../models/voucherOrder');
const { Voucher } = require('../models/vouchers');
const config = require("config");
const xoxoDay = require('../XoxoDayThirdPartyApi');
const {
    joinTable,
    adminJoinTable,
    adminTicketOpen,
    setWinnData,
    dailyRewardsCounter,
    getRemainingRewardTime,
    setDailyRewardEventResponse,
    getRemainingSpinTime,
    setDailySpinEventResponse,
    adminLeave,
    userLeave,
    resetTicket,
    setBetData,
    setGameBetData,
    getCourrantLevelVoucher
} = require('../service');
const { FeedBack } = require('../models/feedBack');
const { Table } = require('../models/Table');
const { Tablebet } = require('../models/Tablebet');



function openSocketConnection() {
    this.io = global.io;
    try {
        console.log("here")
        this.io.on('connection', function (socket) {
            console.log("NEW CONNECTION FROM : " + socket.client.conn.remoteAddress);
            console.log('connection :>> ', socket.id);
            logger.info("NEW CONNECTION FROM : " + socket.client.conn.remoteAddress);
            logger.info("TOKEN : " + socket.handshake.query.token);

            socket.on("SIGN_UP", async (msg) => {
                console.log(' en :  SIGN_UP  start :>> ', msg);
                try {
                    console.log('Argument1', msg);
                    console.log('Type', typeof msg);
                    let objectData = JSON.parse(msg);
                    // check token session and
                    let data = await Session.findOne({ sid: objectData.token });
                    if (data) {
                        let userId = data.uid
                        let user = await User.findOneAndUpdate({ _id: userId }, { $set: { socketId: socket.id, isActive: true } }, { new: true, upsert: true });

                        // get users local friends list
                        const friends = await FriendList.findOne({ userId: userId });

                        let allLocalFriendsList = [];
                        let friendReauqet;
                        if (friends != null) {
                            allLocalFriendsList = await getAllLocalFriends(friends)
                            friendReauqet = await getAllfreiendsRequest(friends)
                        } else {
                            console.log('friends != null :  else ::>> ', friends != null);
                            allLocalFriendsList = [];
                            friendReauqet = []
                        }

                        // daily reward detail
                        const isClaim = false,
                            newDailyRewards = await dailyRewardsCounter(userId, isClaim, 0);
                        const dailyReward = newDailyRewards.dailyReward;
                        const remainigTime = await getRemainingRewardTime(userId)
                        let dailyRewards = {
                            dailyReward,
                            remainigTime
                        }

                        // spin detail
                        const dailySpins = await DailySpins.findOne({ userId: userId });
                        const dailySpinReward = dailySpins.dailySpinReward,
                            isAdSpin = dailySpins.isAdSpin
                        const remaingTime = await getRemainingSpinTime(userId)
                        let dailySpin = {
                            dailySpinReward,
                            remaingTime,
                            isAdSpin
                        }

                        let reconnectObj
                        if (user.currantTableId != "") {
                            const isTableExist = await Table.findOne({ _id: user.currantTableId })
                            if (isTableExist) {
                                const userDetail = await User.findById(userId)
                                const disconnectTime = moment(userDetail.disconnectionTime)
                                const currentTime = moment(new Date());
                                let seconds;
                                seconds = currentTime.diff(disconnectTime, 'seconds')
                                console.log('seconds :>> ', seconds);
                                if (seconds < 300) {
                                    if (isTableExist.adminDetail.adminUserId == userId) {
                                        reconnectObj = {
                                            isReConnection: true,
                                            isAdmin: true,
                                            isAllreadyInTable: false,
                                            tableId: user.currantTableId
                                        }
                                    } else {
                                        reconnectObj = {
                                            isReConnection: true,
                                            isAdmin: false,
                                            isAllreadyInTable: false,
                                            tableId: user.currantTableId
                                        }
                                    }
                                }
                                else {
                                    reconnectObj = {
                                        isReConnection: false,
                                        isAdmin: false,
                                        isAllreadyInTable: true,
                                        tableId: ""
                                    }
                                    await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true })
                                }
                            } else {
                                reconnectObj = {
                                    isReConnection: false,
                                    isAdmin: false,
                                    isAllreadyInTable: true,
                                    tableId: ""
                                }
                                await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true })
                            }
                        }
                        
                        let obj = {
                            userDetails: user,
                            isSignUp: true,
                            allLocalFriendsList,
                            friendReauqet,
                            dailyRewards,
                            dailySpin,
                            reconnectObj: reconnectObj ? reconnectObj : {},
                            purchaseCoinAmount: config.get("PURCHASE_COIN_AMOUNT")
                        }
                        socket.emit("SIGN_UP", HelperUtils.successResponseOBJ("SIGN_UP", obj))

                        let onlineFriendsList = [];
                        let active = true
                        await getLocalFriends(userId, active)
                    } else {
                        let obj = {
                            userDetails: "",
                            isSignUp: false
                        }
                        socket.emit("SIGN_UP", HelperUtils.successResponseOBJ("SIGN_UP", obj))
                    }
                } catch (error) {
                    console.log('SIGN_UP :>> ', error, "-", msg, error.message);
                    socket.emit("SIGN_UP", HelperUtils.errorResponseObj("SIGN_UP", error.message, "-", error))
                }

            });

            socket.on("TEST_EVENT", async (msg) => {
                try {
                    console.log("TEST_EVENT received");
                    const obj = {
                        message: `Test event received`
                    };
                    socket.emit("TEST_EVENT_RESPONSE", HelperUtils.successResponseOBJ("TEST_EVENT", obj));
                } catch (error) {
                    console.log('TEST_EVENT :>> ', error, "-", msg, error.message);
                    socket.emit("TEST_EVENT", HelperUtils.errorResponseObj("TEST_EVENT", error.message, "-", error));
                }
            });


            // Send friend request 
            socket.on("SEND_FRIEND_REQUEST", async (msg) => {
                try {
                    console.log('SEND_FRIEND_REQUEST ', msg);

                    let objectData = JSON.parse(msg);
                    console.log('objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const friendsUserId = objectData.friendsUserId;

                    const getFriendData = await User.findOne({ _id: friendsUserId })

                    let getFriendList = await FriendList.findOne({ userId: userId })
                    if (getFriendList) {
                        const friendId = getFriendList.friendsId
                        const fUserId = getFriendData._id
                        const existFriend = friendId.find(({ userId }) => userId.toString() === fUserId.toString());
                        console.log('existFriend :>> ', existFriend);
                        if (existFriend) {
                            const obj = {
                                message: `you are alrady friend of ${existFriend.fullName}`
                            }
                            socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                        } else {
                            const userFriendDetail = await FriendList.findOne({ userId: friendsUserId });
                            console.log('uerFriendDetail :>> ', userFriendDetail);
                            let friendsRequest = [];
                            if (!userFriendDetail) {
                                const senderDetail = await User.findOne({ _id: userId })
                                console.log('senderDetail :>> ', senderDetail);
                                const senderObj = {
                                    userId: senderDetail._id,
                                    fullName: senderDetail.fullName,
                                    profileUrl: senderDetail.profileUrl,
                                    message: `friend request receive from ${senderDetail.fullName}`
                                }
                                friendsRequest.push(senderObj)
                                const friendRequstObj = new FriendList({
                                    userId: friendsUserId,
                                    friendsRequest
                                })
                                console.log('friendRequstObj :>> ', friendRequstObj);
                                await friendRequstObj.save()
                                console.log('friendRequstObj ,  :>> ', friendRequstObj, typeof friendRequstObj);
                                console.log('friendsRequest :>> ', friendsRequest);

                                const obj = {
                                    message: `friend request send to ${getFriendData.fullName}`
                                }
                                socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                                const recever = await User.findOne({ _id: friendsUserId });

                                socket.to(recever.socketId).emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));

                            } else {
                                let senderDetail = await User.findOne({ _id: userId })
                                const allFriendRequest = userFriendDetail.friendsRequest

                                const existingFriendRequest = allFriendRequest.find(({ userId }) => userId == senderDetail._id)

                                if (existingFriendRequest) {
                                    const obj = {
                                        message: `you are allready send request to ${getFriendData.fullName}`
                                    }
                                    socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                                } else {
                                    const senderObj = {
                                        message: `friend request receive from ${senderDetail.fullName}`,
                                        userId: senderDetail._id,
                                        fullName: senderDetail.fullName,
                                        profileUrl: senderDetail.profileUrl,
                                    }
                                    console.log('senderObj :>> ', senderObj);
                                    await FriendList.updateOne({ userId: friendsUserId }, { $push: { friendsRequest: senderObj } }, { upsert: true, new: true });
                                    const obj = {
                                        message: `friend request send to ${getFriendData.fullName}`
                                    }
                                    socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                                    const recever = await User.findOne({ _id: friendsUserId });

                                    socket.to(recever.socketId).emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));
                                }

                            }

                        }
                    } else {
                        const userFriendDetail = await FriendList.findOne({ userId: friendsUserId });
                        console.log('uerFriendDetail :>> ', userFriendDetail);
                        let friendsRequest = [];
                        if (!userFriendDetail) {
                            const senderDetail = await User.findOne({ _id: userId })
                            console.log('senderDetail :>> ', senderDetail);
                            const senderObj = {
                                userId: senderDetail._id,
                                fullName: senderDetail.fullName,
                                profileUrl: senderDetail.profileUrl,
                                message: `friend request receive from ${senderDetail.fullName}`
                            }
                            friendsRequest.push(senderObj)
                            const friendRequstObj = new FriendList({
                                userId: friendsUserId,
                                friendsRequest
                            })
                            console.log('friendRequstObj :>> ', friendRequstObj);
                            await friendRequstObj.save()
                            console.log('friendRequstObj ,  :>> ', friendRequstObj, typeof friendRequstObj);
                            console.log('friendsRequest :>> ', friendsRequest);

                            const obj = {
                                message: `friend request send to ${getFriendData.fullName}`
                            }
                            socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                            const recever = await User.findOne({ _id: friendsUserId });

                            socket.to(recever.socketId).emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));

                        } else {
                            let senderDetail = await User.findOne({ _id: userId })
                            const allFriendRequest = userFriendDetail.friendsRequest

                            const existingFriendRequest = allFriendRequest.find(({ userId }) => userId == senderDetail._id)

                            if (existingFriendRequest) {
                                const obj = {
                                    message: `you are allready send request to ${getFriendData.fullName}`
                                }
                                socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                            } else {
                                const senderObj = {
                                    message: `friend request receive from ${senderDetail.fullName}`,
                                    userId: senderDetail._id,
                                    fullName: senderDetail.fullName,
                                    profileUrl: senderDetail.profileUrl,
                                }
                                console.log('senderObj :>> ', senderObj);
                                await FriendList.updateOne({ userId: friendsUserId }, { $push: { friendsRequest: senderObj } }, { upsert: true, new: true });
                                const obj = {
                                    message: `friend request send to ${getFriendData.fullName}`
                                }
                                socket.emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", obj));
                                const recever = await User.findOne({ _id: friendsUserId });

                                socket.to(recever.socketId).emit("SEND_FRIEND_REQUEST", HelperUtils.successResponseOBJ("SEND_FRIEND_REQUEST", senderObj));
                            }

                        }
                    }

                } catch (error) {
                    console.log('FRIEND_REQUEST :>> ', error, "-", msg, error.message);
                    socket.emit("FRIEND_REQUEST", HelperUtils.errorResponseObj("FRIEND_REQUEST", error.message, "-", error));
                }
            });


            // ADD friend event
            socket.on("ADD_FRIEND", async (msg) => {
                console.log('ADD_FRIEND : start :>> ', msg);
                try {
                    console.log('ADD_FRIEND', msg);
                    console.log('Type', typeof msg);
                    let objectData = JSON.parse(msg);
                    console.log('objectData :>> ', objectData);
                    const accepterUserId = objectData.userId;
                    console.log('accepterUserId :>> ', accepterUserId, typeof accepterUserId);

                    const senderUserId = objectData.friendsUserId;
                    console.log('senderUserId :>> ', senderUserId, typeof senderUserId);

                    const userData = await User.findOne({ _id: accepterUserId })
                    const getUserFriendsData = await FriendList.findOne({ userId: accepterUserId });
                    console.log('getUserFriendsData :>> ', getUserFriendsData);

                    if (userData) {
                        const friendsRequest = getUserFriendsData.friendsRequest;
                        console.log('friendsRequest :>> ', friendsRequest);

                        // find singal friend request
                        const request = friendsRequest.find(e => e.userId.toString() === senderUserId.toString())


                        console.log('request :>> ', request);
                        if (request) {
                            let accepterUser = await User.findOne({ _id: accepterUserId });
                            const accepterDetail = {
                                userId: accepterUserId,
                                fullName: accepterUser.fullName,
                                profileUrl: accepterUser.profileUrl

                            }
                            await FriendList.updateOne({ userId: senderUserId }, { $push: { friendsId: accepterDetail } }, { upsert: true, new: true });
                            await FriendList.updateOne({ userId: accepterUserId }, { $push: { friendsId: request }, $pull: { friendsRequest: request } }, { upsert: true, new: true });
                            const senderUserData = await User.findOne({ _id: senderUserId })
                            // event send who accept friend request
                            let accepterUserFrindList = await FriendList.findOne({ userId: accepterUserId });
                            console.log('accepterUserFrindList :>> ', accepterUserFrindList);
                            let allLocalFriendsList = [];
                            let friendReauqet = [];
                            if (accepterUserFrindList != null) {

                                allLocalFriendsList = await getAllLocalFriends(accepterUserFrindList)

                                friendReauqet = await getAllfreiendsRequest(accepterUserFrindList)

                            } else {
                                console.log('accepterUserFrindList != null :  else ::>> ', accepterUserFrindList != null);
                                allLocalFriendsList = [];
                                friendReauqet = []
                            }
                            const accepterObj = {
                                message: `now your friend of ${senderUserData.fullName}`,
                                allLocalFriendsList,
                                friendReauqet
                            }
                            socket.emit("ADD_FRIEND", HelperUtils.successResponseOBJ("ADD_FRIEND", accepterObj));

                            // event send if  friend request sender online
                            if (senderUserData.isActive == true) {
                                console.log('senderUserData.isActive == true :>> ', senderUserData.isActive == true);
                                let senderUserFrindList = await FriendList.findOne({ userId: senderUserId });
                                let allLocalFriendsList = [];
                                let friendReauqet = [];
                                if (senderUserFrindList != null) {

                                    allLocalFriendsList = await getAllLocalFriends(senderUserFrindList)

                                    friendReauqet = await getAllfreiendsRequest(senderUserFrindList)

                                } else {
                                    console.log('senderUserFrindList != null :  else ::>> ', senderUserFrindList != null);
                                    allLocalFriendsList = [];
                                    friendReauqet = []
                                }
                                const senderObj = {
                                    message: `your friend request accept by ${userData.fullName}`,
                                    allLocalFriendsList,
                                    friendReauqet
                                }
                                socket.to(senderUserData.socketId).emit("ADD_FRIEND", HelperUtils.successResponseOBJ("ADD_FRIEND", senderObj))
                            }

                        } else {
                            const obj = {
                                message: `you haven't friend request from ${userData.fullName}`
                            }
                            socket.emit("ADD_FRIEND", HelperUtils.successResponseOBJ("ADD_FRIEND", obj))
                        }
                    }
                } catch (error) {
                    console.log('ADD_FRIEND :>> ', error, "-", msg, error.message);
                    socket.emit("ADD_FRIEND", HelperUtils.errorResponseObj("ADD_FRIEND", error.message, "-", error))
                    throw error
                }
            });

            socket.on("REJECT_FRIEND_REQUEST", async (msg) => {
                try {
                    console.log('REJECT_FRIEND_REQUEST', msg);
                    console.log('Type', typeof msg);
                    let objectData = JSON.parse(msg);
                    console.log('objectData :>> ', objectData);
                    const accepterUserId = objectData.userId;
                    console.log('accepterUserId :>> ', accepterUserId, typeof accepterUserId);

                    const senderUserId = objectData.friendsUserId;
                    console.log('senderUserId :>> ', senderUserId, typeof senderUserId);

                    const friendList = await FriendList.findOne({ userId: accepterUserId });

                    let friendReauqets = friendList.friendsRequest
                    if (friendReauqets) {
                        const request = friendReauqets.find(({ userId }) => userId === senderUserId);
                        await FriendList.updateOne({ userId: accepterUserId }, { $pull: { friendsRequest: request } }, { upsert: true, new: true });
                        let friendReauqet = []
                        friendReauqet = await getAllfreiendsRequest(friendList)
                        let rejectObj = {
                            message: `you are rejected a ${request.fullName}'s request`,
                            friendReauqet
                        }
                        socket.emit("REJECT_FRIEND_REQUEST", HelperUtils.successResponseOBJ("REJECT_FRIEND_REQUEST", rejectObj))
                    }
                } catch (error) {
                    console.log('REJECT_FRIEND_REQUEST :>> ', error, "-", msg, error.message);
                    socket.emit("REJECT_FRIEND_REQUEST", HelperUtils.errorResponseObj("REJECT_FRIEND_REQUEST", error.message, "-", error))
                    throw error
                }
            });

            // join table for user
            socket.on("JOIN_TABLE", async (msg) => {
                try {
                    console.log(' en :  JOIN_TABLE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log(objectData);
                    let token = objectData.token;
                    let tableId = objectData.tableId;

                    await joinTable(token, tableId, socket, io)


                } catch (error) {
                    console.log('JOIN_TABLE :>> ', error, " - ", msg);
                    socket.emit("JOIN_TABLE", HelperUtils.errorResponseObj("JOIN_TABLE", error.message, "-", error))
                }

            });

            //* join table for Admin
            socket.on("ADMIN_JOIN", async (msg) => {
                try {
                    console.log(' en :  ADMIN_JOIN_TABLE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log(objectData);
                    let token = objectData.token;
                    let tableId = objectData.tableId;

                    await adminJoinTable(token, tableId, socket);

                } catch (error) {
                    console.log('ADMIN_JOIN :>> ', error, "-", msg, error.message);
                    socket.emit("ADMIN_JOIN", HelperUtils.errorResponseObj("ADMIN_JOIN", error.message, "-", error))
                    throw error
                }
            });

            // send invitation event by admin
            socket.on("LOCAL_INVITE", async (msg) => {
                try {
                    console.log(' en :  LOCAL_INVITE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log(objectData);
                    let senderUserId = objectData.senderUserId;
                    let receiverUserId = objectData.receiverUserId;
                    let tableId = objectData.tableId;

                    const adminDetail = await User.findOne({ _id: senderUserId })
                    const receiverDetail = await User.findOne({ _id: receiverUserId })

                    // invitation receive from admin  
                    const localInvitedObj = {
                        userid: senderUserId,
                        profileUrl: adminDetail.profileUrl,
                        tableId: tableId,
                        message: `you invite by ${adminDetail.fullName}`
                    };
                    console.log('localInvitedObj :>> ', localInvitedObj);
                    socket.to(receiverDetail.socketId).emit("LOCAL_INVITE", HelperUtils.successResponseOBJ("LOCAL_INVITE", localInvitedObj));

                    // admin send invitation to his friends
                    const adminObj = {
                        userid: receiverUserId,
                        message: `invitation send successfully to ${adminDetail.fullName}`
                    };
                    console.log('adminObj :>> ', adminObj);
                    socket.emit("LOCAL_INVITE", HelperUtils.successResponseOBJ("LOCAL_INVITE", adminObj));

                } catch (error) {
                    console.log('CATCH_ERROR : LOCAL_INVITE :>> ', error, "-", msg, error.message);
                    socket.emit("LOCAL_INVITE", HelperUtils.errorResponseObj("LOCAL_INVITE", error.message, "-", error))
                    throw error
                }
            });

            // invitation reject by admin's friend
            socket.on("LOCAL_REJECT", async (msg) => {
                try {
                    console.log(' en :  LOCAL_REJECT  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log(objectData);
                    let senderUserId = objectData.senderUserId;
                    let receiverUserId = objectData.receiverUserId;
                    let tableId = objectData.tableId;

                    const senderDetail = await User.findOne({ _id: senderUserId })
                    const receiverDetail = await User.findOne({ _id: receiverUserId })

                    //user reject invitation by user
                    const rejectUserObj = {
                        message: `you reject ${senderDetail.fullName}'s invitation`
                    }
                    console.log('LOCAL_REJECT : rejectUserObj :>> ', rejectUserObj);
                    socket.emit("LOCAL_REJECT", HelperUtils.successResponseOBJ("LOCAL_REJECT", rejectUserObj))

                    // rejection receive from user to admin
                    const rejectReceiveObj = {
                        userid: receiverUserId,
                        message: `${receiverDetail.fullName} reject your invitation`
                    }
                    console.log('LOCAL_REJECT : rejectReceiveObj :>> ', rejectReceiveObj);
                    socket.to(senderDetail.socketId).emit("LOCAL_REJECT", HelperUtils.successResponseOBJ("LOCAL_REJECT", rejectReceiveObj))

                } catch (error) {
                    console.log('CATCH_ERROR : LOCAL_REJECT :>> ', error, "-", msg, error.message);
                    socket.emit("LOCAL_REJECT", HelperUtils.errorResponseObj("LOCAL_REJECT", error.message, "-", error))
                    throw error
                }
            });

            //* admin start the game round 
            socket.on("ADMIN_START_GAME", async (msg) => {
                try {
                    console.log(' en :  ADMIN_START_GAME  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log(objectData);
                    const token = objectData.token;
                    const tableId = objectData.tableId;
                    const isAdminStart = objectData.isAdminStart

                    const roomSize = socket.adapter.rooms.get(tableId).size;
                    const session = await Session.findOne({ sid: token });
                    const user = await User.findOne({ _id: session.uid })
                    if (session) {
                        await Table.updateOne({ _id: tableId }, { $set: { isAdminStartRound: isAdminStart } }, { upsert: true, })
                        const tDatil = await Table.findOne({ _id: tableId });
                        console.log('tDatil :>> ', tDatil);


                        if (tDatil.isAdminStartRound == true) {
                            const obj = {
                                message: `Game started by admin ${user.fullName}`,
                            }
                            socket.emit("ADMIN_START_GAME", HelperUtils.successResponseOBJ("ADMIN_START_GAME", obj))
                            if (tDatil.tableStatus == "inWaiting") {
                                let tableStatus = "timerStart"
                                await resetTicket(tableId, roomSize, socket, io)
                                await Table.updateOne({ _id: tableId }, { $set: { tableStatus: tableStatus } }, { upsert: true, })
                            }

                        } else {
                            const obj = {
                                message: `Game stop by admin ${user.fullName}`,
                            }
                            socket.emit("ADMIN_START_GAME", HelperUtils.successResponseOBJ("ADMIN_START_GAME", obj))
                        }
                    }

                } catch (error) {
                    console.log('ADMIN_START_GAME :>> ', error, " - ", msg);
                    socket.emit("ADMIN_START_GAME", HelperUtils.errorResponseObj("ADMIN_START_GAME", error.message, "-", error))
                    throw error
                }
            });

            socket.on("ADMIN_TICKET_OPEN", async (msg) => {
                try {
                    let objectData = JSON.parse(msg);
                    console.log('ADMIN_TICKET_OPEN : objectData :>> ', objectData);

                    const tableId = objectData.tableId
                    const senderUserId = objectData.adminUserId

                    await Table.updateOne({ _id: tableId }, { $set: { isTicketOpenByAdmin: true } }, { upsert: true, new: true })
                    const tableDetail = await Table.findOne({ _id: tableId })
                    const isTicketOpenByAdmin = tableDetail.isTicketOpenByAdmin

                    const randomTicket = await adminTicketOpen(tableId, isTicketOpenByAdmin)
                    let counter = tableDetail.currentRoundCounter

                    console.log('socket.adapter.rooms :>> ', socket.adapter.rooms);
                    const roomSize = socket.adapter.rooms.get(tableId).size
                    console.log('roomSize abcd :>> ', roomSize);

                    obj = {
                        ticketLatter: randomTicket,
                        currentRoundCounter: counter,
                        openByAdmin: true
                    }
                    io.to(tableId).emit("ADMIN_TICKET_OPEN", HelperUtils.successResponseOBJ("ADMIN_TICKET_OPEN", obj));
                    // winner declared 
                    await setWinnData(tableId, randomTicket, socket, io, roomSize);
                } catch (error) {
                    console.log('ADMIN_TICKET_OPEN :>> ', error, "-", msg, error.message);
                    socket.emit("ADMIN_TICKET_OPEN", HelperUtils.errorResponseObj("ADMIN_TICKET_OPEN", error.message, "-", error))
                    throw error
                }
            });

            //* USER BET 
            socket.on("USER_BET", async (msg) => {
                try {
                   
                    console.log('msg :>> ', msg, typeof msg);
                    let objectData = JSON.parse(msg);
                    const userId = objectData.userId;
                    const tableId = objectData.tableId;
                    const bets = objectData.bets;

                    let totalBetAmount = 0;
                    bets.forEach(element => {
                        totalBetAmount += element.betLattereAmount
                    });

                    const getUser = await User.findById({ _id: userId });

                    console.log('bets :>> ', bets);
                    if (getUser.walletBalance >= totalBetAmount) {

                        let updateBalance = getUser.walletBalance - totalBetAmount

                        await User.updateOne({ _id: userId }, { $set: { walletBalance: updateBalance } }, { upsert: true });

                        const updatedUserData = await setBetData(objectData, socket);
                        console.log('updatedUserData :>> ', JSON.stringify(updatedUserData, null, 2));

                        const upateBetTableArray = await Table.updateOne({ _id: tableId }, { $set: { "userData": updatedUserData } }, { new: true, upsert: true });

                        let obj = {
                            isBat: true,
                            walletBalance: updateBalance
                        }
                        socket.emit("USER_BET", HelperUtils.successResponseOBJ("USER_BET", obj));

                        const tDetail = await Table.findOne({ _id: tableId });
                        const betData = tDetail.userData

                        const userBetData = betData.find(obj => obj.userId.toString() === userId.toString());
                        const adminDetail = await User.findOne({ _id: tDetail.adminDetail.adminUserId })
                        let betDetailForAdmin = {
                            userId: userId.toString(),
                            betData: userBetData.userBet,
                            adminWalletBalance: adminDetail.walletBalance
                        }
                        socket.to(tDetail.adminDetail.socketId).emit("ADMIN_BET", HelperUtils.successResponseOBJ("ADMIN_BET", betDetailForAdmin))

                    } else {
                        let obj = {
                            isbat: false,
                            walletBalance: Number(getUser.walletBalance.toFixed(2)),
                            message: "insufficient balance in your wallet"
                        };
                        socket.emit("INSUFFICIENT_BALANCE", HelperUtils.successResponseOBJ("INSUFFICIENT_BALANCE", obj))
                    }
                } catch (error) {
                    console.log('CATCH_ERROR : USER_BET :>> ', msg, "-", error);
                    throw error
                }

            });

             //* USER BET GAME
            socket.on("USER_BET_GAME", async (msg) => {
                try {

                    console.log('msg :>> ', msg, typeof msg);
                    let objectData = JSON.parse(msg);
                    const userId = objectData.userId;
                    const tableId = objectData.tableId;
                    const bets = objectData.bets;
                    const gameid = objectData.game_id;

                    let totalBetAmount = bets;
                     //totalBetAmount = bets;
                    // bets.forEach(element => {
                    //     totalBetAmount += element.betLattereAmount
                    // });

                    const getUser = await User.findById({ _id: userId });
                    const updateArray = [];
                    console.log('bets :>> ', bets);
                    if (getUser.walletBalance >= totalBetAmount) {

                        let updateBalance = getUser.walletBalance - totalBetAmount;

                        await User.updateOne({ _id: userId }, { $set: { walletBalance: updateBalance } }, { upsert: true });
                         updateArray.push({
                            userId: getUser.userId,
                            userName: getUser.fullName,
                            userBet: totalBetAmount
                        })
                        // const updatedUserData = await setGameBetData(objectData, socket);
                        // console.log('updatedUserData :>> ', JSON.stringify(updatedUserData, null, 2));

                        //const upateBetTableArray = await Tablebet.updateOne({ _id: tableId }, { $set: { "userData": updateArray, "gameId": gameid,"tableName":getUser.fullName,"betsAmount":totalBetAmount } }, { new: true, upsert: true });

                        let obj = {
                            isBat: true,
                            walletBalance: updateBalance
                        }
                        socket.emit(" Rakesh  USER_BET_GAME");
                        socket.emit("USER_BET_GAME", HelperUtils.successResponseOBJ("USER_BET_GAME", obj));

                        // const tDetail = await Table.findOne({ _id: tableId });
                        // const betData = tDetail.userData

                        // const userBetData = betData.find(obj => obj.userId.toString() === userId.toString());
                        // const adminDetail = await User.findOne({ _id: tDetail.adminDetail.adminUserId })
                        // let betDetailForAdmin = {
                        //     userId: userId.toString(),
                        //     betData: userBetData.userBet,
                        //     game_id: gameid,
                        //     adminWalletBalance: adminDetail.walletBalance
                        // }
                        //socket.to(tDetail.adminDetail.socketId).emit("ADMIN_BET", HelperUtils.successResponseOBJ("ADMIN_BET", betDetailForAdmin))

                    } else {
                        let obj = {
                            isbat: false,
                            walletBalance: Number(getUser.walletBalance.toFixed(2)),
                            message: "insufficient balance in your wallet"
                        };
                        socket.emit("INSUFFICIENT_BALANCE", HelperUtils.successResponseOBJ("INSUFFICIENT_BALANCE", obj))
                    }
                } catch (error) {
                    console.log('CATCH_ERROR : USER_BET_GAME :>> ', msg, "-", error);
                    throw error
                }

            });

            // for user leave table
            socket.on("LEAVE_TABLE", async (msg) => {
                try {
                    console.log(' en :  LEAVE_TABLE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('LEAVE_TABLE : objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const tableId = objectData.tableId;

                    await User.updateOne({ _id: userId }, { currantTableId: "" }, { upsert: true, new: true });

                    const isAdminLeft = false
                    const message = "you are leave table successfully."

                    await userLeave(userId, tableId, isAdminLeft, message, socket);





                } catch (error) {
                    console.log('CATCH_ERROR : LEAVE_TABLE :>> ', msg, "-", error);
                    throw error
                }
            });

            // for admin leave table
            socket.on("ADMIN_LEAVE_TABLE", async (msg) => {
                try {
                    console.log(' en :  ADMIN_LEAVE_TABLE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('LEAVE_TABLE : objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const tableId = objectData.tableId;

                    const message = "you left table successfully."
                    const userMessage = "you are leave table due to admin left."
                    await adminLeave(userId, tableId, message, userMessage, socket, io)

                } catch (error) {
                    console.log('ADMIN_LEAVE_TABLE_Event :>> ', error, "-", msg, error.message);
                    socket.emit("ADMIN_LEAVE_TABLE", HelperUtils.errorResponseObj("ADMIN_LEAVE_TABLE", error.message, "-", error))
                    throw error
                }
            });

            socket.on("ADMIN_MUTE_ALL", async (msg) => {
                try {
                    console.log(' en :  ADMIN_LEAVE_TABLE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('LEAVE_TABLE : objectData :>> ', objectData);
                    const isAllMute = objectData.isAllMute;
                    const tableId = objectData.tableId;
                    let resobj;
                    if (isAllMute) {
                        resobj = {
                            isAdminMute: true,
                            message: "Admin mute all user"
                        }
                    } else {
                        resobj = {
                            isAdminMute: false,
                            message: "Admin unmute all user"
                        }
                    }

                    const tDatail = await Table.findOne({ _id: tableId })
                    const userdata = tDatail.userData
                    userdata.forEach((element) => {
                        const socketId = element.socketId
                        socket.to(socketId).emit("ADMIN_MUTE_ALL", HelperUtils.successResponseOBJ("ADMIN_MUTE_ALL", resobj))
                    });
                    let adminObj;
                    if (isAllMute) {
                        adminObj = {
                            isAdminMute: true,
                            message: "You muted all user in table"
                        }
                    } else {
                        adminObj = {
                            isAdminMute: false,
                            message: "You unmuted all user in table"
                        }
                    }

                    socket.emit("ADMIN_MUTE_ALL", HelperUtils.successResponseOBJ("ADMIN_MUTE_ALL", adminObj))

                } catch (error) {
                    console.log('ADMIN_MUTE_ALL_Event :>> ', error, "-", msg, error.message);
                    socket.emit("ADMIN_MUTE_ALL", HelperUtils.errorResponseObj("ADMIN_MUTE_ALL", error.message, "-", error))
                    throw error
                }
            })


            socket.on("DAILY_SPIN", async (msg) => {
                try {
                    console.log(' en :  DAILY_SPIN  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('DAILY_SPIN : objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const isAdSpin = objectData.isAdSpin

                    await setDailySpinEventResponse(userId, isAdSpin, socket)

                } catch (error) {
                    console.log('DAILY_SPIN_Event :>> ', error, "-", msg, error.message);
                    socket.emit("DAILY_SPIN", HelperUtils.errorResponseObj("DAILY_SPIN", error.message, "-", error))
                    throw error
                }
            })

            socket.on("DAILY_REWARDS", async (msg) => {
                try {
                    console.log(' en :  DAILY_REWARDS  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('DAILY_REWARDS : objectData :>> ', objectData);
                    const userId = objectData.userId,
                        day = objectData.day

                    const isClaim = true,
                        dailyReward = await dailyRewardsCounter(userId, isClaim, day);
                    await setDailyRewardEventResponse(userId, dailyReward, socket)

                } catch (error) {
                    console.log('DAILY_REWARDS_Event :>> ', error, "-", msg, error.message);
                    socket.emit("DAILY_REWARDS", HelperUtils.errorResponseObj("DAILY_REWARDS", error.message, "-", error))
                    throw error
                }
            });

            socket.on("UPDATE_BALANCE", async (msg) => {
                try {
                    console.log(' en :  UPDATE_BALANCE  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('UPDATE_BALANCE : objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const userBonus = objectData.userBonus;

                    const userDetail = await User.findOne({ _id: userId });
                    console.log('userDetail :>> ', userDetail);

                    const updatedWalletBalance = userDetail.walletBalance == 0 ? userBonus : userDetail.walletBalance + userBonus

                    const userDetails = await User.updateOne({ _id: userId }, { walletBalance: updatedWalletBalance }, { upsert: true, new: true });
                    console.log('userDetails :>> ', userDetails);

                    const updatedWalletBalanceObj = {
                        userBonus,
                        walletBalance: updatedWalletBalance
                    }

                    socket.emit("UPDATE_BALANCE", HelperUtils.successResponseOBJ("UPDATE_BALANCE", updatedWalletBalanceObj))
                } catch (error) {
                    console.log('UPDATE_BALANCE_Event :>> ', error, "-", msg, error.message);
                    socket.emit("UPDATE_BALANCE", HelperUtils.errorResponseObj("UPDATE_BALANCE", error.message, "-", error))
                    throw error
                }
            })


            socket.on("STORE_HISTROY", async (msg) => {
                try {
                    var moment = require('moment-timezone');
                    console.log(' en :  STORE_HISTROY  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('STORE_HISTROY : objectData :>> ', objectData);
                    const token = objectData.token;
                    const purchaseAmount = objectData.purchaseAmount;
                    const session = await Session.findOne({ sid: token });
                    if (session) {
                        const currantDate = moment(Date.now()).format('l');
                        console.log('currantDate :>> ', currantDate);
                        const currantTime = moment(Date.now()).tz("Asia/Calcutta").format('LT');
                        console.log('currantTime :>> ', currantTime);
                        let obj
                        const histroyDetail = await Histroy.findOne({ userId: session.uid })
                        if (!histroyDetail) {
                            obj = [{
                                amount: purchaseAmount,
                                purchaseDate: currantDate,
                                purchaseTime: currantTime
                            }]
                            await Histroy.create({
                                userId: session.uid,
                                histroy: obj
                            })
                        } else {
                            obj = {
                                amount: purchaseAmount,
                                purchaseDate: currantDate,
                                purchaseTime: currantTime
                            }
                            await Histroy.updateOne({ userId: session.uid }, { $push: { histroy: obj } }, { upsert: true, new: true })
                        }
                        console.log('obj :>> ', obj);
                    }

                } catch (error) {
                    console.log('STORE_HISTROY_Event :>> ', error, "-", msg, error.message);
                    socket.emit("STORE_HISTROY", HelperUtils.errorResponseObj("STORE_HISTROY", error.message, "-", error))
                    throw error
                }
            })

            socket.on("GET_HISTROY", async (msg) => {
                try {
                    console.log(' en :  GET_HISTROY  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('GET_HISTROY : objectData :>> ', objectData);
                    const token = objectData.token;

                    const session = await Session.findOne({ sid: token });
                    if (session) {
                        const histroy = await Histroy.findOne({ userId: session.uid })
                        let obj
                        if (!histroy) {
                            obj = {
                                message: "histroy not found",
                                histroy: []
                            }
                        } else {
                            obj = {
                                message: "histroy fetch successfully",
                                histroy: histroy.histroy
                            }
                        }
                        socket.emit("GET_HISTROY", HelperUtils.successResponseOBJ("GET_HISTROY", obj))
                    }

                } catch (error) {
                    console.log('GET_HISTROY_Event :>> ', error, "-", msg, error.message);
                    socket.emit("GET_HISTROY", HelperUtils.errorResponseObj("GET_HISTROY", error.message, "-", error))
                    throw error
                }
            })

            socket.on("DELETE_ACCOUNT", async (msg) => {
                try {
                    console.log(' en :  DELETE_ACCOUNT  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('DELETE_ACCOUNT : objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const feedback = objectData.feedback;

                    const userDetail = await User.findById(userId)

                    const feedBack = await FeedBack.create({
                        userId: userId,
                        mobileNo: userDetail.mobileNo,
                        email: userDetail.email,
                        fid: userDetail.fid,
                        feedback: feedback
                    })

                    await User.deleteOne({ _id: userId });
                    await DailySpins.deleteOne({ userId: userId });
                    await Dailyrewards.deleteOne({ userId: userId });

                    const friendDetail = await FriendList.findOne({ userId: userId });
                    if (friendDetail) {
                        await FriendList.deleteOne({ userId: userId });
                    }
                    const histroyDetail = await Histroy.findOne({ userId: userId });
                    if (histroyDetail) {
                        await Histroy.deleteOne({ userId: userId });
                    }
                    await Session.deleteOne({ uid: userId });
                    await UserLevel.deleteOne({ userId: userId })

                    obj = {
                        message: "your account deleted successfully",
                    }
                    socket.emit("DELETE_ACCOUNT", HelperUtils.successResponseOBJ("DELETE_ACCOUNT", obj))

                } catch (error) {
                    console.log('DELETE_ACCOUNT_Event :>> ', error, "-", msg, error.message);
                    socket.emit("DELETE_ACCOUNT", HelperUtils.errorResponseObj("DELETE_ACCOUNT", error.message, "-", error))
                    throw error
                }
            });

            // place order 
            socket.on("PLACE_ORDER", async (msg) => {
                try {

                    console.log(' en :  PLACE_ORDER  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('PLACE_ORDER : objectData :>> ', objectData);
                    const userId = objectData.userId,
                        level = objectData.level,
                        productId = objectData.productId,
                        denomination = objectData.denomination,
                        contact = objectData.contact,
                        tag = objectData.tag

                    const quantity = 1;

                    const userDetail = await User.findOne({ _id: userId })
                    console.log('userDetail :>> ', userDetail);

                    const unum = Math.floor(100000 + Math.random() * 900000);
                    console.log('unum :>> ', unum);

                    const poNumber = `${userDetail.fullName}` + `${level}` + `${unum}`
                    console.log('poNumber :>> ', poNumber);


                    const data = {
                        productId: Number(productId),
                        quantity: Number(quantity),
                        denomination: Number(denomination),
                        email: "",
                        contact: `+91-${contact}`,
                        tag,
                        poNumber
                    }

                    let respData = await xoxoDay.placeXoxoVoucherOrder(data);

                    console.log('resp.code :>> ', respData.isError);

                    if (respData.isError == false) {
                        let resp = respData.resp;
                        const findLeavelDetail = await UserLevel.findOne({ userId: userId });

                        const claimedVoucher = findLeavelDetail.claimedVoucher

                        const query = { userId: userId, "claimedVoucher.level": level }
                        const update = {
                            $set: {
                                "claimedVoucher.$.isClaimed": true,
                                "claimedVoucher.$.voucherData": resp
                            }
                        }

                        await UserLevel.updateOne(query, update)

                        const userData = await UserLevel.findOne({ userId: userId })

                        const levelData = {
                            userId,
                            level: userData.userlevel
                        }

                        const response = await getCourrantLevelVoucher(levelData)

                        const userVoucher = findLeavelDetail.userVoucher

                        let tempUserVoucher = [];
                        for (let i = 0; i < userVoucher.length; i++) {
                            const element = userVoucher[i];

                            if (element.level != level) {
                                tempUserVoucher.push(element)
                            } else {
                                console.log('claimed user voucher data remove ');
                            }

                        }

                        await UserLevel.findOneAndUpdate({ userId: userId }, { userVoucher: tempUserVoucher }, { upsert: true, new: true });

                        const obj = {
                            message: "Order placed successfully",
                            userlevel: response.userlevel,
                            userlevelRound: response.userlevelRound,
                            levelUpRound: response.levelUpRound,
                            levelUpPrice: response.levelUpPrice,
                            currantLevelVouchers: response.finalVoucherArray,
                            claimedVoucher: response.claimedVoucher
                        }

                        console.log('obj :>> ', JSON.stringify(obj, null, 2));

                        socket.emit("PLACE_ORDER", HelperUtils.successResponseOBJ("PLACE_ORDER", obj));

                    } else {
                        socket.emit("PLACE_ORDER_ERROR", HelperUtils.successResponseOBJ("PLACE_ORDER_ERROR", respData.resp));
                    }
                } catch (error) {
                    console.log('CATCH_ERROR :: PLACE_ORDER :>> ', error, " - ", JSON.parse(msg));
                    socket.emit("PLACE_ORDER", HelperUtils.errorResponseObj("PLACE_ORDER", error.message, "-", error));
                    throw error
                }
            });


            //in this event user currant leavel detail and  vouchers
            socket.on("CURRANT_LEVEL", async (msg) => {
                try {
                    console.log(' en :  CURRANT_LEVEL  start :>> ', msg);
                    let objectData = JSON.parse(msg);
                    console.log('CURRANT_LEVEL : objectData :>> ', objectData);
                    const userId = objectData.userId;
                    const level = objectData.level;

                    const data = {
                        userId,
                        level
                    }

                    const result = await getCourrantLevelVoucher(data)
                    const obj = {
                        message: "available voucher",
                        userlevel: result.userlevel,
                        userlevelRound: result.userlevelRound,
                        levelUpRound: result.levelUpRound,
                        levelUpPrice: result.levelUpPrice,
                        currantLevelVouchers: result.finalVoucherArray,
                        claimedVoucher: result.claimedVoucher
                    }
                    console.log('obj :>> ', JSON.stringify(obj, null, 2));
                    socket.emit("CURRANT_LEVEL", HelperUtils.successResponseOBJ("CURRANT_LEVEL", obj))


                } catch (error) {
                    console.log('CURRANT_LEVEL_Event :>> ', error, "-", msg, error.message);
                    socket.emit("CURRANT_LEVEL", HelperUtils.errorResponseObj("CURRANT_LEVEL", error.message, "-", error))
                    throw error
                }
            });

            async function getAllLocalFriends(friends) {
                try {

                    console.log('friends != null :  if ::>> ', friends != null);
                    let friendsDeatil = friends.friendsId;
                    console.log('friendsDeatil :>> ', JSON.stringify(friendsDeatil, null, 2));
                    let allLocalFriendsList = []
                    for (let index = 0; index < friendsDeatil.length; index++) {
                        const element = friendsDeatil[index];
                        const getUser = await User.findOne({ _id: element.userId });
                        const allFriendsListObj = {
                            userId: getUser._id.toString(),
                            userName: getUser.fullName,
                            profileUrl: getUser.profileUrl,
                            isActive: getUser.isActive
                        };
                        allLocalFriendsList.push(allFriendsListObj)
                    }
                    console.log('getAllLocalFriends : allLocalFriendsList :>> ', allLocalFriendsList);
                    return allLocalFriendsList
                } catch (error) {
                    console.log('CATCH_ERROR : getAllLocalFriends :>> ', error);
                    throw error
                }
            }

            async function getAllfreiendsRequest(friends) {
                try {
                    console.log('friends != null :  if ::>> ', friends != null);
                    let friendReauqet = friends.friendsRequest;
                    console.log('friendReauqet :>> ', JSON.stringify(friendReauqet, null, 2));
                    let allFriendRequest = []
                    friendReauqet.forEach(async (element) => {
                        const allFriendsListObj = {
                            userId: element.userId,
                            userName: element.fullName,
                            profileUrl: element.profileUrl,
                            message: element.message
                        };
                        allFriendRequest.push(allFriendsListObj)
                    });
                    console.log('getAllfreiendsRequest : allFriendRequest :>> ', allFriendRequest);
                    return allFriendRequest


                } catch (error) {
                    console.log('CATCH_ERROR : getAllfreiendsRequest :>> ', error);
                    throw error
                }
            };

            socket.on('disconnect', async function (disc) {
                try {

                    const socketId = socket.id

                    // TODO send event to all friends that this user is dicconect
                    console.log('socketId :>> ', socketId);
                    const getUser = await User.findOne({ socketId: socketId });
                    console.log('getUser :>> ', getUser);
                    if (getUser) {
                        const userId = getUser._id.toString();
                        if (getUser.currantTableId != "") {
                            const tableDetail = await Table.findOne({ _id: getUser.currantTableId })

                            if (userId != tableDetail.adminDetail.adminUserId) {

                                const isAdminLeft = false
                                const message = "you are leave table successfully."
                                await userLeave(userId, getUser.currantTableId, isAdminLeft, message, socket);
                            }
                            await User.findOneAndUpdate({ _id: userId }, { $set: { disconnectionTime: Date.now() } }, { upsert: true, new: true })
                        }
                        const isActive = false;
                        await getLocalFriends(userId, isActive);
                        const userDetail = await User.updateOne({ socketId: socket.id }, { $set: { isActive: false } }, { upsert: true, new: true });
                        console.log('userDetail :>> ', userDetail);
                    }


                    console.log('user disconnected');
                    console.log(
                        "ping: disconnect------------>>>disc: ",
                        disc,
                        "socket:",
                        socket.id
                    );
                } catch (error) {
                    console.log('disconnects :>> ', error, "-", error.message);
                    socket.emit("disconnects", HelperUtils.errorResponseObj("disconnects", error.message, "-", error))
                    throw error
                }
            });

            async function getLocalFriends(userId, isActive) {
                try {
                    const friendsList = await FriendList.findOne({ userId })
                    console.log('friendsList :>> ', friendsList);

                    if (friendsList != null) {
                        const friendId = friendsList.friendsId

                        for (let i = 0; i < friendId.length; i++) {
                            const element = friendId[i];
                            const getFriends = await User.findOne({ _id: element.userId });
                            console.log('getFriends :>> ', getFriends);
                            if (getFriends.isActive === true) {
                                const fList = {
                                    userId,
                                    isActive
                                };
                                socket.to(getFriends.socketId).emit("STATUS_LOCAL_FRIEND", HelperUtils.successResponseOBJ("STATUS_LOCAL_FRIEND", fList))
                            };
                        }
                    }

                    return
                } catch (error) {
                    console.log('CATCH_ERROR : getLocalFriends  :>> ', error);
                }
            }

        });
    } catch (error) {
        console.log('error :>> ', error);
    }

}

module.exports = function (server) {
    logger.info("In socket Server")
    //const io = socketio(server, { origins: "*", 'pingTimeout': 180000, 'pingInterval': 25000 })
    const io = socketio(server, {
        cors: {
            origin: "*", // Allow all origins
            methods: ["GET", "POST"],
            allowedHeaders: ["my-custom-header"],
            credentials: true
        },
        pingTimeout: 180000,
        pingInterval: 25000
    });
    global.io = io;
    console.log("ddasddffdhgfghgfhjhg")
    openSocketConnection();
}

