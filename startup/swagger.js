
const config = require("config");

module.exports = function (app) {
    const expressSwagger = require('express-swagger-generator')(app)

    let options = {
        swaggerDefinition: {
            info: {
                description: 'Api for Prediction App',
                title: 'Prediction App',
                version: '1.0.0',
            },
            host: config.get("host"),
            basePath: '/api',
            produces: [
                "application/json"
            ],
             schemes: ['http', 'https'],
            securityDefinitions: {
                Admin: {
                    type: 'apiKey',
                    in: 'header',
                    name: 'x-auth-token',
                    description: "",
                },
                User: {
                    type: 'apiKey',
                    in: 'header',
                    name: 'x-auth-token',
                    description: "",
                },
            }
        },
        basedir: __dirname, //app absolute path
        files: ['./../routes/*.js'] //Path to the API handle folder
    }
    expressSwagger(options)
}