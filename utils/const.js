const config = require("config");
const { DEFAULT_CONTENT_TYPE } = require("multer-s3");

let PathUrlConstant = Object.freeze({
    APPLICATION_URL: config.get("host")
});


module.exports = {
    PathUrlConstant: PathUrlConstant,
}